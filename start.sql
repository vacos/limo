-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 14, 2018 at 06:19 AM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `doctorgaming`
--

-- --------------------------------------------------------

--
-- Table structure for table `features`
--

CREATE TABLE `features` (
  `fea_id` int(11) NOT NULL,
  `fea_slug` varchar(255) NOT NULL,
  `fea_slug_spacail` varchar(255) DEFAULT NULL,
  `fea_use` text NOT NULL,
  `fea_status` tinyint(1) NOT NULL,
  `fea_custom_1` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `features`
--

INSERT INTO `features` (`fea_id`, `fea_slug`, `fea_slug_spacail`, `fea_use`, `fea_status`, `fea_custom_1`) VALUES
(3, 'ads', 'ads-top', '20170424112819774.png', 0, ''),
(4, 'ads', 'ads-rec-a', '20180123032141693.jpg', 0, ''),
(5, 'ads', 'ads-bottom-player', '20170605080041723.gif', 1, 'http://www.open365vip.com/');

-- --------------------------------------------------------

--
-- Table structure for table `maintenance`
--

CREATE TABLE `maintenance` (
  `id` tinyint(1) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `maintenance`
--

INSERT INTO `maintenance` (`id`, `status`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tc_admins`
--

CREATE TABLE `tc_admins` (
  `adm_id` int(11) NOT NULL,
  `adm_username` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `adm_password` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `adm_title` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `adm_login` datetime DEFAULT NULL,
  `adm_level` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `adm_status` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `adm_createdon` datetime DEFAULT NULL,
  `adm_modifiedon` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `tc_admins`
--

INSERT INTO `tc_admins` (`adm_id`, `adm_username`, `adm_password`, `adm_title`, `adm_login`, `adm_level`, `adm_status`, `adm_createdon`, `adm_modifiedon`) VALUES
(1, 'admin', 'f79b243766171f9417c50dd20e2f1b0d', NULL, NULL, '0', '0', NULL, NULL),
(2, 'topperAdmin', 'f79b243766171f9417c50dd20e2f1b0d', NULL, NULL, '0', '0', NULL, NULL),
(3, 'support', 'e10adc3949ba59abbe56e057f20f883e', NULL, NULL, '0', '0', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `features`
--
ALTER TABLE `features`
  ADD PRIMARY KEY (`fea_id`);

--
-- Indexes for table `maintenance`
--
ALTER TABLE `maintenance`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `features`
--
ALTER TABLE `features`
  MODIFY `fea_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `maintenance`
--
ALTER TABLE `maintenance`
  MODIFY `id` tinyint(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
