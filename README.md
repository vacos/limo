# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

1. Pull this repo
2. Config Constant /_ci314/application/config/constant.php
3. Config DB /_ci314/application/config/database.php
4. Set Host to domainname.local
5. Enable VirtualHosts /Applications/XAMPP/xamppfiles/etc/httpd.conf
6. Create your VirtualHosts /Applications/XAMPP/xamppfiles/etc/extra/httpd-vhosts.conf

First Setup
# localhost
<VirtualHost *:80>
    ServerName localhost
    DocumentRoot "/Applications/XAMPP/xamppfiles/htdocs"
    <Directory "/Applications/XAMPP/xamppfiles/htdocs">
        Options Indexes FollowSymLinks Includes execCGI
        AllowOverride All
        Require all granted
    </Directory>
</VirtualHost>

# My custom host
<VirtualHost *:80>
    ServerName mysite.local
    DocumentRoot "/Users/yourusername/path/to/your/site"
    <Directory "/Users/yourusername/path/to/your/site">
        Options Indexes FollowSymLinks Includes ExecCGI
        AllowOverride All
        Require all granted
    </Directory>
    ErrorLog "logs/mysite.local-error_log"
</VirtualHost>

7. Create and Dump DB to phpmyadmin start.sql
8. Run npm init
9. Run npm i
10. Go to http://domainname.local

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact