var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var rename = require('gulp-rename');
var minifyCSS = require('gulp-minify-css');

var DEST = 'assets/web/';

gulp.task('build', function() {
    gulp.src('assets/scss/**/*.scss')
        
        // output non-minified CSS file
        .pipe(autoprefixer('last 2 versions', '> 5%'))
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer())
        .pipe(gulp.dest(DEST))

        // output the minified version
        .pipe(minifyCSS())
        .pipe(rename({ extname: '.min.css' }))
        .pipe(gulp.dest(DEST))
        
});

//Watch task
gulp.task('default',function() {
    gulp.watch('assets/scss/**/*.scss',['build']);
});
