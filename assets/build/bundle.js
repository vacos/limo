/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

$( "a" ).click(function() {
  var url = $( this ).attr("href");
  var blank = $( this ).attr("blank");
  if(blank != 'no'){
    //window.open(url);
    //return false;
  }
});

function LoadMore(type,cat_id,offset,limit){
  var url = '/loadmore/' + type +'/'+ cat_id +'/'+ offset +'/'+ limit;
  $.ajax({ type: "GET",   
         url: url,   
         async: false,
         success : function(html)
         {
           if(html != 'no_result'){
              $("#category_" + cat_id).append(html);
           }
           else{
              alert('ไม่พบคลิป');
           }
             
         }
  });
}

$(document).ready(function(){
  $('.navbar-toggle').click(function(){
    $('.nav-left').toggleClass('nav-view');
  });
});

$(document).ready(function(){
  $( ".view_entry--body img" ).addClass('img-responsive');
  $( ".view_entry--body img" ).removeAttr( "height" );
  $( ".view_entry--body img" ).removeAttr( "width" );
  $( ".view_entry--body img" ).removeAttr( "style" );
  $( ".view_entry--body img" ).wrap('<div class="img-container"></div>');

  $( ".view_entry--body iframe" ).wrap('<div class="video-container"></div>');
  
});

function show_menu(id){
  $('.nav-' + id).stop(true).slideToggle();
}
$(document).mouseup(function(e) 
{
    var container = $(".nav-football");

    // if the target of the click isn't the container nor a descendant of the container
    if (!container.is(e.target) && container.has(e.target).length === 0) 
    {
        $('.nav-football').stop(true).slideUp();
    }
});

function go(url){
  window.open(url,'_blank');
}

function view(action){
    $('.filter_desktop--menu').removeClass('active');
    $('#btn-' + action).addClass('active');
    
    show_program(action);
  }

  $( document ).ready(function() {
    $( ".filter" ).change(function() {
      var filter = $(this).val();
      show_program(filter);
    });
  });

  function show_program(status_action){

    if(status_action != 'all'){
      $('.list_program').hide();
    }
    else{
      $('.list_program').show();
    }
    
    $(".program--program .list_program").each(function(){
        var status = $(this).attr("data-status");
        if(status == status_action){
          $(this).show();
        }
    });
  }


/***/ })
/******/ ]);