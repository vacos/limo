<?php
class MY_Controller extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('usermodel', 'user', TRUE);
		$this->load->model('payment_methodmodel', 'payment_method', TRUE);
		$this->load->model('ordermodel', 'order', TRUE);
		$this->load->model('drivermodel', 'driver', TRUE);
		$this->load->model('countrymodel', 'country', TRUE);
		$this->load->model('carmodel', 'car', TRUE);
		$this->load->model('placemodel', 'place', TRUE);
		$this->load->model('pagemodel', 'page', TRUE);
		$this->load->model('paymentmodel', 'payment', TRUE);

		$this->load->library('omise_api');
	}

	protected function checkHeader($raw_data){

		header('Content-Type: application/json');
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods: GET, POST');
		header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept,x-api-key");
		
		$token = $this->genToken();
		if(isset($raw_data['X-Api-Key'])){

			if($raw_data['X-Api-Key'] == $token){
				return true;
			}
			
		}

		$json['status'] = 500;
		$json['message'] = 'Error API KEY';

		$this->return_json($json);
	}

	private function genToken(){

		return md5('LIM@:*&007');
	}

	function getRequestHeaders() {
		$headers = array();
		foreach($_SERVER as $key => $value) {
			if (substr($key, 0, 5) <> 'HTTP_') {
				continue;
			}
			$header = str_replace(' ', '-', ucwords(str_replace('_', ' ', strtolower(substr($key, 5)))));
			$headers[$header] = $value;
		}
		return $headers;
	}

	protected function getCache(){
		$this->output->cache(5);
	}

	protected function return_json($data = array()){
		echo json_encode($data);
		exit;
	}

	protected function genPassword($password = ''){
		$salt = $this->config->item('salt_p');
		return md5($password.$salt);
	}

	protected function checkCardGateWay($user_id,$omise_data = array()){
		$this->omise_api->setUserID($user_id);
		$omise_return = $this->omise_api->getToken($omise_data);

		if(isset($omise_return['object'])){

			if($omise_return['object'] == 'error'){
				return false;
			}

			else{

				if(isset($omise_return['id'])){

					$amount = $this->calAmountOmise(20);
					$return_charges = $this->omise_api->charges($omise_return['id'],$amount);

					
					if(isset($return_charges['status']) && $return_charges['status'] == 'successful'){
						return true;
					}
					else{
						return false;
					}

				}
				else{
					return false;

				}

			}
			

		}else{

			return false;

		}
	}

	protected function calAmountOmise($amout = 0){
		return (int) $amout.'00';
	}
	
	protected function calDataToOmise($data = array()){
		$omise_post['card']['name'] = @$data['name'];
		$omise_post['card']['number'] = @$data['card_number'];
		$expire = explode('/',@$data['expire']);
		$omise_post['card']['expiration_month'] = @$expire[0]*1;
		$omise_post['card']['expiration_year'] = @$expire[1];
		$omise_post['card']['city'] = '';
		$omise_post['card']['postal_code'] = '';
		$omise_post['card']['security_code'] = @$data['cvv'];

		return $omise_post;
	}

	protected function __sendSMS($phone = '',$message = 'Test'){

		//return true;

		if($phone != ''){

			$url = 'http://www.thaibulksms.com/sms_api.php';
			$field['username'] = '0909861366';
			$field['password'] = 'NIckvacos12';
			$field['msisdn'] = $phone;//More 090xxxxxxx,090xxxxxxx
			$field['message'] = $message;
			$field['sender'] = 'limoapp';
			$field['force'] = 'premium';

			$xml_result = $this->__curl($url,'POST',$field);
			$sms = new SimpleXMLElement($xml_result);
			$count = count($sms->QUEUE);
			if($count > 0){
				$count_pass = 0;
				$count_fail = 0;
				$used_credit = 0;
				for($i=0;$i<$count;$i++){
					if($sms->QUEUE[$i]->Status){

						return true;

					}else{	
											
						return false;
					}
				}
				
			}else{

				return false;

			}
		}
	}

	protected function __curl($url='',$type='GET',$field=''){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$url);

		if($type == 'POST'){
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS,$field);
		}
		else{
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
		}

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$server_output = curl_exec ($ch);
		
		curl_close ($ch);

		return $server_output;
	}

	protected function getRawData(){
		$raw_data = file_get_contents('php://input');
		return json_decode($raw_data,true);
	}

	protected function __postToJson(){
		echo json_encode($_POST);die;
	}

	protected function isDriver($driver_id){

		if(!$this->driver->check($driver_id)){
			$json['status'] = 500;
			$json['message'] = 'Driver ID is incorect';

			$this->return_json($json);
		}

		
	}
	
}
