<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends MY_Controller {

	var $path_usr = './uploaded/usr/';

	function __construct()
	{
		parent::__construct();
		$this->load->driver('cache');
		$this->use_segment = 2;
	}

	public function test_fare(){
		$time = (int) ($this->input->get('time')) ? $this->input->get('time') : 1;
		$km = (int) ($this->input->get('km')) ? $this->input->get('km') : 1;
		$fare = $this->order->calculate(1,$km,$time);

		echo '<p>time : '.$time.'</p>'.'<p>km : '.$km.'</p>'.'<p>fare : '.$fare.'</p>';
	}

	public function index()
	{
		$asstes['title'] = 'Limo App - App เรียกรถระดับพรีเมี่ยม';
		$asstes['description'] = 'พวกเราเลือกรถที่ดีที่สุดสำหรับคุณ by Limo App';
		$this->load->view('home',$asstes);
	}

	public function page(){
		$slug = $this->uri->segment(2);

		$asstes['title_display'] = '';
		switch ($slug) {
			case 'refund':
				$asstes['title_display'] = 'Refund';
				break;
			
			case 'privacy':
				$asstes['title_display'] = 'Privacy Policy';
				break;

			case 'term':
				$asstes['title_display'] = 'Term & Condition';
				break;
		}

		$asstes['title'] = ucfirst($asstes['title_display']).' | Limo App - App เรียกรถระดับพรีเมี่ยม';
		$asstes['description'] = 'พวกเราเลือกรถที่ดีที่สุดสำหรับคุณ by Limo App';

		$asstes += $this->page->get($slug);
		$this->load->view('page',$asstes);
	}

	public function test_map()
	{
		$this->load->view('test_map');
	}

	public function upload_image(){
		// print_r($_FILES["file_upload"]);die;
		// $name = $_FILES["file_upload"]["name"];
		$ext = '.jpeg'; //'.'.end((explode(".", $name)));
		$filename = date('Ymdhis').rand(100,900).$ext;
		$target_path = $this->path_usr.$filename;

		if (move_uploaded_file($_FILES['file_upload']['tmp_name'], $target_path)) { 
			$this->resizeCenter($filename);

			$json['status'] = 200;
			$json['message'] = 'Success';
			$json['data']['filename'] = $filename;
			$json['data']['path'] = $this->config->item('usr_url').$filename;
		}else{
			$json['status'] = 500;
	 		$json['message'] = 'Cann\'t Upload File';
		}

		$this->return_json($json);

	}

	private function resizeCenter($image_name){
		
		//require_once APPPATH."third_party/Zebra_Image.php";
		require_once APPPATH."third_party/crop_image.php";

		$image = new Zebra_Image();
		// if you handle image uploads from users and you have enabled exif-support with --enable-exif
		// (or, on a Windows machine you have enabled php_mbstring.dll and php_exif.dll in php.ini)
		// set this property to TRUE in order to fix rotation so you always see images in correct position
		$image->auto_handle_exif_orientation = false;

		// indicate a source image (a GIF, PNG or JPEG file)
		$image->source_path = $this->path_usr.$image_name;

		// indicate a target image
		// note that there's no extra property to set in order to specify the target
		// image's type -simply by writing '.jpg' as extension will instruct the script
		// to create a 'jpg' file
		$image->target_path = $this->path_usr.'thumb/'.$image_name;

		// since in this example we're going to have a jpeg file, let's set the output
		// image's quality
		$image->jpeg_quality = 100;

		// some additional properties that can be set
		// read about them in the documentation
		$image->preserve_aspect_ratio = true;
		$image->enlarge_smaller_images = true;
		$image->preserve_time = true;
		$image->handle_exif_orientation_tag = true;

		// resize the image to exactly 100x100 pixels by using the "crop from center" method
		// (read more in the overview section or in the documentation)
		//  and if there is an error, check what the error is about
		if (!$image->resize(200, 200, ZEBRA_IMAGE_CROP_CENTER)) {

			return false;

		// if no errors
		} else{
			return true;
		}
	}

	public function contact(){
		$message = $this->getMessage($_POST);

		$this->load->library('email');

		$config['charset'] = 'utf-8';
		$config['mailtype'] = 'html';

		$this->email->initialize($config);

		$this->email->from('info@limoapp.me', 'Info Limo app');
		$this->email->to('info@limoapp.me');
		$this->email->bcc('nick.suttipong@gmail.com');

		$this->email->subject('ส่งข้อมูลมาจากการกรอกระบบติดต่อเรา บนเว็บไซต์ Limoapp.me');
		$this->email->message($message);

		if ( ! $this->email->send())
		{
			$j['code'] = 500;
		}
		else{
			$j['code'] = 200;
		}

		echo json_encode($j);
		exit;
	}

	private function getMessage($data){
		$h = '';
		$h .= 'ชื่อผู้ติดต่อ : '.@$data['name'].'<br>';
		$h .= 'อีเมล : <a href="mailto:'.@$data['email'].'">'.@$data['email'].'</a><br>';
		$h .= 'ข้อความ : '.@$data['message'].'<br>';
		return $h;
	}

	public function notfound(){
		echo '404';
	}

	public function reset_all(){
		$sql = 'TRUNCATE `login_log`';
		$this->db->query($sql);
		$sql = 'TRUNCATE `omise_log`;';
		$this->db->query($sql);
		$sql = 'TRUNCATE `order`;';
		$this->db->query($sql);
		$sql = 'TRUNCATE `payment`;';
		$this->db->query($sql);
		$sql = 'TRUNCATE `payment_method`;';
		$this->db->query($sql);
		$sql = 'TRUNCATE `place`;';
		$this->db->query($sql);
		$sql = 'TRUNCATE `user`;';
		$this->db->query($sql);
		$sql = 'TRUNCATE `income`;';
		$this->db->query($sql);
		echo 'Reset Success';
	}

	public function get_retrieve(){
		$return = $this->omise_api->retrieve('chrg_test_5czt91v1i8bdgjc0hb6');
		print_r($return);
	}

	public function refund(){
		$return = $this->omise_api->refund('chrg_test_5czvp37vfmk6f9e5mpv',$this->calAmountOmise(350));
		print_r($return);
	}

	public function get_charges(){
		$return = $this->omise_api->get_charges('chrg_test_5czvye1mzpayf76i8zv');
		print_r($return);
	}

	public function get_customer(){
		$user_id = 1;
		$user = $this->user->get('',$user_id);
		//echo $user['token'];die;
		$return = $this->omise_api->customer_get($user['token']);
		print_r($return);
	}

	public function add_card(){
		$user_id = $this->input->get('user_id');
		if($this->user->check('',$user_id)){
			$data['user_id'] = $user_id;
			$this->load->view('add_card',$data);
		}
		else{
			redirect('/');
			exit;
		}
		
	}

	public function do_add_card(){

		$token = $this->input->post('omise_token');

		$user_id = $this->input->post('user_id');

		$user = $this->user->get('',$user_id);

		if($user != false && $token != ''){
			
			if(!$this->payment_method->check('',$token,$user['id'])){

				$this->omise_api->customer_add_card($user['token'],$token);
				$omise_cust = $this->omise_api->customer_get($user['token']);

				if(isset($omise_cust['cards']['data']) && count($omise_cust['cards']['data']) > 0){

					$card_index = count($omise_cust['cards']['data']) - 1;

					$data['card_number'] = $omise_cust['cards']['data'][$card_index]['last_digits'];
					$data['expire']= $omise_cust['cards']['data'][$card_index]['expiration_month'].'/'.$omise_cust['cards']['data'][$card_index]['expiration_year'];
					$data['cvv'] = null;
					$data['country'] = $omise_cust['cards']['data'][$card_index]['country'];
					$data['token'] = $omise_cust['cards']['data'][$card_index]['id'];
					$data['user_id'] = $user['id'];
					$data['create_on'] = date('Y-m-d H:i:s');
					$data['update_on'] = date('Y-m-d H:i:s');

					$new_id_payment = $this->payment_method->insert($data);

					$json['status'] = 200;
					$json['message'] = 'Add card success!!';
				}
				else{

					$json['status'] = 500;
					$json['message'] = 'Card number is fail';

				}

			}
			else{

				$json['status'] = 500;
				$json['message'] = 'Card number is duplicate';

			}

		}
		else{

			$json['status'] = 500;
			$json['message'] = 'Not find user';

		}

		$this->load->view('do_add_card',$json);	
	}

	public function customer_add_card(){
		$this->load->view('test_omise');	
	}

	public function p_test(){
		$user_id = 1;
		$this->omise_api->setUserID($user_id);
		$user = $this->user->get('',$user_id);

		$token = $this->input->post('omise_token');
		echo $token.'<hr>';die;
		//echo $user['token'];die;
		$return = $this->omise_api->customer_add_card($user['token'],$token);
		print_r($return);
	}

	public function check_payment(){
		$order_id = (int) $this->input->get('order_id');
		$order = $this->order->get('',$order_id,true);
		if($order != false ){ 

			$payments = $this->payment->get_charge('',$order_id);
			$payment = $payments[0];

			if($order[0]['status'] == 3 && $payment['status_payment'] == 3){//Order status = driving to user and waiting payment && Payment Status = Pending Payment
				
				$charge_return = $this->omise_api->get_charges($payment['charge_token']);
				$log = $this->getLog($charge_return);
				//echo $log;print_r($charge_return);die;
				if(@$charge_return['status'] == 'successful'){

					$this->payment->success($payment['payment_id'],'',$payment['user_id'],$log);
					$driver = $this->user->get('',$payment['driver_id']);
					$data_user['income'] = $driver['income'] + $this->order->calculate_income_driver($payment['fare']);
					$this->user->update($payment['driver_id'],$data_user);
					$json['status'] = 200;
					$json['message'] = 'ชำระเงินสำเร็จ';
					
				}else{/*if(@$charge_return['status'] == 'failed'){*/

					$this->payment->fail($payment['payment_id'],$log);
					$json['status'] = 500;
					$json['message'] = 'ชำระเงินไม่สำเร็จ';
				}
				/*elseif(@$charge_return['status'] == 'pending'){

					
					echo 'รอการชำระเงิน';
				}*/

				$this->load->view('payment_omise',$json);	
			}else{
				redirect('/');exit;
			}
		}
		else{
			redirect('/');exit;
		}
	}

	private function getLog($charge_return = array()){
		return @$charge_return['error'].'|'.@$charge_return['status'].'|'.@$charge_return['failure_code'].'|'.@$charge_return['failure_message'];
	}


	public function p(){
		echo '<pre>';
		$payments = $this->payment->cron();
		if(count($payments) > 0){
			//print_r($payments);die;
			foreach ($payments as $payment) {
				//print_r($payment);die;
				$is_pay = false;
				if($payment['card_number'] == 'Cash'){

					$is_pay = true;
					$charge_data = 'Cash';
					
				}else{
					$this->omise_api->setUserID($payment['user_id']);
					$charge_return = $this->omise_api->charges($payment['user_token'], $payment['card_token'], $this->calAmountOmise($payment['fare']));
					//print_r($charge_return);die;
					if(@$charge_return['status'] == 'successful'){

						$charge_data = $charge_return['id'];
						$is_pay = true;

					}elseif(@$charge_return['status'] == 'pending'){
						$charge_data = $charge_return['id'];
						$this->payment->pending($payment['payment_id'],$charge_data,@$charge_return['authorize_uri']);

					}else{
						$this->payment->fail($payment['payment_id'],@$charge_return['error'].'|'.@$charge_return['failure_message']);
					}

				}
				
				if($is_pay){

					$this->payment->success($payment['payment_id'],$charge_data);

					$driver = $this->user->get('',$payment['driver_id']);
					$data_user['income'] = $driver['income'] + $this->order->calculate_income_driver($payment['fare']);
					$this->user->update($payment['driver_id'],$data_user);

					echo '<p>Update Payment ID : '.$payment['payment_id'].' <span style="color:green">Success</span></p><hr>';
				}
				else{
					echo '<p>Update Payment ID : '.$payment['payment_id'].' <span style="color:red">Fail</span></p><hr>';
				}	
			}
		}else{
			echo 'No Payment';
		}
	}

	public function cancel_order_old(){

		$time_del_10_min = time() - 180;
		$date_time_del_10_min = date('Y-m-d H:i:s',$time_del_10_min);
		
		$this->order->update_order_old($date_time_del_10_min);
		$txt = 'Updated Time < '.$date_time_del_10_min;
		echo $txt;
		
		$root = '/home/limoapp/domains/limoapp.me/private_html';
		$path_file = $root."/storage/logs_cron.txt";
		//echo $path_file;die;
		$myfile = fopen($path_file, "w") or die("Unable to open file!");
		fwrite($myfile, $txt);
		fclose($myfile);
	}

	public function c()
	{
		$this->load->helper('form');
		echo '<h1>'.WEB_TITLE.' Clear CACHE System</h1>';
		echo form_open('/appclear');
		//Open Form

		$f_url = array(
						'name'        => 'url',
						'maxlength'   => '100',
						'size'        => '50',
					);

		 echo '<p>URL : '.form_input($f_url).'</p>';
		 //Input

		 $f_password = array(
						'name'        => 'p',
						'maxlength'   => '100',
						'size'        => '50',
					);

		 //echo '<p>Password : '.form_input($f_password).'</p>';
		 //Input

		 echo form_submit('mysubmit', 'Submit');
		 // BTN Submit

		 echo form_close();
		 //Close Form

		if($this->input->post('p') == ''){
			$post_url = $this->input->post('url');
			$url = str_replace(BASE_LINK, '/', $post_url);
			$json['status'] = 'Clear CACHE Complete';
			$json['url'] = $post_url;
			$this->output->delete_cache($url);
		}
		else{
			$json['status'] = 'Password incorrect!!';
		}
		echo json_encode($json);

	}

	// function testSMS(){
	// 	if($this->__sendSMS('0909861366','Test '.date('d m Y H:i'))){
	// 		echo 'Ok';
	// 	}else{
	// 		echo 'No';
	// 	}
	// }
}
