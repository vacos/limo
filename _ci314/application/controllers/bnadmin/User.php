<?php

class User extends MY_Controller {

//////////////////////////SET/////////////////////////////////////

    private $pfile = 'user';
    private $prefix = ''; // field table
    private $page_title = 'User'; 
    private $display = 'table'; //"table" || "box"
    private $db_table = 'user';

    function _setdata() {


        $this->lan = array('th');
        /// "text" || "image" || "inlist" array('inlist',array()) || link array('link','page','img.type')
        $this->table = array(
            'ID' => array('id', 'text'),
            'Name' => array('name', 'text'),
            'Phone' => array('phone', 'text'),
        );
        /// "array('id'=>$id,'image'=>$thumbnail)"
        $this->box_img = array('id' => 'con_id', 'image' => 'con_thumbnail', 'status' => 'con_status');
        /// "text" || "editor" || "file" ||  "listbox" || "listbox-group" || "radio" || "textarea" || "hidden"
        $this->form = array(
            'name' => 'text',
            'phone' => 'text'
        );

        $this->form_label = array('ชื่อ' ,'เบอร์​โทรศัพท์');
        $this->form_label += array_keys($this->form);
        /// "text" || "inlist" 
        $this->form_search = array(
            'keyword' => array('text'),
        );
        
        $this->filed_syn = array();
        
        $this->options = array();
        
        $this->add_link = false;
        $this->edit_link = true;
        $this->status_link = false;
        $this->del_link = false;
        $this->can_edit = true;
    }

///////////////////////////////////////////////////////////////////////////

    private $table = array();
    private $box_img = array();
    private $form = array();
    private $form_label = array();
    private $list_files = array();
    private $form_search = array();
    private $lan = array();
    private $filed_syn = array();
    private $add_link = false;
    private $gallery_link = false;
    private $edit_link = false;
    private $del_link = false;
    private $status_link = false;
    private $export_link = false;

    function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('adminmodel', 'main', TRUE);
        $this->main->set($this->db_table);
        $this->load->library('session');
        $this->_check_login();
    }

    function _check_multi_language(){
        return (isset($this->lan) && count($this->lan) > 1);
    }

    function _check_login() {
        if (!$this->session->userdata('admin_login')) {
            redirect(BASE_LINK_BACK, 'refresh');
        } else {
            $this->_setdata();
            foreach ($this->form as $key => $val) {
                if (is_array($val) && $val[0] == 'file') {
                    $this->list_files[] = $key;
                }
            }
            if ($this->_check_multi_language()&&$this->session->userdata($this->pfile . '_slan')=="") {
                $this->session->set_userdata(array($this->pfile . '_slan' => $this->lan[0]));
            }
        }
    }

    function index() {
        
        $data_body['total_rows'] = 0;
        if ( $this->input->post('keyword') )
		{
            $keyword = $this->input->post('keyword',true);
            $members = $this->user->search('',$keyword,0,0,1000);
            if($members != false){
                $data_body['result'] = $members;
                $data_body['total_rows'] = count($members);
                $data_body['key_search']['keyword'] = $keyword;
                
            }
            //print_r($members);
        }

        

        $data_body['table'] = $this->table;
        $data_body['box_img'] = $this->box_img;
        $data_body['pfile'] = $this->pfile;
        $data_body['prefix'] = $this->prefix;
        $data_body['add_link'] = $this->add_link;
        $data_body['gallery_link'] = $this->gallery_link;
        $data_body['edit_link'] = $this->edit_link;
        $data_body['del_link'] = $this->del_link;
        $data_body['status_link'] = $this->status_link;
        $data_body['export_link'] = $this->export_link;
        $data_body['title'] = $this->page_title;
        $data_body['form_search'] = $this->form_search;
        $data_body['lan'] = $this->lan;
        $data_body['display'] = $this->display;
        
        

        $data_main['body'] = $this->load->view('admin/'.$this->pfile.'/lists', $data_body, true);
        $this->load->view('admin/layout', $data_main);
    }

    function post() {
        $post = $_POST;
        $data['mem_comment'] = $post['mem_comment'];
        

        if($post['action'] == 'update'){
            $filter['mem_id'] = $post['id'];
            $this->main->edit($filter,$data);
            redirect(BASE_LINK_BACK . 'member/edit/'.$filter['mem_id'], 'refresh');
        }
        
    }

    function edit() {
        $id = $this->uri->segment(4);
        $data_body['form'] = $this->form;
        $data_body['options'] = $this->options;
        $data_body['label'] = $this->form_label;
        $data_body['pfile'] = $this->pfile;
        $data_body['prefix'] = $this->prefix;
        $data_body['redirect'] = $this->pfile . '/index';
        $data_body['title'] = $this->page_title;
        $data_body['breadcrumbs'] = array('Edit'=>'');
        $data_body['display'] = 'form';
        $data_body['lan'] = $this->lan;
        $data_body['primary_id'] = $this->prefix . 'id';
        $cd[$this->prefix . 'id'] = $id;
        if ($this->_check_multi_language()) {
            $cd[$this->prefix . 'lan'] = $this->session->userdata($this->pfile . '_slan');
        }
        $sql = "SELECT * FROM user WHERE type='0' and id = ".$id;
        $result = $this->db->query($sql)->row();

        $data_body['orders'] = $this->order->history($id);

        $data_body['record'] = $result;
        $data_body['can_edit'] = $this->can_edit;
        
        //echo html_entity_decode($data_body['record']->lea_banner);
        $data_main['body'] = $this->load->view('admin/'.$this->pfile.'/edit', $data_body, true);
        $this->load->view('admin/layout', $data_main);
    }

    public function change(){
        $id = (int) $this->input->get('id');
        $data['type'] = 2;
        $this->user->update($id,$data);

        redirect(BASE_LINK_BACK . 'driver/edit/'.$id, 'refresh');
    }

}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */