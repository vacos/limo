<?php

class Singlepage extends CI_Controller {

//////////////////////////SET/////////////////////////////////////
    private $pfile = 'singlepage';
    private $prefix = 'sin_'; // field table
    private $page_title = ''; 
    private $display = 'table'; //"table" || "box"
    private $db_table = 'tc_single_pages';

    function _setdata() {
        $this->lan = array('th','en');
       /// "text" || "editor" || "file" ||  "listbox" || "listbox-group" || "radio" || "textarea" || "hidden"
        $this->form = array(
            'sin_title' => 'text',
            'sin_alias' => 'text',
            'sin_introduction' => 'text',
            'sin_link' => 'text',
            'sin_image' => array('file', 'image'),
            'sin_thumbnail' => array('file', 'image'),
            'sin_status' => array('listbox', array('1' => 'Show', '0' => 'Hide'))
        );
        $this->form_label = array('Title', 'Issue', 'Date', 'URL', 'Image (435x570)', 'Thumbnail (75x100)', 'Status', '');

        
        $this->filed_syn = array('sin_image', 'sin_status');
        
    }

///////////////////////////////////////////////////////////////////////////

    private $form = array();
    private $form_label = array();
    private $lan = array();
    private $filed_syn = array();

    function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('adminmodel', 'main', TRUE);
        $this->main->set($this->db_table);
        $this->load->library('session');
        $this->_check_login();
    }

    function _check_login() {
        if (!$this->session->userdata('admin_login')) {
            redirect(BASE_LINK_BACK, 'refresh');
        } else {
            $this->_setdata();
            foreach ($this->form as $key => $val) {
                if (is_array($val) && $val[0] == 'file') {
                    $this->list_files[] = $key;
                }
            }
            if ($this->_check_multi_language()&&$this->session->userdata($this->pfile . '_slan')=="") {
                $this->session->set_userdata(array($this->pfile . '_slan' => $this->lan[0]));
            }
        }
    }


    function index() {
        $id = $this->uri->segment(4);
        $data_body['form'] = $this->form;
        $data_body['label'] = $this->form_label;
        $data_body['pfile'] = $this->pfile;
        $data_body['prefix'] = $this->prefix;
        $data_body['redirect'] = $this->pfile . '/index';
        $data_body['title'] = $this->page_title;
        $data_body['breadcrumbs'] = array('Edit'=>'');
        $data_body['display'] = 'form';
        $data_body['lan'] = $this->lan;
        $data_body['primary_id'] = $this->prefix . 'id';

        $data_body['btn_back'] =false;
        $cd[$this->prefix . 'id'] = $id;
        if ($this->_check_multi_language()) {
            $cd[$this->prefix . 'lan'] = $this->session->userdata($this->pfile . '_slan');
        }
        $data_body['record'] = $this->main->getrecord($cd)->row();
        $data_main['body'] = $this->load->view('admin/form_edit', $data_body, true);
        $this->load->view('admin/layout', $data_main);
    }

    function post() {
        foreach ($_POST as $key => $val) {
            if (in_array($key,array_keys($this->form))||$key==$this->prefix."lan") {
                $data[$key] = $val;//str_replace("\r\n", "", $val);
            }
        }
        $data[$this->prefix . 'modifiedon'] = date('Y-m-d H:i:s');
        $this->load->library("upload");
        $re_update = array();
        foreach ($_FILES as $key => $val) {
            $prefix = explode('_', $key);
            if (isset($val['name'])) {
                $this->upload->upload($val);
                if ($this->upload->uploaded) {
                    $this->upload->file_new_name_body = $prefix[1] . '-' . date('dmyhis') . rand(100, 999);
                    $this->upload->Process('./uploaded/content/');
                    if ($this->upload->processed) {
                        $re_update[] = $key;
                        $data[$key] = $this->upload->file_dst_name;
                        $this->upload->clean();
                    } else {
                        echo 'error : ' . $this->upload->error;
                        exit;
                    }
                }
            }
        }
       
        $id = $this->input->post('id');
        $record = $this->main->getrecord(array($this->prefix . 'id' => $id))->row();
        foreach ($re_update as $val) {
            if (file_exists('./uploaded/content/' . $record->$val) && $record->$val != "") {
                unlink('./uploaded/content/' . $record->$val);
            }
        }
        $cd[$this->prefix . 'id'] = $id;
        if ($this->_check_multi_language()) {
            $cd[$this->prefix . 'lan'] = $data[$this->prefix . 'lan'];
        }
        $this->main->edit($cd, $data);


        if ($this->_check_multi_language()) {
            $row = $this->main->getrecord(array($this->prefix . 'id' => $id, $this->prefix . 'lan' => $data[$this->prefix . 'lan']))->row_array();
            $data = array();

            if (isset($this->filed_syn) && is_array($this->filed_syn) && count($this->filed_syn) > 0) {

                foreach ($this->filed_syn as $filed) {
                    if (isset($row[$filed])) {
                        $data[$filed] = $row[$filed];
                    }
                }
            }

            if (count($data) > 0) {
                $this->main->edit(array($this->prefix . 'id' => $row[$this->prefix . 'id']), $data);
            }
        }
        redirect(BASE_LINK_BACK . $this->input->post('redirect'), 'refresh');
    }

    function set_language($lan) {
        $lan = (!in_array($lan, $this->lan)) ? $this->lan[0] : $lan;
        $data = array($this->pfile . '_slan' => $lan);
        $this->session->set_userdata($data);
        redirect($_SERVER["HTTP_REFERER"], 'refresh');
        
    }
    
    function _check_multi_language(){
        return (isset($this->lan) && count($this->lan) > 1);
    }

    function deletefile() {
        $id = $this->uri->segment(5);
        $field = $this->uri->segment(4);
        $row = $this->main->getrecord(array($this->prefix.'id'=>$id))->row();
        if (isset($row->$field) && $row->$field != "" && file_exists('./uploaded/content/' . $row->$field)) {
            unlink('./uploaded/content/' . $row->$field);
            $data[$field] ="";
            $this->main->edit(array($this->prefix.'id'=>$id), $data);
            redirect($_SERVER["HTTP_REFERER"], 'refresh');
        }else{
            echo "";
        }
    }
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */