<?php

class Order extends MY_Controller {

//////////////////////////SET/////////////////////////////////////

    private $pfile = 'order';
    private $prefix = 'ord_'; // field table
    private $page_title = 'Order'; 
    private $display = 'table'; //"table" || "box"
    private $db_table = 'order';

    function _setdata() {


        $this->lan = array('th');
        /// "text" || "image" || "inlist" array('inlist',array()) || link array('link','page','img.type')
        $this->table = array(
            'Number' => array('ord_number', 'text')
        );
        /// "array('id'=>$id,'image'=>$thumbnail)"
        $this->box_img = array('id' => 'con_id', 'image' => 'con_thumbnail', 'status' => 'con_status');
        /// "text" || "editor" || "file" ||  "listbox" || "listbox-group" || "radio" || "textarea" || "hidden"
        $this->form = array(
            'ord_number' => 'text'
        );

        $this->form_label = array('Order Number' , 'Order Status');
        $this->form_label += array_keys($this->form);
        /// "text" || "inlist" 
        $this->form_search = array(
            'number' => array('text'),
        );
        
        $this->filed_syn = array();
        
        $this->options = array();
        
        $this->add_link = false;
        $this->edit_link = true;
        $this->status_link = false;
        $this->del_link = false;
        $this->can_edit = false;
    }

///////////////////////////////////////////////////////////////////////////

    private $table = array();
    private $box_img = array();
    private $form = array();
    private $form_label = array();
    private $list_files = array();
    private $form_search = array();
    private $lan = array();
    private $filed_syn = array();
    private $add_link = false;
    private $gallery_link = false;
    private $edit_link = false;
    private $del_link = false;
    private $status_link = false;
    private $export_link = false;

    function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('adminmodel', 'main', TRUE);
        $this->main->set($this->db_table);
        $this->load->library('session');
        $this->_check_login();
    }

    function _check_multi_language(){
        return (isset($this->lan) && count($this->lan) > 1);
    }

    function _check_login() {
        if (!$this->session->userdata('admin_login')) {
            redirect(BASE_LINK_BACK, 'refresh');
        } else {
            $this->_setdata();
            foreach ($this->form as $key => $val) {
                if (is_array($val) && $val[0] == 'file') {
                    $this->list_files[] = $key;
                }
            }
            if ($this->_check_multi_language()&&$this->session->userdata($this->pfile . '_slan')=="") {
                $this->session->set_userdata(array($this->pfile . '_slan' => $this->lan[0]));
            }
        }
    }

    function index() {
        
        redirect(BASE_LINK_BACK ,'refresh');
    }

    function edit() {
        $id = $this->uri->segment(4);
        $data_body['form'] = $this->form;
        $data_body['options'] = $this->options;
        $data_body['label'] = $this->form_label;
        $data_body['pfile'] = $this->pfile;
        $data_body['prefix'] = $this->prefix;
        $data_body['redirect'] = $this->pfile . '/index';
        $data_body['title'] = $this->page_title;
        $data_body['breadcrumbs'] = array('Edit'=>'');
        $data_body['display'] = 'form';
        $data_body['lan'] = $this->lan;
        $data_body['primary_id'] = $this->prefix . 'id';
        $cd[$this->prefix . 'id'] = $id;
        if ($this->_check_multi_language()) {
            $cd[$this->prefix . 'lan'] = $this->session->userdata($this->pfile . '_slan');
        }

        $data_body['orders'] = $this->order->get('',$id,true);

        $cars = $this->car->listing();
        foreach ($cars as $car) {
            if($car['id'] == $data_body['orders'][0]['car_type']){
                $data_body['orders'][0]['car_type_show'] = $car;
            }
        }

        $data_body['orders'][0]['payment_by'] = $this->payment_method->get($data_body['orders'][0]['id_payment']);

        //print_r($data_body['orders'][0]);die;

        $data_body['can_edit'] = $this->can_edit;

        $data_body['vote'] = $this->driver->getVote($id);
        
        //echo html_entity_decode($data_body['record']->lea_banner);
        $data_main['body'] = $this->load->view('admin/'.$this->pfile.'/edit', $data_body, true);
        $this->load->view('admin/layout', $data_main);
    }

    function update_to_success(){
        $order_id = (int) $this->input->get('order_id',true);
        $timeline_id = (int) $this->input->get('timeline_id',true);
        $this->order->update_status_success($order_id,$timeline_id);

        $order_data = $this->order->get($order_id);
        $booking_id = $order_data[0]['boo_id'];

        $booking_data = $this->course->get_booking($booking_id);
        $players = $order_data[0]['ord_player'];
        $course_id = $booking_data[0]['cou_id'];
        $courses = $this->course->get($course_id);

        if($timeline_id > 0){
            $count_player = $this->timeline->count_players($timeline_id);
            $check_player = $count_player + 1;
        }
        else{
            $status_timeline = 2;//Show on Friend
            $mem_id = $order_data[0]['mem_id'];
            $this->timeline->insert($booking_id,$mem_id,$status_timeline,$players);
            // Insert Timeline
            $check_player = $players + 1;
        }
        //echo $check_player.' '.$courses[0]['cou_max_player'];
        if($check_player <= $courses[0]['cou_max_player']){
            $this->course->update_booking_available($booking_id);
        }

        // $member = $this->member->get('',$order_data[0]['mem_id']);
        // $member[0]['email'];
        

        $user = 'ADMIN UPDATE TO SUCCESS';
        $this->order->insert_log(0,$order_id,$user);
        redirect(BASE_LINK_BACK . $this->pfile . '/edit/' . $order_id .'?t='.time() , 'refresh');
    }

}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */