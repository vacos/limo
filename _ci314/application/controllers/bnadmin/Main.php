<?php
class Main extends MY_Controller {

       function __construct()
       {
            parent::__construct();
            // load helper
            $this->load->helper('url');
            // load model
            $this->load->model('adminmodel','admin',TRUE);
            $this->admin->set('tc_admins');
            // load session
            $this->load->library('session');
        }
        
        function index()
        {
            $data['_is_maintenance']  = false;
            $sql = "SELECT * FROM `maintenance` WHERE `id` = 1 AND `status` = 1";
            $record = $this->db->query($sql);
            if($record->num_rows() == 0){
                $record = $record->row();
                $data['_is_maintenance']  = true;        
            }

            if($this->session->userdata('admin_login')){
                redirect(BASE_LINK_BACK.'main/home', 'refresh');
            }
            $this->load->view('admin/login',$data);
        }
 
        function login_post(){
            $record = $this->admin->getrecord(array('adm_username'=>$_POST['username'],'adm_password'=>md5($_POST['password'])));
            if($record->num_rows()==1){
                $record = $record->row();
                $admindata = array('admin_login' => true,'admin_level'=>$record->adm_level,'admin_user'=>$record->adm_username,'admin_id'=>$record->adm_id);
                $this->session->set_userdata($admindata);                
                redirect(BASE_LINK_BACK.'main/home', 'refresh');
                exit;
            }else{
                redirect(BASE_LINK_BACK.'main', 'refresh'); exit;
            }
        }
        function logout(){
            $admindata = array('admin_login' => '','admin_level'=>'','admin_user'=>'','admin_id'=>'');
            $this->session->set_userdata($admindata);
            $this->session->sess_destroy();
            redirect(BASE_LINK_BACK, 'refresh');
        }
        function _check_login(){
            if(!$this->session->userdata('admin_login')):
                redirect(BASE_LINK_BACK, 'refresh');
            endif;
        }
        function home(){
            $this->_check_login();
            $data['title'] = "Dashboard";
            $data['body'] =$this->load->view('admin/home',null,true);
            $this->load->view('admin/layout',$data);
        }
        function password(){
            $this->_check_login();
            $data['title'] = "Edit Profile";
            $data['breadcrumbs'] = array('Edit Profile'=>'main/password');
            $data['err'] = $this->uri->segment(4);
            $data_main['body'] = $this->load->view('admin/password',$data,true);
            $this->load->view('admin/layout',$data_main);
        }
        function password_post(){
            $this->_check_login();
            $record = $this->admin->getrecord(array('adm_id'=>$this->session->userdata('admin_id')));
            if($record->num_rows()==1){
                $data['adm_password']= md5($_POST['news_pass']);
                $this->admin->edit(array('adm_id'=>$record->row()->adm_id),$data);
                $this->logout();
                redirect(BASE_LINK_BACK, 'refresh');
            }else{
                redirect(BASE_LINK_BACK.'main/password/err', 'refresh');
            }
        }
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */