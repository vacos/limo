<?php

class Ads extends MY_Controller {

//////////////////////////SET/////////////////////////////////////

    private $pfile = 'ads';
    private $folder = 'features';
    private $prefix = 'fea_'; // field table
    private $page_title = 'Ads'; 
    private $display = 'table'; //"table" || "box"
    private $db_table = 'features';
    private $slug_module = SLUG_MOD_ADS;

    function _setdata() {


        $this->lan = array('th');
        /// "text" || "image" || "inlist" array('inlist',array()) || link array('link','page','img.type')
        $this->table = array(
            'ID' => array('fea_id', 'text'),
            'Slug' => array('fea_slug_spacail', 'text')
        );
        /// "array('id'=>$id,'image'=>$thumbnail)"
        $this->box_img = array('id' => 'con_id', 'image' => 'con_thumbnail', 'status' => 'con_status');
        /// "text" || "editor" || "file" || "image" ||  "listbox" || "listbox-group" || "listbox-group-multiple" || "radio" || "textarea" || "hidden"
        $this->form = array(
            'fea_use' => 'image',
            'fea_custom_1' => 'text'
        );
        $this->form_label = array('Image','Link (http://xxx.xx)');
        $this->form_label += array_keys($this->form);
        /// "text" || "inlist" 
        $this->form_search = array();
        
        $this->filed_syn = array();
        
        
        $this->add_link = false;
        $this->edit_link = true;
        $this->status_link = true;
        $this->del_link = false;
    }

///////////////////////////////////////////////////////////////////////////

    private $table = array();
    private $box_img = array();
    private $form = array();
    private $form_label = array();
    private $list_files = array();
    private $form_search = array();
    private $lan = array();
    private $filed_syn = array();
    private $add_link = false;
    private $gallery_link = false;
    private $edit_link = false;
    private $del_link = false;
    private $status_link = false;
    private $export_link = false;

    function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('adminmodel', 'main', TRUE);
        $this->main->set($this->db_table);
        $this->load->library('session');
        $this->_check_login();
    }

    function _check_multi_language(){
        return (isset($this->lan) && count($this->lan) > 1);
    }

    function _check_login() {
        if (!$this->session->userdata('admin_login')) {
            redirect(BASE_LINK_BACK, 'refresh');
        } else {
            $this->_setdata();
            foreach ($this->form as $key => $val) {
                if (is_array($val) && $val[0] == 'file') {
                    $this->list_files[] = $key;
                }
            }
            if ($this->_check_multi_language()&&$this->session->userdata($this->pfile . '_slan')=="") {
                $this->session->set_userdata(array($this->pfile . '_slan' => $this->lan[0]));
            }
        }
    }

    function index() {
        $sql = "SELECT * FROM features where fea_slug = '".$this->slug_module."'";
        $result = $this->db->query($sql);

        //$result = $query->result_array();
 
        $data_body['result'] = $result;

        $data_body['table'] = $this->table;
        $data_body['box_img'] = $this->box_img;
        $data_body['pfile'] = $this->pfile;
        $data_body['prefix'] = $this->prefix;
        $data_body['add_link'] = $this->add_link;
        $data_body['gallery_link'] = $this->gallery_link;
        $data_body['edit_link'] = $this->edit_link;
        $data_body['del_link'] = $this->del_link;
        $data_body['status_link'] = $this->status_link;
        $data_body['export_link'] = $this->export_link;
        $data_body['title'] = $this->page_title;
        $data_body['form_search'] = $this->form_search;
        $data_body['lan'] = $this->lan;
        $data_body['display'] = $this->display;

        $data_main['body'] = $this->load->view('admin/'.$this->folder.'/lists', $data_body, true);
        $this->load->view('admin/layout', $data_main);
    }

    function post() {
        $post = $_POST;
        
        $config['upload_path']          =   './uploaded/ads/';
        $config['allowed_types']        =   'gif|jpg|png';
        $config['file_name']            =   date('Ymdhis').rand(100,900);
        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('fea_use'))
        {
            $data['fea_use'] = $post['old_fea_use'];
        }
        else
        {
            $data['fea_use'] = $this->upload->data('file_name');
        }


        if($post['action'] == 'create'){}


        elseif($post['action'] == 'update'){
            $filter['fea_id'] = $post['id'];
            $data['fea_custom_1'] = $post['fea_custom_1'];
            $this->main->edit($filter,$data);
            redirect($_SERVER["HTTP_REFERER"], 'refresh');
            exit;
            
        }
        redirect(BASE_LINK_BACK . $this->pfile, 'refresh');
    }

    private function toClean($str=''){
        return htmlentities($str);
        
    }

    private function toUse($val = array(),$action = 'keep'){
        if($action == 'keep'){
            return json_encode($val);
        }
        else{
            return json_decode($val,true);
        }
        
    }
    
    private function toAscii($str, $replace=array(), $delimiter='-') {
        setlocale(LC_ALL, 'en_US.UTF8');
        if( !empty($replace) ) {
            $str = str_replace((array)$replace, ' ', $str);
        }

        $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
        $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
        $clean = strtolower(trim($clean, '-'));
        $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);
        return $clean;
        
    }

    function delete(){
        $sql = "DELETE FROM category WHERE cat_id = ".$this->uri->segment(4);
        $this->db->query($sql);
        redirect($_SERVER["HTTP_REFERER"], 'refresh');
    }

    function del_all() {
        if (isset($_POST['del']) && count($_POST['del']) > 0) {
            foreach ($_POST['del']as $val) {
                $sql = "DELETE FROM category WHERE cat_id = $val";
                $this->db->query($sql);
            }
        }
        redirect($_SERVER["HTTP_REFERER"], 'refresh');
    }

    function status($id, $status) {
        $sql = "UPDATE features SET fea_status = '$status' WHERE fea_id = $id";
        $this->db->query($sql);
        redirect($_SERVER["HTTP_REFERER"], 'refresh');
    }

    function add() {
        $data_body['form'] = $this->form;
        $data_body['label'] = $this->form_label;
        $data_body['pfile'] = $this->pfile;
        $data_body['prefix'] = $this->prefix;
        $data_body['redirect'] = $this->pfile . '/index';
        $data_body['title'] = $this->page_title;
        $data_body['breadcrumbs'] = array('Add'=>'');
        $data_body['display'] = 'form';
        $data_body['lan'] = $this->lan;
        $data_main['body'] = $this->load->view('admin/'.$this->pfile.'/add', $data_body, true);
        $this->load->view('admin/layout', $data_main);
    }

    function edit() {
        $id = $this->uri->segment(4);
        $data_body['form'] = $this->form;
        //$data_body['options'] = $this->options;
        $data_body['label'] = $this->form_label;
        $data_body['pfile'] = $this->pfile;
        $data_body['prefix'] = $this->prefix;
        $data_body['redirect'] = $this->pfile . '/index';
        $data_body['title'] = $this->page_title;
        $data_body['breadcrumbs'] = array('Edit'=>'');
        $data_body['display'] = 'form';
        $data_body['lan'] = $this->lan;
        $data_body['primary_id'] = $this->prefix . 'id';
        $cd[$this->prefix . 'id'] = $id;
        if ($this->_check_multi_language()) {
            $cd[$this->prefix . 'lan'] = $this->session->userdata($this->pfile . '_slan');
        }
        $sql = "SELECT * FROM features WHERE fea_id = ".$id;
        $result = $this->db->query($sql)->row();

        $data_body['record'] = $result;
        $data_main['body'] = $this->load->view('admin/'.$this->folder.'/edit', $data_body, true);
        $this->load->view('admin/layout', $data_main);
    }



    private function getOptions(){
        $sql = "SELECT * FROM category";
        $query = $this->db->query($sql);
        $return = array();
        foreach ($query->result_array() as $value) {
            $sql = "SELECT * FROM clip WHERE cli_status = 1 AND cat_parent_id = ".$value['cat_id'];
            $list_clips = $this->db->query($sql);
            foreach ($list_clips->result_array() as $value_clip) {
                $return[$value['cat_slug']][$value_clip['cli_id']] = $value_clip['cli_title'];
            }
        }
        return $return;
    }

}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */