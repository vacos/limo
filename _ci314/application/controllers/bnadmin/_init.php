<?php

class Init extends CI_Controller {

//////////////////////////SET/////////////////////////////////////

    private $pfile = 'init';
    private $prefix = 'con_'; // field table
    private $page_title = 'Init'; 
    private $mod_id = '';
    private $display = 'table'; //"table" || "box"
    private $db_table = 'tc_contents';

    function _setdata() {
        $this->lan = array('th','en');
        /// "text" || "image" || "inlist" array('inlist',array()) || link array('link','page','img.type')
        $this->table = array(
            'ID' => array('con_id', 'text'),
            'Title' => array('con_title', 'text'),
            'Thumbnail' => array('con_thumbnail', 'image'),
            'Modify on' => array('con_modifiedon', 'text'),
            'Order' => array('con_order', 'order')
        );
        /// "array('id'=>$id,'image'=>$thumbnail)"
        $this->box_img = array('id' => 'con_id', 'image' => 'con_thumbnail', 'status' => 'con_status');
        /// "text" || "editor" || "file" ||  "listbox" || "listbox-group" || "radio" || "textarea" || "hidden"
        $this->form = array(
            'con_title' => 'text',
            'con_alias' => 'text',
            'con_introduction' => 'text',
            'con_link' => 'text',
            'con_image' => array('file', 'image'),
            'con_thumbnail' => array('file', 'image'),
            'con_status' => array('listbox', array('1' => 'Show', '0' => 'Hide')),
            'con_module' => array('hidden', $this->mod_id)
        );
        $this->form_label = array('Title', 'Issue', 'Date', 'URL', 'Image (435x570)', 'Thumbnail (75x100)', 'Status', '');
        $this->form_label += array_keys($this->form);
        /// "text" || "inlist" 
        $this->form_search = array(
            'Keyword' => array('text', array('con_id', 'con_title')),
            'Category' => array('inlist', 'con_category', array('1' => 'L1', '2' => 'L2', '3' => 'L3'))
        );
        
        $this->filed_syn = array('con_image','con_thumbnail', 'con_status');
        
        
        $this->add_link = $this->pfile . '/add';
        $this->edit_link = true;
        $this->status_link = true;
        $this->del_link = true;
    }

///////////////////////////////////////////////////////////////////////////

    private $table = array();
    private $box_img = array();
    private $form = array();
    private $form_label = array();
    private $list_files = array();
    private $form_search = array();
    private $lan = array();
    private $filed_syn = array();
    private $add_link = false;
    private $gallery_link = false;
    private $edit_link = false;
    private $del_link = false;
    private $status_link = false;
    private $export_link = false;

    function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('adminmodel', 'main', TRUE);
        $this->main->set($this->db_table);
        $this->load->library('session');
        $this->_check_login();
    }

    function _check_login() {
        if (!$this->session->userdata('admin_login')) {
            redirect(BASE_LINK_BACK, 'refresh');
        } else {
            $this->_setdata();
            foreach ($this->form as $key => $val) {
                if (is_array($val) && $val[0] == 'file') {
                    $this->list_files[] = $key;
                }
            }
            if ($this->_check_multi_language()&&$this->session->userdata($this->pfile . '_slan')=="") {
                $this->session->set_userdata(array($this->pfile . '_slan' => $this->lan[0]));
            }
        }
    }

    function index() {
        
        $uri_segment = 4;
        $per_page = 10;
        $filter = array($this->prefix.'status !='=>4,'order' => array($this->prefix . 'order' => 'asc'));
        
        if($this->mod_id!=''){
            $filter[$this->prefix . 'module'] = $this->mod_id;
        }
        if($this->_check_multi_language()){
            $filter[$this->prefix . 'lan'] = $this->session->userdata($this->pfile . '_slan');
        }
        
        
        //Search
        $link = "";
        $key_search = "";
        if ($this->uri->segment($uri_segment) == "search") {
            $uri_segment++;
            $link = "search/";
            $filter['words'] = $this->prefix . "id != ''";
            if (isset($_POST) && count($_POST) > 0) {
                foreach ($this->form_search as $key => $val) {
                    $key_search[$key] = $this->input->post($key);
                    if ($key_search[$key] != "") {
                        $filter['words'] .= 'AND (';
                        if (is_array($val[1])) {
                            $i = 1;
                            foreach ($val[1] as $val2) {
                                $filter['words'] .= ($val[0] == "text") ? $val2 . " like '%" . $key_search[$key] . "%' " : $val2 . " = '" . $key_search[$key] . "' ";
                                $filter['words'] .= (count($val[1]) != $i) ? ' OR ' : '';
                                $i++;
                            }
                        } else {
                            //$filter['words'] .= ($val[0] == "text") ? $val[1] . " like '%" . $key_search[$key] . "%' " : $val[1] . " = '" . $key_search[$key] . "' ";
                            if($val[0] == "text"){
                                $filter['words'] .= $val[1] . " like '%" . $key_search[$key] . "%' ";
                            }elseif($val[0] == "sdate"){
                                $filter['words'] .= $val[1] . ">= '" . $key_search[$key] . "' ";
                            }elseif($val[0] == "edate"){
                                $filter['words'] .= $val[1] . "<= '" . $key_search[$key] . "' ";
                            }else{////if($val[0] == "text"){
                                $filter['words'] .= $val[1] . " like '%" . $key_search[$key] . "%' ";
                            }
                        }
                        $filter['words'] .= ') ';
                    }
                }
                $this->session->set_userdata(array('ses_word' => $filter['words'], 'key_search' => $key_search));
            } else {
                $filter['words'] = $this->session->userdata('ses_word');
                $key_search = $this->session->userdata('key_search');
            }
        }
        //////////////////End Search

        $offset = ($this->uri->segment($uri_segment) != "") ? $this->uri->segment($uri_segment) : 0;
        //if($this->display=='box'){$per_page='';$offset='';}
        $result = $this->main->getlist($per_page, $offset, $filter);
 
        
        $this->load->library('pagination');
        $config['base_url'] = BASE_LINK_BACK . $this->pfile . '/index/' . $link;
        $config['total_rows'] = $this->main->countrow($filter);
        $config['per_page'] = $per_page;
        $config['uri_segment'] = $uri_segment;
        $this->pagination->initialize($config);
        $data_body['pagination'] = $this->pagination->create_links();
        $data_body['result'] = $result;
        $data_body['offset'] = $offset;
        $data_body['per_page'] = $per_page;
        $data_body['total_rows'] = $config['total_rows'];

        $data_body['table'] = $this->table;
        $data_body['box_img'] = $this->box_img;
        $data_body['pfile'] = $this->pfile;
        $data_body['prefix'] = $this->prefix;
        $data_body['add_link'] = $this->add_link;
        $data_body['gallery_link'] = $this->gallery_link;
        $data_body['edit_link'] = $this->edit_link;
        $data_body['del_link'] = $this->del_link;
        $data_body['status_link'] = $this->status_link;
        $data_body['export_link'] = $this->export_link;
        $data_body['title'] = $this->page_title;
        $data_body['form_search'] = $this->form_search;
        $data_body['key_search'] = $key_search;
        $data_body['lan'] = $this->lan;
        $data_body['display'] = $this->display;

        $data_main['body'] = $this->load->view('admin/' . $this->display . '_list', $data_body, true);
        $this->load->view('admin/layout', $data_main);
    }

    function status($id, $status) {
        $record = $this->main->getrecord(array($this->prefix . 'id' => $id));
        $status = ($status == "1") ? '0' : '1';
        $this->main->edit(array($this->prefix . 'id' => $id), array($this->prefix . 'status' => $status));
        redirect($_SERVER["HTTP_REFERER"], 'refresh');
    }

    function delete($id) {
        $row = $this->main->getrecord(array($this->prefix . 'id' => $id))->row();
        foreach ($this->list_files as $field) {
            if (isset($row->$field) && $row->$field != "" && file_exists('./uploaded/content/' . $row->$field)) {
                unlink('./uploaded/content/' . $row->$field);
            }
        }
        $this->main->edit(array($this->prefix . 'id' => $id),array($this->prefix.'status'=>4));
        redirect($_SERVER["HTTP_REFERER"], 'refresh');
    }

    function add() {
        $data_body['form'] = $this->form;
        $data_body['label'] = $this->form_label;
        $data_body['pfile'] = $this->pfile;
        $data_body['prefix'] = $this->prefix;
        $data_body['redirect'] = $this->pfile . '/index';
        $data_body['title'] = $this->page_title;
        $data_body['breadcrumbs'] = array('Add'=>'');
        $data_body['display'] = 'form';
        $data_body['lan'] = $this->lan;
        $data_main['body'] = $this->load->view('admin/form_add', $data_body, true);
        $this->load->view('admin/layout', $data_main);
    }

    function edit() {
        $id = $this->uri->segment(4);
        $data_body['form'] = $this->form;
        $data_body['label'] = $this->form_label;
        $data_body['pfile'] = $this->pfile;
        $data_body['prefix'] = $this->prefix;
        $data_body['redirect'] = $this->pfile . '/index';
        $data_body['title'] = $this->page_title;
        $data_body['breadcrumbs'] = array('Edit'=>'');
        $data_body['display'] = 'form';
        $data_body['lan'] = $this->lan;
        $data_body['primary_id'] = $this->prefix . 'id';
        $cd[$this->prefix . 'id'] = $id;
        if ($this->_check_multi_language()) {
            $cd[$this->prefix . 'lan'] = $this->session->userdata($this->pfile . '_slan');
        }
        $data_body['record'] = $this->main->getrecord($cd)->row();
        $data_main['body'] = $this->load->view('admin/form_edit', $data_body, true);
        $this->load->view('admin/layout', $data_main);
    }

    function post() {
        foreach ($_POST as $key => $val) {
            if (in_array($key,array_keys($this->form))||$key==$this->prefix."lan") {
                $data[$key] = $val;//str_replace("\r\n", "", $val);
            }
        }
        $data[$this->prefix . 'modifiedon'] = date('Y-m-d H:i:s');
        $this->load->library("upload");
        $re_update = array();
        foreach ($_FILES as $key => $val) {
            $prefix = explode('_', $key);
            if (isset($val['name'])) {
                $this->upload->upload($val);
                if ($this->upload->uploaded) {
                    $this->upload->file_new_name_body = $prefix[1] . '-' . date('dmyhis') . rand(100, 999);
                    $this->upload->Process('./uploaded/content/');
                    if ($this->upload->processed) {
                        $re_update[] = $key;
                        $data[$key] = $this->upload->file_dst_name;
                        $this->upload->clean();
                    } else {
                        echo 'error : ' . $this->upload->error;
                        exit;
                    }
                }
            }
        }
        if ($this->input->post('action') == "create") {
            $record = $this->main->getrecord(array($this->prefix . 'module' => $this->mod_id, 'order' => array($this->prefix . 'order' => 'desc')))->row_array();
            $data[$this->prefix . 'order'] = (count($record) > 0 && $record[$this->prefix . 'order'] != '') ? $record[$this->prefix . 'order'] + 1 : 1;
            $data[$this->prefix . 'createdon'] = date('Y-m-d H:i:s');
            $id = $this->main->add($data);
            if ($this->_check_multi_language()) {
                $data[$this->prefix . 'id'] = $id;
                foreach ($this->lan as $ilan) {
                    if ($ilan != $_POST[$this->prefix . 'lan']) {
                        $data[$this->prefix . 'lan'] = $ilan;
                        $this->main->add($data);
                    }
                }
            }
        } else {
            $id = $this->input->post('id');
            $record = $this->main->getrecord(array($this->prefix . 'id' => $id))->row();
            foreach ($re_update as $val) {
                if (file_exists('./uploaded/content/' . $record->$val) && $record->$val != "") {
                    unlink('./uploaded/content/' . $record->$val);
                }
            }
            $cd[$this->prefix . 'id'] = $id;
            if ($this->_check_multi_language()) {
                $cd[$this->prefix . 'lan'] = $data[$this->prefix . 'lan'];
            }
            $this->main->edit($cd, $data);
            

            if ($this->_check_multi_language()) {
                $row = $this->main->getrecord(array($this->prefix . 'id' => $id, $this->prefix . 'lan' => $data[$this->prefix . 'lan']))->row_array();
                $data = array();
             
                if (isset($this->filed_syn) && is_array($this->filed_syn) && count($this->filed_syn) > 0) {
          
                    foreach ($this->filed_syn as $filed) {
                        if (isset($row[$filed])){
                            $data[$filed] = $row[$filed];
                        }    
                    }
                }
                
                if(count($data)>0){
                    $this->main->edit(array($this->prefix . 'id' => $row[$this->prefix . 'id']), $data);
                }
            }
        }
        $redirect = $this->input->post('redirect')!=""?BASE_LINK_BACK . $this->input->post('redirect'):BASE_LINK_BACK.$this->pfile.'/edit/'.$id;
        redirect($redirect, 'refresh');
    }

    function del_all() {
        if (isset($_POST['del']) && count($_POST['del']) > 0) {
            foreach ($_POST['del']as $val) {
                $row = $this->main->getrecord(array($this->prefix . 'id' => $val))->row();
                foreach ($this->list_files as $field) {
                    if (isset($row->$field) && $row->$field != "" && file_exists('./uploaded/content/' . $row->$field)) {
                        unlink('./uploaded/content/' . $row->$field);
                    }
                }
                $this->main->edit(array($this->prefix . 'id' => $val),array($this->prefix.'status'=>4));
            }
        }
        redirect($_SERVER["HTTP_REFERER"], 'refresh');
    }

    function seq($or, $act) {
        $row = $this->main->getrecord(array($this->prefix . 'order' => $or, $this->prefix . 'module' => $this->mod_id))->row_array();
        if ($act == "up") {
            $order = $row[$this->prefix . 'order'] - 1;
            $wh = array($this->prefix . 'order <=' => $order, $this->prefix . 'module' => $this->mod_id, 'order' => array($this->prefix . 'order' => 'desc'));
        } else {
            $order = $row[$this->prefix . 'order'] + 1;
            $wh = array($this->prefix . 'order >=' => $order, $this->prefix . 'module' => $this->mod_id, 'order' => array($this->prefix . 'order' => 'asc'));
        }
        $row2 = $this->main->getrecord($wh)->row_array();
        $data[$this->prefix . 'order'] = $row[$this->prefix . 'order'];
        $this->main->edit(array($this->prefix . 'id' => $row2[$this->prefix . 'id']), $data);
        $data[$this->prefix . 'order'] = $order;
        $this->main->edit(array($this->prefix . 'id' => $row[$this->prefix . 'id']), $data);
        redirect($_SERVER["HTTP_REFERER"], 'refresh');
    }

    function ajax_set_order() {
        foreach ($_POST as $order => $id) {
            $this->main->edit(array($this->prefix . 'id' => $id), array($this->prefix . 'order' => $order));
        }
    }

    function export_xls() {
        $filter = array($this->prefix . 'module' => $this->mod_id, 'order' => array('mem_modifyon' => 'desc'));
        $data['result'] = $this->main->getlist($filter);
        $name = "subscriber_" . date('ymdhis') . ".xls";
        force_download($name, $this->load->view('admin/excel_export', $data, true));
    }

    function export_img() {
        $this->load->library('zip');
        $id_all = $_POST;
        foreach ($id_all as $id) {
            $row = $this->main->getrecord(array($this->prefix . 'id' => $id))->row();
            $path = (file_exists('./includes/upload/full/' . $row->pho_image)) ? './includes/upload/full/' : './includes/upload/';
            $this->zip->read_file($path . $row->pho_image);
        }
        $this->zip->archive('./uploaded/tmp/photo.zip');
        $size = filesize('./uploaded/tmp/photo.zip') / 1024;
        if ($size <= 5000) {
            echo "<a href=\"" . base_url() . "uploaded/tmp/photo.zip\">Download (" . round($size / 1024, 2) . " M) </a>  <a href=\"#\" onclick=\"cancle_download()\" >Cancle</a>";
        } else {
            unlink('./uploaded/tmp/photo.zip');
            echo "<span>* ไฟล์ที่เลือกขนาด " . round($size / 1024, 2) . " M  [ Maximum 5M ]</span>";
        }
    }

    function del_zip() {
        unlink('./uploaded/tmp/photo.zip');
    }

    function set_language($lan) {
        $lan = (!in_array($lan, $this->lan)) ? $this->lan[0] : $lan;
        $data = array($this->pfile . '_slan' => $lan);
        $this->session->set_userdata($data);
        redirect($_SERVER["HTTP_REFERER"], 'refresh');
        
    }
    
    function _check_multi_language(){
        return (isset($this->lan) && count($this->lan) > 1);
    }

    function deletefile() {
        $id = $this->uri->segment(5);
        $field = $this->uri->segment(4);
        $row = $this->main->getrecord(array($this->prefix.'id'=>$id))->row();
        if (isset($row->$field) && $row->$field != "" && file_exists('./uploaded/content/' . $row->$field)) {
            unlink('./uploaded/content/' . $row->$field);
            $data[$field] ="";
            $this->main->edit(array($this->prefix.'id'=>$id), $data);
            redirect($_SERVER["HTTP_REFERER"], 'refresh');
        }else{
            echo "";
        }
    }
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */