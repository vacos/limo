<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Driver extends MY_Controller {

	function __construct()
	{
		parent::__construct();

		$this->date_now = date('Y-m-d H:i:s');
		$this->raw = $this->getRawData();
		$this->checkHeader($this->raw);
        
	}

	public function index()
	{
		redirect(BASE_LINK);
	}

	public function driving()
	{
		$drivers = $this->driver->find();
		if($drivers != false){
			$json['status'] = 200;
			$json['message'] = 'Success';
			$json['data'] = $drivers;
		}else{
			$json['status'] = 500;
			$json['message'] = 'Not have driver';
		}
		
		$this->return_json($json);
    }
	
	public function find()
	{
		$orders = $this->order->find_driver();
		if($orders != false){
			$json['status'] = 200;
			$json['message'] = 'Success';
			$json['data'] = $orders;
		}else{
			$json['status'] = 500;
			$json['message'] = 'Not have order now';
		}
		
		$this->return_json($json);
	}
	
	public function vote(){
		$order_id = @$this->raw['order_id'];
		if($this->order->check_status($order_id,1)){ // Check Order Success

			$order_id = @$this->raw['order_id'];
			$rate = (int) @$this->raw['rate'];
			$comment = @$this->raw['comment'];

			$rate = ($rate > 5) ? 5 : $rate;
			$rate = ($rate < 0) ? 0 : $rate;
			$this->driver->vote($order_id,$rate,$comment);
			
			$json['status'] = 200;
			$json['message'] = 'Vote Success';
		}
		else{
			$json['status'] = 500;
			$json['message'] = 'Check Order ID';
		}

		$this->return_json($json);
	}
    
    public function confirm()
	{
		$driver_id = @$this->raw['driver_id'];
		$this->isDriver($driver_id);

		$order_id = @$this->raw['order_id'];
		
		if($this->order->check_status($order_id,2)){ // Check Order Find driver
			$this->driver->confirm($order_id,$driver_id);

			//charge omise
			$payment_id = $this->payment->insert($order_id);

			$payments = $this->payment->get_charge($payment_id);
			if(count($payments) > 0){

				foreach ($payments as $payment) {
					//print_r($payment);die;
					if($payment['card_number'] != 'Cash'){

						$this->omise_api->setUserID($payment['user_id']);
						$return_uri = $this->config->item('base_url').'/main/check_payment?order_id='.$order_id;
						$charge_return = $this->omise_api->charges($payment['user_token'], $payment['card_token'], $this->calAmountOmise($payment['fare']),$return_uri);
						
						if(@$charge_return['status'] == 'pending' && @$charge_return['authorize_uri'] != ''){
							$this->payment->pending($payment['payment_id'],$charge_return['id'],@$charge_return['authorize_uri']);
							$json['status'] = 200;
							$json['message'] = 'กรุณารอ User ทำการชำระเงิน';
	
						}else{
							$this->payment->fail($payment['payment_id'],@$charge_return['error'].'|'.@$charge_return['failure_message']);
							$json['status'] = 500;
							$json['message'] = 'Charge Omise Fail : '.@$charge_return['error'].'|'.@$charge_return['failure_message'];
						}
						
					}else{
						$json['status'] = 201;
						$json['message'] = 'ลูกค้าชำระเงินด้วยเงินสด';
					}
				}
			}

			
		}else{

			$json['status'] = 500;
			$json['message'] = 'Check Order ID';
		}
		
		$this->return_json($json);
    }

	public function success()
	{
		$driver_id = @$this->raw['driver_id'];
		$this->isDriver($driver_id);

        $order_id = @$this->raw['order_id'];

		if($this->order->check_status($order_id,3)){ // Check Order Driving

			$this->driver->success($order_id,$driver_id);

			$payments = $this->payment->get_charge('',$order_id);
			$payment = $payments[0];
			if($payment['card_number'] == 'Cash'){
				$this->payment->success($payment['payment_id'],$payment['card_number']);
				$driver = $this->user->get('',$payment['driver_id']);
				$data_user['income'] = $driver['income'] + $this->order->calculate_income_driver($payment['fare']);
				$this->user->update($payment['driver_id'],$data_user);
			}
			
			$json['status'] = 200;
			$json['message'] = 'Success';
		}else{

			$json['status'] = 500;
			$json['message'] = 'Check Order ID';
		}

		$this->return_json($json);
	}
	
	public function pickup()
	{
		$driver_id = @$this->raw['driver_id'];
		$this->isDriver($driver_id);

        $order_id = @$this->raw['order_id'];

		if($this->order->check_status($order_id,3)){ // Check Order Driving

			$this->driver->pickup($order_id,$driver_id);
			
			$json['status'] = 200;
			$json['message'] = 'Success';
		}else{

			$json['status'] = 500;
			$json['message'] = 'Check Order ID';
		}

		$this->return_json($json);
    }
}
