<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends MY_Controller {

	function __construct()
	{
		parent::__construct();

		$this->date_now = date('Y-m-d H:i:s');
		$this->raw = $this->getRawData();
		$this->checkHeader($this->raw);
	}

	public function index()
	{
		redirect(BASE_LINK);
    }

    public function book(){
        $id = @$this->raw['id'];
		$user = $this->user->get('',$id);

		$id_payment = @$this->raw['id_payment'];
		$medthod = $this->payment_method->get($id_payment);

		if($user != false && $medthod != false){

			$data['user_id'] = $user['id'];

			$data['number'] = $this->order->getLastOrderNumber();

			$data['start_location_lat'] = @$this->raw['start_location_lat'];
			$data['start_location_long'] = @$this->raw['start_location_long'];
			$data['start_location'] = @$this->raw['start_location'];

			$data['destination_lat'] = @$this->raw['destination_lat'];
			$data['destination_long'] = @$this->raw['destination_long'];
			$data['destination'] = @$this->raw['destination'];

			$data['schedule_ride'] = @$this->raw['schedule_ride'];
			$data['car_type'] = @$this->raw['car_type'];
			$data['note'] = @$this->raw['note'];
			$data['id_payment'] = @$this->raw['id_payment'];
			$data['fare'] = @$this->raw['fare'];

			$data['status'] = 2;// 0 Cancel or Fail, 1 Success, 2 Find Driver, 3 Driving
			$data['create_on'] = $this->date_now;
			$data['expressway'] = (int) @$this->raw['expressway'];// 0 not use, 1 use
			
			$order_id = $this->order->insert($data);

			$json['status'] = 200;
			$json['message'] = 'Success';
			$json['data'] = $this->order->get($user['id'],$order_id);
		}
		else{

			$json['status'] = 500;
			$json['message'] = 'Not find user or ID Payment is incorrect';
			
		}
		
		$this->return_json($json);
	}
	
	public function calculate(){

		$json['status'] = 200;
		$json['message'] = 'Success';

		$km = $this->raw['km'];
		$time = $this->raw['time'];

		
		$cars = $this->car->listing($km,$time);

		$i = 0;
		foreach ($cars as $car_id => $car) {
			$fare = $this->order->calculate($car['id'],$km,$time);
			$listing[$car_id] = $car;
			$listing[$car_id]['fare'] = $fare;
			$i++;
		}

		$json['data'] = $listing;

		$this->return_json($json);

	}

    public function history(){
		$id = $this->raw['id'];

		$user = $this->user->get('',$id);


		if($user != false){
			$json['status'] = 200;
			$json['message'] = 'Success';

			$offset = (int) @$this->raw['offset'];
			$limit = (int) ( @$this->raw['limit'] == '') ? 10 : @$this->raw['limit'];
			$json['data'] = $this->order->history($user['id'],$offset,$limit);

		}
		else{

			$json['status'] = 500;
			$json['message'] = 'Not find user';
			
		}

		$this->return_json($json);
	}
	
	public function get(){
		$id = $this->raw['id'];
		$user = $this->user->get('',$id);

		$id_history = $this->raw['id_history'];
		$history = $this->order->get($id,$id_history);
		if($user != false && $history != false){

			$json['status'] = 200;
			$json['message'] = 'Success';

			$json['data'] = $history;
			$json['data']['user'] = $this->user->get('',$user['id']);

			$driver_id = $json['data'][0]['driver_id'];
			$json['data']['driver'] = ($driver_id != 0) ? $this->user->get('',$driver_id) : array();

			$json['data']['payment'] = $this->payment->get_charge('',$id_history);

		}
		else{

			$json['status'] = 500;
			$json['message'] = 'Not find user or ID History is incorrect';
			
		}

		$this->return_json($json);
	}
	
	public function cancel()
	{
		$id = $this->raw['id'];
		$user = $this->user->get('',$id);

		$order_id = $this->raw['order_id'];
		$order = $this->order->get($id,$order_id);
		if($user != false && $order != false){

			if($order[0]['status'] != 0){
				$this->order->cancel($order_id,$id);

				$json['status'] = 200;
				$json['message'] = 'Success';
			}
			else{
				$json['status'] = 500;
				$json['message'] = 'Order is cancelled';
			}
			

		}
		else{

			$json['status'] = 500;
			$json['message'] = 'Not find user or Order ID s incorrect';
			
		}

		$this->return_json($json);
    }
}