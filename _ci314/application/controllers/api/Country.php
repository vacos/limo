<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Country extends MY_Controller {

	function __construct()
	{
		parent::__construct();

		$this->raw = $this->getRawData();
		$this->checkHeader($this->raw);
        
	}

	public function index()
	{
		redirect(BASE_LINK);
	}
	
	public function listing()
	{
        $json['status'] = 200;
		$json['message'] = 'Success';
		$json['data'] = $this->country->listing();
		
		$this->return_json($json);
	}
	
	public function get()
	{
		$id = (int) @$this->raw['id'];
		$country = $this->country->get($id);
		if($country != false){
			$json['status'] = 200;
			$json['message'] = 'Success';
			$json['data'][$id] = $country;
		}else{
			$json['status'] = 500;
			$json['message'] = 'Not found';
		}
        
		
		$this->return_json($json);
    }
    
    
}
