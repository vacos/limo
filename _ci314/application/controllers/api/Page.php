<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends MY_Controller {

	function __construct()
	{
        parent::__construct();
		
		$this->raw = $this->getRawData();
		$this->checkHeader($this->raw);
        
	}

	public function index()
	{
		redirect(BASE_LINK);
	}

	public function get()
	{
		$page = $this->page->get(@$this->raw['slug']);
		if($page != false){
			$json['status'] = 200;
			$json['message'] = 'Success';
			$json['data'] = $page;
		}else{
			$json['status'] = 500;
			$json['message'] = 'Not have page';
		}
		
		$this->return_json($json);
    }
	
	
}
