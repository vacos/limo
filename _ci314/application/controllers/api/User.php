<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller {

	var $path_usr = './uploaded/usr/';

	function __construct()
	{
		parent::__construct();
		
		$this->date_now = date('Y-m-d H:i:s');
		$this->raw = $this->getRawData();
		if($this->input->post('bp') != 'limoBP'){
			$this->checkHeader($this->raw);
		}
		
	}

	public function index()
	{
		redirect(BASE_LINK);
	}

	// public function upload_image(){

	// 	$name = $_FILES["file_upload"]["name"];
	// 	$ext = '.'.end((explode(".", $name)));
	// 	$filename = date('Ymdhis').rand(100,900).$ext;
	// 	$target_path = $this->path_usr.$filename;

	// 	if (move_uploaded_file($_FILES['file_upload']['tmp_name'], $target_path)) { 
	// 		$json['status'] = 200;
	// 		$json['message'] = 'Success';
	// 		$json['data']['filename'] = $filename;
	// 		$json['data']['path'] = $this->config->item('usr_url').$filename;
	// 	}else{
	// 		$json['status'] = 500;
	//  		$json['message'] = 'Cann\'t Upload File';
	// 	}

	// 	$this->return_json($json);

	// }

	// public function upload_image(){
		
	// 	$config['upload_path']          =   $this->path_usr;
    //     $config['allowed_types']        =   'gif|jpg|png|jpeg|JPG|PNG|JPEG|GIF';
    //     $config['file_name']            =   date('Ymdhis').rand(100,900);
	// 	$this->load->library('upload', $config);
		
    //     if ($this->upload->do_upload('file_upload'))
    //     {
	// 		$json['status'] = 200;
	// 		$json['message'] = 'Success';
    //         $filename = $this->upload->data('file_name');
	// 		$this->resizeCenter($filename);
	// 		$json['data']['filename'] = $filename;
	// 		$json['data']['path'] = $this->config->item('usr_url').$filename;
	// 	}
	// 	else{
	// 		$json['status'] = 500;
	// 		$json['message'] = $this->upload->display_errors();
	// 	}

	// 	$this->return_json($json);
	// }

	public function update(){
		$id = @$this->raw['id'];
		$user = $this->user->get('',$id);
		if($user != false){
			
			if(isset($this->raw['facebook_id'])){
				$face_id = $this->raw['facebook_id'];
				$data['facebook_id'] = $face_id;
			}
	
			if(isset($this->raw['name'])){
				$data['name'] = $this->raw['name'];
			}
	
			if(isset($this->raw['email'])){
				$data['email'] = $this->raw['email'];
			}
			
			if(isset($this->raw['image'])){
				$data['image'] = $this->raw['image'];
			}
	
			if(isset($this->raw['phone'])){
				$phone = $this->raw['phone'];

				if($this->user->check($phone)){
					$json['status'] = 500;
					$json['message'] = 'Phone is duplicate';
					$this->return_json($json);
				}

				$data['phone'] = $phone;

			}

			if(isset($this->raw['password'])){
				$data['password'] = $this->genPassword($this->raw['password']);
			}

			if(isset($this->raw['uuid'])){
				$data['uuid'] = $this->raw['uuid'];
			}

			if(isset($this->raw['udid'])){
				$data['udid'] = $this->raw['udid'];
			}

			if(isset($this->raw['pushbadge'])){
				$data['pushbadge'] = $this->raw['pushbadge'];
			}

			if(isset($this->raw['pushsound'])){
				$data['pushsound'] = $this->raw['pushsound'];
			}

			if(isset($this->raw['pushalert'])){
				$data['pushalert'] = $this->raw['pushalert'];
			}

			if(isset($this->raw['device'])){
				$data['device'] = $this->raw['device'];
			}

			if(isset($data)){
				$this->user->update($id,$data);
			}

			$user_update = $this->user->get('',$id);

			$json['status'] = 200;
			$json['message'] = 'Success';
			$json['data'] = $user_update;

		}else{

			$json['status'] = 500;
			$json['message'] = 'Not find user';

		}

		$this->return_json($json);
	}

	public function get(){
		//$phone = @$this->raw['phone'];
		$id = @$this->raw['id'];
		$user = $this->user->get('',$id);
		if($user != false){
			
			$json['status'] = 200;
			$json['message'] = 'Success';

			$json['data'] = $user;
		}
		else{
			$json['status'] = 500;
			$json['message'] = 'Not find user';
		}

		$this->return_json($json);
	}

	public function check(){
		$json['status'] = 500;

		$phone = @$this->raw['phone'];
		
		if($this->user->check($phone)){

			$user = $this->user->get($phone);

			if($user['verify'] == 1){
				$json['status'] = 200;
				$json['message'] = 'Is Member';
				$data['phone'] = $phone;
				$json['data'] = $data;

			}else{
				
				$json['message'] = 'New Member';

				$data['phone'] = $phone;
				$data['code'] = $this->genVerifyCode();
				$json['data'] = $data;

				$this->user->update($user['id'],$data);

				$this->__sendSMS($phone,$this->smsText($data['code']));
				//Send SMS Here

			}
		}
		else{
			$json['message'] = 'New Member';
			
			$data['phone'] = $phone;
			$data['code'] = $this->genVerifyCode();
			$json['data'] = $data;

			$check_only = (int) @$this->raw['check_only'];
			if($check_only != 1){
				$data['create_on'] = $this->date_now;
				$this->user->insert($data);
			}else{
				$json['message'] .= ' | Check only';
			}

			$this->__sendSMS($phone,$this->smsText($data['code']));
			//Send SMS Here
			
		}

		$this->return_json($json);
	}

	public function reset_password(){
		$json['status'] = 500;
		$json['message'] = 'ไม่พบเบอร์นี้ในระบบ';

		$phone = @$this->raw['phone'];
		
		if($this->user->check($phone)){

			$user = $this->user->get($phone);

			if($user['verify'] == 1){
				$json['status'] = 200;
				$json['message'] = 'ส่งรหัสผ่านชั่วคราวให้ทาง SMS เรียบร้อยแล้ว';
				$new_pass = mt_rand(100000, 999999);
				$data['password'] = $this->genPassword($new_pass);
				$this->user->update($user['id'],$data);

				$this->__sendSMS($phone,'รหัสผ่านชั่วคราวคือ '.$new_pass);
				//Send SMS Here
			}
		}

		$this->return_json($json);
	}

	public function verify(){
		
		$phone = @$this->raw['phone'];
		$code = @$this->raw['code'];
		
		if($this->user->check($phone)){

			$user = $this->user->get($phone);
			if($user['verify'] == 0 && $code == $user['code']){

				$customer_omise = $this->omise_api->customer_crate($user['id'],$user['phone']);
				$data['token'] = @$customer_omise['id'];

				$json['status'] = 200;
				$json['message'] = 'Verify Success';
				$data['verify'] = 1;
				$data['last_login'] = $this->date_now;
				$this->user->update($user['id'],$data);

				$new_user = $this->user->get($phone);

				$json['data'] = $new_user;

			}
			elseif($user['verify'] == 1){

				$json['status'] = 500;
				$json['message'] = 'Verified';
				
			}
			else{
				$json['status'] = 500;
				$json['message'] = 'Verify Fail';
			}
		}
		else{

			$json['status'] = 500;
			$json['message'] = 'Not find user';
			
		}

		$this->return_json($json);
	}

	public function login(){

		$phone = @$this->raw['phone'];
		$password = $this->genPassword(@$this->raw['password']);
		
		if($this->user->login($phone,$password)){

			$user = $this->user->get($phone);
			if($user['active'] == 0){
				$data['last_login'] = $this->date_now;
				$data['active'] = 1;

				$this->user->update($user['id'],$data);

				$this->user->log_login('login',$user['id'],$this->date_now,$this->raw['lat'],$this->raw['long']);

				$json['status'] = 200;
				$json['message'] = 'Success';
				$json['data'] = $this->user->get($phone);
			}else{
				$json['status'] = 500;
				$json['message'] = 'User is logged';
				$json['data']['id'] = $user['id'];
			}
			

		}
		else{

			$json['status'] = 500;
			$json['message'] = 'Password ไม่ถูกต้อง';

		}

		$this->return_json($json);
	}

	public function login_app(){
		$id = @$this->raw['id'];
		$user = $this->user->get('',$id);
		if($user != false /*&& $user['active'] == 0*/){
			
			$data['active'] = 1;

			$this->user->update($user['id'],$data);

			if($this->user->log_login('login',$user['id'],$this->date_now,$this->raw['lat'],$this->raw['long'])){
				$json['status'] = 200;
				$json['message'] = 'Success';
			}
			else{
				$json['status'] = 500;
				$json['message'] = 'Not login';
			}
			
		}
		else{
			$json['status'] = 500;
			$json['message'] = 'Not find user Or User not active';
		}

		$this->return_json($json);
	}

	public function logout(){
		$id = @$this->raw['id'];
		$user = $this->user->get('',$id);
		if($user != false && $user['active'] == 1){
			
			$data['active'] = 0;

			$this->user->update($user['id'],$data);

			if($this->user->log_login('logout',$user['id'],$this->date_now)){
				$json['status'] = 200;
				$json['message'] = 'Success';
			}
			else{
				$json['status'] = 500;
				$json['message'] = 'Not login';
			}
			
		}
		else{
			$json['status'] = 500;
			$json['message'] = 'Not find user Or User not active';
		}

		$this->return_json($json);
	}

	public function payment_add_cash(){
		$id = @$this->raw['id'];
		$user = $this->user->get('',$id);

		if($user != false){
			
			if(!$this->payment_method->check_cash($user['id'])){

				$data['card_number	'] = 'Cash';
				$data['user_id'] = $user['id'];
				$data['create_on'] = $this->date_now;
				$data['update_on'] = $this->date_now;

				$new_id_payment = $this->payment_method->insert($data);

				$json['data'] = $this->payment_method->get($new_id_payment);

			}
			else{

				$json['status'] = 500;
				$json['message'] = 'Added Cash';

			}

		}
		else{

			$json['status'] = 500;
			$json['message'] = 'Not find user';
			
		}

		
		$this->return_json($json);
	}

	public function payment_add(){
		$id = @$this->raw['id'];
		$user = $this->user->get('',$id);

		if($user != false){

			$token = @$this->raw['token'];
			//$id_payment = $this->raw['id_payment'];
			
			if(!$this->payment_method->check('',$token,$user['id'])){

				$this->omise_api->customer_add_card($user['token'],$token);
				$omise_cust = $this->omise_api->customer_get($user['token']);

				if(isset($omise_cust['cards']['data']) && count($omise_cust['cards']['data']) > 0){

					$card_index = count($omise_cust['cards']['data']) - 1;

					$data['card_number'] = $omise_cust['cards']['data'][$card_index]['last_digits'];
					$data['expire']= $omise_cust['cards']['data'][$card_index]['expiration_month'].'/'.$omise_cust['cards']['data'][$card_index]['expiration_year'];
					$data['cvv'] = null;
					$data['country'] = $omise_cust['cards']['data'][$card_index]['country'];
					$data['token'] = $omise_cust['cards']['data'][$card_index]['id'];
					$data['user_id'] = $user['id'];
					$data['create_on'] = $this->date_now;
					$data['update_on'] = $this->date_now;

					$new_id_payment = $this->payment_method->insert($data);

					$json['data'] = $this->payment_method->get($new_id_payment);
				}
				else{

					$json['status'] = 500;
					$json['message'] = 'Card number is fail';

				}

			}
			else{

				$json['status'] = 500;
				$json['message'] = 'Card number is duplicate';

			}

		}
		else{

			$json['status'] = 500;
			$json['message'] = 'Not find user';
			
		}

		
		$this->return_json($json);
	}

	public function payment_listing(){
		$id = @$this->raw['id'];
		$user = $this->user->get('',$id);

		if($user != false){

			$json['status'] = 200;
			$json['message'] = 'Success';
			$json['data'] = $this->payment_method->listing($user['id']);

		}
		else{

			$json['status'] = 500;
			$json['message'] = 'Not find user';
			
		}
		
		$this->return_json($json);
	}

	// public function payment_update(){
	// 	$id = @$this->raw['id'];
	// 	$user = $this->user->get('',$id);

	// 	if($user != false){

	// 		$id_payment = @$this->raw['id_payment'];

	// 		$medthod = $this->payment_method->get($id_payment,true);
			
	// 		if($medthod != false){

	// 			$data['cvv'] = @$this->raw['cvv'];
	// 			$data['expire']= @$this->raw['expire'];
	// 			$medthod['cvv'] = $data['cvv'];
	// 			$medthod['expire'] = $data['expire'];

	// 			$_omise_data = $medthod;
	// 			$_omise_data['name'] = $user['name'];
	// 			$omise_data = $this->calDataToOmise($_omise_data);

	// 			if($this->checkCardGateWay($user['id'],$omise_data)){

	// 				$json['status'] = 200;
	// 				$json['message'] = 'Success';

	// 				$data['update_on'] = $this->date_now;

	// 				$this->payment_method->update($id_payment,$data);

	// 				$json['data'] = $this->payment_method->get($id_payment);

	// 			}
	// 			//Send check to Bank
	// 			else{

	// 				$json['status'] = 500;
	// 				$json['message'] = 'Card number is fail';

	// 			}
	// 		}
	// 		else{

	// 			$json['status'] = 500;
	// 			$json['message'] = 'Card number is fail';

	// 		}
	// 	}
	// 	else{

	// 		$json['status'] = 500;
	// 		$json['message'] = 'Not find user';
			
	// 	}

	// 	$this->return_json($json);
	// }

	public function payment_delete(){
		$id = @$this->raw['id'];
		$user = $this->user->get('',$id);

		if($user != false){

			$id_payment = @$this->raw['id_payment'];

			$medthod = $this->payment_method->get($id_payment);
			
			if($medthod != false){

				$json['status'] = 200;
				$json['message'] = 'Success';

				$this->payment_method->delete($id_payment);
			}
			else{

				$json['status'] = 500;
				$json['message'] = 'Card number is fail';

			}
		}
		else{

			$json['status'] = 500;
			$json['message'] = 'Not find user';
			
		}

		$this->return_json($json);
	}

	public function add_place(){
		$id = @$this->raw['id'];
		$user = $this->user->get('',$id);
		if($user != false){

			$data['user_id'] = $id;
			$data['lat'] = @$this->raw['lat'];
			$data['long'] = @$this->raw['long'];
			$data['name'] = @$this->raw['name'];
			
			if(@$this->raw['type'] == 1 || @$this->raw['type'] == 2){

				// if(!$this->place->check_type($id,$this->raw['type'])){
				// 	$data['type'] =  $this->raw['type'];
				// }
				// else{
				// 	$json['status'] = 500;
				// 	$json['message'] = 'Type Home Or Work is duplicate';
				// 	$this->return_json($json);
				// }

				$data['type'] =  $this->raw['type'];
				
			}
			else{
				
				$data['type'] = 3;//other
				
			}

			$this->place->insert($data);

			$places = $this->place->listing($id);

			$json['status'] = 200;
			$json['message'] = 'Success';
			$json['data'] = $places;
			
		}
		else{
			$json['status'] = 500;
			$json['message'] = 'Not find user';
		}

		$this->return_json($json);
	}

	public function del_place(){
		$id = @$this->raw['id'];
		$user = $this->user->get('',$id);
		if($user != false){

			$this->place->delete($id,$this->raw['place_id']);

			$places = $this->place->listing($id);

			$json['status'] = 200;
			$json['message'] = 'Success';
			$json['data'] = $places;
			
		}
		else{
			$json['status'] = 500;
			$json['message'] = 'Not find user';
		}

		$this->return_json($json);
	}

	public function listing_place(){
		$id = @$this->raw['id'];
		$user = $this->user->get('',$id);
		if($user != false){

			$places = $this->place->listing($id);

			$json['status'] = 200;
			$json['message'] = 'Success';
			$json['data'] = $places;
			
		}
		else{
			$json['status'] = 500;
			$json['message'] = 'Not find user';
		}

		$this->return_json($json);
	}

	private function genVerifyCode(){
		$code = '';
		for ($i = 0; $i<4; $i++) 
		{
			$code .= mt_rand(0,9);
		}
		return $code;
	}

	private function genUserThumb($url){
		$filename = date('Ymdhis').rand(100,900);
		$profile_image = $this->path_usr.$filename.'.jpg';

		if(file_get_contents($url)){

			file_put_contents($profile_image, file_get_contents($url));
			//Save Image Facebook to Apps
			$userThumb = $filename.'.jpg';

			if($this->resizeCenter($userThumb)){
				return $userThumb;
			}
		}
		
		return '';
	}

	private function resizeCenter($image_name){
		
		//require_once APPPATH."third_party/Zebra_Image.php";
		require_once APPPATH."third_party/crop_image.php";

		$image = new Zebra_Image();
		// if you handle image uploads from users and you have enabled exif-support with --enable-exif
		// (or, on a Windows machine you have enabled php_mbstring.dll and php_exif.dll in php.ini)
		// set this property to TRUE in order to fix rotation so you always see images in correct position
		$image->auto_handle_exif_orientation = false;

		// indicate a source image (a GIF, PNG or JPEG file)
		$image->source_path = $this->path_usr.$image_name;

		// indicate a target image
		// note that there's no extra property to set in order to specify the target
		// image's type -simply by writing '.jpg' as extension will instruct the script
		// to create a 'jpg' file
		$image->target_path = $this->path_usr.'thumb/'.$image_name;

		// since in this example we're going to have a jpeg file, let's set the output
		// image's quality
		$image->jpeg_quality = 100;

		// some additional properties that can be set
		// read about them in the documentation
		$image->preserve_aspect_ratio = true;
		$image->enlarge_smaller_images = true;
		$image->preserve_time = true;
		$image->handle_exif_orientation_tag = true;

		// resize the image to exactly 100x100 pixels by using the "crop from center" method
		// (read more in the overview section or in the documentation)
		//  and if there is an error, check what the error is about
		if (!$image->resize(200, 200, ZEBRA_IMAGE_CROP_CENTER)) {

			return false;

		// if no errors
		} else{
			return true;
		}
	}

	private function smsText($code = ''){
		return 'Verify code: '.$code;
	}

	
}
