<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    
    <title>Check Payment Omise</title>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link href="<?=$this->config->item('static_web')?>add_card.min.css" rel="stylesheet">
    
</head>
<body>
    <div class="container">

        <div class="row">

            <div class="col-12">
                <p class="message">
                    <?php if($status == 200):?>
                    <i class="fas fa-check ok"></i> 
                    <?php else:?>
                    <i class="fas fa-times error"></i> 
                    <?php endif;?>
                    <?=$message?>
                </p>
            </div>

        </div>

    </div>
</body>
</html>