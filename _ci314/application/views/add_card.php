<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link href="<?=$this->config->item('static_web')?>add_card.min.css" rel="stylesheet">
    <title>Add Card</title>
    
</head>
<body>
    <div class="container">
        <form action="/do_add_card" method="post" id="checkout">

            <div class="row">

                <div class="col-12">
                    <div id="token_errors"></div>
                    <input type="hidden" name="omise_token">
                    <input type="hidden" name="user_id" value="<?=$user_id?>">
                </div>

                <div class="col-12">
                    <input type="text" data-omise="holder_name" placeholder="Name" >
                </div>

                <div class="col-12">
                    <input oninput="maxLengthCheck(this)" class="card_number" type="number" data-omise="number" maxLength="16" placeholder="Card Number" >
                </div>

                <div class="col-6">
                    <input oninput="maxLengthCheck(this)" type="number" maxLength="2" data-omise="expiration_month" size="4" placeholder="Expire (mm)">
                </div>
                <div class="col-6">
                    <input oninput="maxLengthCheck(this)" maxLength="4" type="number" data-omise="expiration_year" size="8" placeholder="Expire (yyyy)">
                </div>

                <div class="col-12 last-input">
                    <input type="number" data-omise="security_code" placeholder="CVV" size="8" >
                </div>

                <input type="submit" id="create_token" value="">
            </div>
        </form>
    </div>
    
        
    
    <script src="https://cdn.omise.co/omise.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script>
    Omise.setPublicKey("<?= $this->config->item('pkey')?>");
</script>
<script>
$("#checkout").submit(function () {

var form = $(this);

// Disable the submit button to avoid repeated click.
form.find("input[type=submit]").prop("disabled", true);

// Serialize the form fields into a valid card object.
var card = {
  "name": form.find("[data-omise=holder_name]").val(),
  "number": form.find("[data-omise=number]").val(),
  "expiration_month": form.find("[data-omise=expiration_month]").val(),
  "expiration_year": form.find("[data-omise=expiration_year]").val(),
  "security_code": form.find("[data-omise=security_code]").val()
};

// Send a request to create a token then trigger the callback function once
// a response is received from Omise.
//
// Note that the response could be an error and this needs to be handled within
// the callback.
Omise.createToken("card", card, function (statusCode, response) {
  if (response.object == "error") {
    // Display an error message.
    var message_text = "SET YOUR SECURITY CODE CHECK FAILED MESSAGE";
    if(response.object == "error") {
      message_text = response.message;
    }
    $("#token_errors").html(message_text);

    // Re-enable the submit button.
    form.find("input[type=submit]").prop("disabled", false);
  } else {
    // Then fill the omise_token.
    form.find("[name=omise_token]").val(response.id);

    // Remove card number from form before submiting to server.
    form.find("[data-omise=number]").val("");
    form.find("[data-omise=security_code]").val("");

    // submit token to server.
    form.get(0).submit();
  };
});

// Prevent the form from being submitted;
return false;

});
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>

<script>
function maxLengthCheck(object)
{
    if (object.value.length > object.maxLength)
    object.value = object.value.slice(0, object.maxLength)
}
</script>
</body>
</html>