
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Title -->
    <title><?= $title?></title>

    <meta name="description" content="<?= $description?>" />
	<meta name="keywords" content="เรียกรถ, เช่ารถ, limo, app เรียกรถ" />

	<meta property="og:type" content="website">       
	<meta property="og:title" content="<?= $title?>" />
	<meta property="og:description" content="<?= $description?>" />
	<meta property="og:url" content="<?=$this->config->item('base_url')?>" />
	<meta property="og:image" content="<?=$this->config->item('static_url')?>di/share.jpg" />
	<link rel="canonical" href="<?=$this->config->item('base_url')?>">

    <meta name="twitter:creator" content="@Limoapp">
    <meta name="twitter:site" content="@Limoapp" />
    <meta name="twitter:domain" content="<?=$this->config->item('base_url')?>" />
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:image" content="<?=$this->config->item('static_url')?>di/share.jpg" />  

    <!-- Favicon -->
    <link rel="icon" href="<?=$this->config->item('base_url')?>/favicon.ico">

    <!-- Custom Stylesheet -->
    <link href="<?=$this->config->item('static_web')?>custom.min.css?v1.0.1" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Raleway&display=swap" rel="stylesheet">
    <style>
    body, h1, h2, h3, h4, h5, h6{font-family:Kanit,sans-serif;}
    </style>

</head>

<body>
    <h1 style="display:none;"><?= $title?></h1>
    <!-- Preloader Start -->
    <div id="preloader">
        <div class="colorlib-load"></div>
    </div>

    <!-- ***** Header Area Start ***** -->
    <header class="header_area animated">
        <div class="container-fluid">
            <div class="row align-items-center">
                <!-- Menu Area Start -->
                <div class="col-12 col-lg-10">
                    <div class="menu_area">
                        <nav class="navbar navbar-expand-lg navbar-light">
                            <!-- Logo -->
                            <a class="navbar-brand" href="#">
                                <img height="30" src="<?=$this->config->item('static_web')?>img/logo.png" alt="limoapp">
                            </a>
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ca-navbar" aria-controls="ca-navbar" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                            <!-- Menu Area -->
                            <div class="collapse navbar-collapse" id="ca-navbar">
                                <ul class="navbar-nav ml-auto" id="nav">
                                    <li class="nav-item active"><a class="nav-link" href="#home">Limo</a></li>
                                    <li class="nav-item"><a class="nav-link" href="#about">Why Limo</a></li>
                                    <li class="nav-item"><a class="nav-link" href="#feature">Feature</a></li>
                                    <li class="nav-item"><a class="nav-link" href="#information">Information</a></li>
                                    <li class="nav-item"><a class="nav-link" href="#contact">Contact</a></li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- ***** Header Area End ***** -->

    <!-- ***** Wellcome Area Start ***** -->
    <section class="wellcome_area clearfix" id="home">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12 col-md">
                    <div class="wellcome-heading">
                        <h2>Limo app</h2>
                        <img src="<?=$this->config->item('static_web')?>img/home_vector.png" alt="limoapp">
                        <p>พวกเราเลือกรถที่ดีที่สุดสำหรับคุณ</p>
                    </div>
                    <div class="get-start-area">
                        <img onclick="downloadAndroid()" width="160" src="<?=$this->config->item('static_web')?>img/btn_android.png" alt="ios">
                        <img onclick="downloadiOS()" width="160" src="<?=$this->config->item('static_web')?>img/btn_ios.png" alt="android">
                    </div>
                </div>
            </div>
        </div>
        <!-- Welcome thumb -->
        <div class="welcome-thumb wow fadeInDown" data-wow-delay="0.5s">
            <img src="<?=$this->config->item('static_web')?>img/bg-img/welcome-img2.png" alt="">
        </div>
    </section>
    <!-- ***** Wellcome Area End ***** -->

    <!-- ***** Special Area Start ***** -->
    <section class="special-area bg-white section_padding_100" id="about">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <!-- Section Heading Area -->
                    <div class="section-heading text-center">
                        <h2>Why Limo</h2>
                        <div class="line-shape"></div>
                    </div>
                </div>
            </div>

            <div class="row">
                <!-- Single Special Area -->
                <div class="col-12 col-md-4">
                    <div class="single-special text-center wow fadeInUp" data-wow-delay="0.2s">
                        <div class="single-icon">
                            <i class="ti-car" aria-hidden="true"></i>
                        </div>
                        <h4>รถระดับพรีเมี่ยม</h4>
                        <p>พวกเราเลือกรถที่ดีที่สุดสำหรับคุณ ปลอดภัยด้วยคนขับที่ไว้ใจได้</p>
                    </div>
                </div>
                <!-- Single Special Area -->
                <div class="col-12 col-md-4">
                    <div class="single-special text-center wow fadeInUp" data-wow-delay="0.4s">
                        <div class="single-icon">
                            <i class="ti-credit-card" aria-hidden="true"></i>
                        </div>
                        <h4>รองรับการจ่ายเงินด้วยบัตร</h4>
                        <p>เพื่อความสะดวกสบายในการจ่ายและปลอดภัย เราพัฒนามาเพื่อคุณ</p>
                    </div>
                </div>
                <!-- Single Special Area -->
                <div class="col-12 col-md-4">
                    <div class="single-special text-center wow fadeInUp" data-wow-delay="0.6s">
                        <div class="single-icon">
                            <i class="ti-headphone-alt" aria-hidden="true"></i>
                        </div>
                        <h4>ติดต่อเรา</h4>
                        <p>หากมีข้อสงสัย สามารถติดต่อได้ที่เบอร์ <a href="tel:<?=$this->config->item('tel')?>"><?=$this->config->item('tel')?></a></p>
                    </div>
                </div>
            </div>
        </div>
        <!-- Special Description Area -->
        <div class="special_description_area mt-150">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="special_description_img">
                            <img src="<?=$this->config->item('static_web')?>img/bg-img/special2.png" alt="limoapp">
                        </div>
                    </div>
                    <div class="col-lg-6 col-xl-5 ml-xl-auto">
                        <div class="special_description_content">
                            <h2>Limo app!</h2>
                            <p>ความสะดวกสบายต่างๆ คุณสามารถพบได้ที่เรา Limo app</p>
                            <div class="app-download-area">
                                <div class="app-download-btn wow fadeInUp" data-wow-delay="0.2s">
                                    <!-- Google Store Btn -->
                                    <a onclick="downloadAndroid()" href="javascript:void(0)">
                                        <i class="fa fa-android"></i>
                                        <p class="mb-0"><span>available on</span> Google Store</p>
                                    </a>
                                </div>
                                <div class="app-download-btn wow fadeInDown" data-wow-delay="0.4s">
                                    <!-- Apple Store Btn -->
                                    <a onclick="downloadiOS()" href="javascript:void(0)">
                                        <i class="fa fa-apple"></i>
                                        <p class="mb-0"><span>available on</span> Apple Store</p>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** Special Area End ***** -->

    <!-- ***** Video Area Start ***** -->
    <div class="video-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <!-- Video Area Start -->
                    <div class="video-area" style="background-image: url(<?=$this->config->item('static_web')?>img/video.png);">
                        <div class="video-play-btn">
                            <a href="#" class="video_btn"><i class="fa fa-play" aria-hidden="true"></i></a><!-- Link Youtube -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ***** Video Area End ***** -->

    <!-- ***** Cool Facts Area Start ***** -->
    <section class="cool_facts_area clearfix">
        <div class="container">
            <div class="row">
                <!-- Single Cool Fact-->
                <div class="col-12 col-md-3 col-lg-3">
                    <div class="single-cool-fact d-flex justify-content-center wow fadeInUp" data-wow-delay="0.2s">
                        <div class="counter-area">
                            <h3><span class="counter">90</span></h3>
                        </div>
                        <div class="cool-facts-content">
                            <i class="ion-arrow-down-a"></i>
                            <p>APP <br> DOWNLOADS</p>
                        </div>
                    </div>
                </div>
                <!-- Single Cool Fact-->
                <div class="col-12 col-md-3 col-lg-3">
                    <div class="single-cool-fact d-flex justify-content-center wow fadeInUp" data-wow-delay="0.4s">
                        <div class="counter-area">
                            <h3><span class="counter">120</span></h3>
                        </div>
                        <div class="cool-facts-content">
                            <i class="ion-happy-outline"></i>
                            <p>Happy <br> Clients</p>
                        </div>
                    </div>
                </div>
                <!-- Single Cool Fact-->
                <div class="col-12 col-md-3 col-lg-3">
                    <div class="single-cool-fact d-flex justify-content-center wow fadeInUp" data-wow-delay="0.6s">
                        <div class="counter-area">
                            <h3><span class="counter">40</span></h3>
                        </div>
                        <div class="cool-facts-content">
                            <i class="ion-person"></i>
                            <p>ACTIVE <br>ACCOUNTS</p>
                        </div>
                    </div>
                </div>
                <!-- Single Cool Fact-->
                <div class="col-12 col-md-3 col-lg-3">
                    <div class="single-cool-fact d-flex justify-content-center wow fadeInUp" data-wow-delay="0.8s">
                        <div class="counter-area">
                            <h3><span class="counter">10</span></h3>
                        </div>
                        <div class="cool-facts-content">
                            <i class="ion-ios-star-outline"></i>
                            <p>TOTAL <br>APP RATES</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** Cool Facts Area End ***** -->

    <!-- ***** App Screenshots Area Start ***** -->
    <section class="app-screenshots-area bg-white section_padding_0_100 clearfix" id="feature">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <!-- Heading Text  -->
                    <div class="section-heading">
                        <h2>Feature</h2>
                        <div class="line-shape"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <!-- App Screenshots Slides  -->
                    <div class="app_screenshots_slides owl-carousel">
                        <div class="single-shot">
                            <img src="<?=$this->config->item('static_web')?>img/scr-img/a-1.jpg" alt="">
                        </div>
                        <div class="single-shot">
                            <img src="<?=$this->config->item('static_web')?>img/scr-img/a-2.jpg" alt="">
                        </div>
                        <div class="single-shot">
                            <img src="<?=$this->config->item('static_web')?>img/scr-img/a-3.jpg" alt="">
                        </div>
                        <div class="single-shot">
                            <img src="<?=$this->config->item('static_web')?>img/scr-img/a-4.jpg" alt="">
                        </div>
                        <div class="single-shot">
                            <img src="<?=$this->config->item('static_web')?>img/scr-img/a-5.jpg" alt="">
                        </div>
                        <div class="single-shot">
                            <img src="<?=$this->config->item('static_web')?>img/scr-img/a-4.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** App Screenshots Area End *****====== -->

    <!-- ***** Awesome Features Start ***** -->
    <section class="awesome-feature-area bg-white section_padding_0_50 clearfix" id="information">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <!-- Heading Text -->
                    <div class="section-heading text-center">
                        <h2>Information</h2>
                        <div class="line-shape"></div>
                    </div>
                </div>
            </div>

            <div class="row">
                <!-- Single Feature Start -->
                <div class="col-12 col-sm-6 col-lg-4">
                    <div class="single-feature">
                        <i class="ti-credit-card" aria-hidden="true"></i>
                        <h5>คืนเงิน</h5>
                        <p> <a href="<?=BASE_LINK.'page/refund'?>">ดูรายละเอียดทั้งหมดที่นี้</a> </p>
                    </div>
                </div>
                <!-- Single Feature Start -->
                <div class="col-12 col-sm-6 col-lg-4">
                    <div class="single-feature">
                        <i class="ti-check" aria-hidden="true"></i>
                        <h5>ข้อกำหนดและเงื่อนไข</h5>
                        <p> <a href="<?=BASE_LINK.'page/term'?>">ดูรายละเอียดทั้งหมดที่นี้</a> </p>
                    </div>
                </div>
                <!-- Single Feature Start -->
                <div class="col-12 col-sm-6 col-lg-4">
                    <div class="single-feature">
                        <i class="ti-user" aria-hidden="true"></i>
                        <h5>นโยบายความเป็นส่วนตัว</h5>
                        <p> <a href="<?=BASE_LINK.'page/privacy'?>">ดูรายละเอียดทั้งหมดที่นี้</a> </p>
                    </div>
                </div>
                <!-- Single Feature Start -->
                <div class="col-12 col-sm-6 col-lg-4">
                    <div class="single-feature">
                        <i class="ti-info" aria-hidden="true"></i>
                        <h5>ลิขสิทธิ์</h5>
                        <p> <a href="<?=BASE_LINK.'page/term'?>">ดูรายละเอียดทั้งหมดที่นี้</a> </p>
                    </div>
                </div>
                <!-- Single Feature Start -->
                <div class="col-12 col-sm-6 col-lg-4">
                    <div class="single-feature">
                        <i class="ti-headphone" aria-hidden="true"></i>
                        <h5>ติดต่อเรา</h5>
                        <p>หากมีข้อสงสัย สามารถติดต่อได้ที่เบอร์ <a href="tel:<?=$this->config->item('tel')?>"><?=$this->config->item('tel')?></a></p>
                    </div>
                </div>
                <!-- Single Feature Start -->
                <div class="col-12 col-sm-6 col-lg-4">
                    <div class="single-feature">
                        <i class="ti-money" aria-hidden="true"></i>
                        <h5>สร้างรายได้</h5>
                        <p>สมัครหรือสอบถามได้ที่ <a href="tel:<?=$this->config->item('tel')?>"><?=$this->config->item('tel')?></a></p>
                    </div>
                </div>
                
            </div>

        </div>
    </section>
    <!-- ***** Awesome Features End ***** -->

    <!-- ***** Contact Us Area Start ***** -->
    <section class="footer-contact-area section_padding_100 clearfix" id="contact">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <!-- Heading Text  -->
                    <div class="section-heading">
                        <h2>Contact Us!</h2>
                        <div class="line-shape"></div>
                    </div>
                    <div class="footer-text">
                        <p>หากมีข้อสงสัย แจ้งเรื่องร้องเรียน หรือสอบถามปัญหาต่างๆ สามารถกรอกข้อมูล และทางเราจะรีบติดต่อกลับทันที</p>
                    </div>
                    <div class="address-text">
                        <p><span>Address:</span> 326 ซ.นาคนิวาส38 ถ.นาคนิวาส แขวงลาดพร้าว เขตลาดพร้าว กทม.10230</p>
                    </div>
                    <div class="phone-text">
                        <p><span>Phone:</span> <a href="tel:<?=$this->config->item('tel')?>"><?=$this->config->item('tel')?></a></p>
                    </div>
                    <div class="email-text">
                        <p><span>Email:</span> info@limoapp.me</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <!-- Form Start-->
                    <div class="contact_from">
                        <form action="#" method="post">
                            <!-- Message Input Area Start -->
                            <div class="contact_input_area">
                                <div class="row">
                                    <!-- Single Input Area Start -->
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="name" id="name" placeholder="Your Name" required>
                                        </div>
                                    </div>
                                    <!-- Single Input Area Start -->
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="email" class="form-control" name="email" id="email" placeholder="Your E-mail" required>
                                        </div>
                                    </div>
                                    <!-- Single Input Area Start -->
                                    <div class="col-12">
                                        <div class="form-group">
                                            <textarea name="message" class="form-control" id="message" cols="30" rows="4" placeholder="Your Message *" required></textarea>
                                        </div>
                                    </div>
                                    <!-- Single Input Area Start -->
                                    <div class="col-12">
                                        <button type="submit" class="btn submit-btn">Send</button>
                                    </div>
                                </div>
                            </div>
                            <!-- Message Input Area End -->
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** Contact Us Area End ***** -->

    <!-- ***** Footer Area Start ***** -->
    <footer class="footer-social-icon text-center section_padding_70 clearfix">
        <!-- footer logo -->
        <div class="footer-text">
            <h2>LIMO</h2>
        </div>
        <!-- social icon-->
        <div class="footer-social-icon">
            <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
            <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
        </div>
        <div class="footer-menu">
            <nav>
                <ul>
                    <li><a href="#about">Why Limo</a></li>
                    <li><a href="#feature">Feature</a></li>
                    <li><a href="#information">Information</a></li>
                    <li><a href="#contact">Contact</a></li>
                </ul>
            </nav>
        </div>
    </footer>
    <!-- ***** Footer Area Start ***** -->

    <!-- Jquery-2.2.4 JS -->
    <script src="<?=$this->config->item('static_web')?>js/jquery-2.2.4.min.js"></script>
    <!-- Popper js -->
    <script src="<?=$this->config->item('static_web')?>js/popper.min.js"></script>
    <!-- Bootstrap-4 Beta JS -->
    <script src="<?=$this->config->item('static_web')?>js/bootstrap.min.js"></script>
    <!-- All Plugins JS -->
    <script src="<?=$this->config->item('static_web')?>js/plugins.js"></script>
    <!-- Slick Slider Js-->
    <script src="<?=$this->config->item('static_web')?>js/slick.min.js"></script>
    <!-- Footer Reveal JS -->
    <script src="<?=$this->config->item('static_web')?>js/footer-reveal.min.js"></script>
    <!-- Active JS -->
    <script src="<?=$this->config->item('static_web')?>js/active.js?v1.0.1"></script>

<script>
    $('.submit-btn').click(function(e)
    {
        e.preventDefault();
        
        var name = $("input[name=name]");
        var email = $("input[name=email]");
        var message = $("textarea[name=message]");
        
        if($.trim(name.val()) == '')
        {
            name.focus();
            return false;
        }

        if($.trim(email.val()) == '')
        {
            email.focus();
            return false;
        }

        if($.trim(message.val()) == '')
        {
            message.focus();
            return false;
        }

        $.ajax({
            url: '/post/contact',
            type: 'POST',
            data: ({
                name: name.val(),
                email: email.val(),
                message: message.val()
            }),
            success: function(res)
            {	
                res = $.parseJSON(res);
                if(res.code == 200){
                    name.val('');
                    email.val('');
                    message.val('');
                    alert('ส่งความสำเร็จ');
                }
            }
        }); 
    });

function downloadAndroid(){

    window.location.href = 'https://play.google.com/store/apps/details?id=th.co.me.limoapp';
}

function downloadiOS(){

    window.location.href = 'https://itunes.apple.com/th/app/limo-me/id1449473380?mt=8';
}
</script>
</body>

</html>
