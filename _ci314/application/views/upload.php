<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
</head>
<body>
    <h1>Upload</h1>
    <form action="/main/upload_image" method="post" enctype="multipart/form-data">
        Select image to upload:
        <input type="file" name="file_upload" id="file_upload">
        <input type="submit" value="Upload Image" name="submit">
    </form>
</body>
</html>