
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Title -->
    <title><?= $title?></title>

    <meta name="description" content="<?= $description?>" />
	<meta name="keywords" content="เรียกรถ, เช่ารถ, limo, app เรียกรถ" />

	<meta property="og:type" content="website">       
	<meta property="og:title" content="<?= $title?>" />
	<meta property="og:description" content="<?= $description?>" />
	<meta property="og:url" content="<?=$this->config->item('base_url')?>" />
	<meta property="og:image" content="<?=$this->config->item('static_url')?>di/share.jpg" />
	<link rel="canonical" href="<?=$this->config->item('base_url')?>">

    <meta name="twitter:creator" content="@Limoapp">
    <meta name="twitter:site" content="@Limoapp" />
    <meta name="twitter:domain" content="<?=$this->config->item('base_url')?>" />
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:image" content="<?=$this->config->item('static_url')?>di/share.jpg" />  

    <!-- Favicon -->
    <link rel="icon" href="<?=$this->config->item('base_url')?>/favicon.ico">

    <!-- Custom Stylesheet -->
    <link href="<?=$this->config->item('static_web')?>custom.min.css" rel="stylesheet">

</head>

<body>
    <h1 style="display:none;"><?= $title_display?></h1>
    <!-- Preloader Start -->
    <div id="preloader">
        <div class="colorlib-load"></div>
    </div>

    <!-- ***** Header Area Start ***** -->
    <header class="header_area animated">
        <div class="container-fluid">
            <div class="row align-items-center">
                <!-- Menu Area Start -->
                <div class="col-12 col-lg-10">
                    <div class="menu_area">
                        <nav class="navbar navbar-expand-lg navbar-light">
                            <!-- Logo -->
                            <a class="navbar-brand" href="#">
                                <img height="30" src="<?=$this->config->item('static_web')?>img/logo.png" alt="limoapp">
                            </a>
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ca-navbar" aria-controls="ca-navbar" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                            <!-- Menu Area -->
                            <div class="collapse navbar-collapse" id="ca-navbar">
                                <ul class="navbar-nav ml-auto" id="nav">
                                    <li class="nav-item active"><a class="nav-link" href="/#home">Limo</a></li>
                                    <li class="nav-item"><a class="nav-link" href="/#about">Why Limo</a></li>
                                    <li class="nav-item"><a class="nav-link" href="/#feature">Feature</a></li>
                                    <li class="nav-item"><a class="nav-link" href="/#information">Information</a></li>
                                    <li class="nav-item"><a class="nav-link" href="/#contact">Contact</a></li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- ***** Header Area End ***** -->

    

    <!-- ***** Contact Us Area Start ***** -->
    <section class="footer-contact-area section_padding_100 clearfix" id="contact">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- Heading Text  -->
                    <div class="section-heading">
                        <h2><?=$title_display?></h2>
                        <div class="line-shape"></div>
                    </div>
                    <div class="footer-text">
                        <?=$content?>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
    <!-- ***** Contact Us Area End ***** -->

    <!-- ***** Footer Area Start ***** -->
    <footer class="footer-social-icon text-center section_padding_70 clearfix">
        <!-- footer logo -->
        <div class="footer-text">
            <h2>LIMO</h2>
        </div>
        <!-- social icon-->
        <div class="footer-social-icon">
            <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
            <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
        </div>
        <div class="footer-menu">
            <nav>
                <ul>
                    <li><a href="#about">Why Limo</a></li>
                    <li><a href="#feature">Feature</a></li>
                    <li><a href="#information">Information</a></li>
                    <li><a href="#contact">Contact</a></li>
                </ul>
            </nav>
        </div>
    </footer>
    <!-- ***** Footer Area Start ***** -->

    <!-- Jquery-2.2.4 JS -->
    <script src="<?=$this->config->item('static_web')?>js/jquery-2.2.4.min.js"></script>
    <!-- Popper js -->
    <script src="<?=$this->config->item('static_web')?>js/popper.min.js"></script>
    <!-- Bootstrap-4 Beta JS -->
    <script src="<?=$this->config->item('static_web')?>js/bootstrap.min.js"></script>
    <!-- All Plugins JS -->
    <script src="<?=$this->config->item('static_web')?>js/plugins.js"></script>
    <!-- Slick Slider Js-->
    <script src="<?=$this->config->item('static_web')?>js/slick.min.js"></script>
    <!-- Footer Reveal JS -->
    <script src="<?=$this->config->item('static_web')?>js/footer-reveal.min.js"></script>
    <!-- Active JS -->
    <script src="<?=$this->config->item('static_web')?>js/active.js?v1.0.1"></script>

<script>
    $('.submit-btn').click(function(e)
    {
        e.preventDefault();
        
        var name = $("input[name=name]");
        var email = $("input[name=email]");
        var message = $("textarea[name=message]");
        
        if($.trim(name.val()) == '')
        {
            name.focus();
            return false;
        }

        if($.trim(email.val()) == '')
        {
            email.focus();
            return false;
        }

        if($.trim(message.val()) == '')
        {
            message.focus();
            return false;
        }

        $.ajax({
            url: '/post/contact',
            type: 'POST',
            data: ({
                name: name.val(),
                email: email.val(),
                message: message.val()
            }),
            success: function(res)
            {	
                res = $.parseJSON(res);
                if(res.code == 200){
                    name.val('');
                    email.val('');
                    message.val('');
                    alert('ส่งความสำเร็จ');
                }
            }
        }); 
    });

function downloadAndroid(){

    window.location.href = 'https://play.google.com/store/apps/details?id=th.co.me.limoapp';
}

function downloadiOS(){

    window.location.href = 'https://itunes.apple.com/th/app/limo-me/id1449473380?mt=8';
}
</script>
</body>

</html>
