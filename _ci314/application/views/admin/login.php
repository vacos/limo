<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title>PORTNICK.com | <?php echo WEB_TITLE?></title>
		<link rel="stylesheet" href="<?php echo BASE_INCLUDE_BACK?>css/style.default.css" type="text/css" />
		<link rel="stylesheet" href="<?php echo BASE_INCLUDE_BACK?>css/style.navyblue.css" type="text/css" />
		<script type="text/javascript" src="<?php echo BASE_INCLUDE_BACK?>js/jquery-1.9.1.min.js"></script>
		<script type="text/javascript">
		    jQuery(document).ready(function(){
		        jQuery('#login').submit(function(){
		            var u = jQuery('#username').val();
		            var p = jQuery('#password').val();
		            if(u == '' && p == '') {
		                jQuery('.login-alert').fadeIn();
		                return false;
		            }
		        });
		    });
		</script>
	</head>
	<body>
	<body class="loginpage">
	<div class="loginpanel">
	    <div class="loginpanelinner">
	        <div class="logo animate1 fadeIn" >
	        	<div>
					<h1 style="color:#fff"><?php echo DOMAIN?></h1>
	        	</div>
		    </div>
			<?php if(!$_is_maintenance):?>
	        <form action="<?php echo BASE_LINK_BACK ?>main/login_post" method="POST" id="login">
	            <div class="inputwrapper login-alert">
	                <div class="alert alert-error">Invalid username or password</div>
	            </div>
	            <div class="inputwrapper">
	                <input type="text" name="username" id="username" placeholder="Enter any username" autocomplete="off"/>
	            </div>
	            <div class="inputwrapper">
	                <input type="password" name="password" id="password" placeholder="Enter any password" autocomplete="off" />
	            </div>
	            <div class="inputwrapper">
	                <button name="submit">Sign In</button>
	            </div>
	            <!--
	            <div class="inputwrapper">
	                <label><input type="checkbox" class="remember" name="signin" /> Keep me sign in</label>
	            </div>
	            -->
	        </form>
			<?php else:?>
				<div style="text-align: center;color:#fff;">
					We are currently under maintenance!!
				</div>
			<?php endif;?>
	    </div><!--loginpanelinner-->
	</div><!--loginpanel-->
	<div class="loginfooter">
	    <p>&copy; 2017. Portnick.com Panel. All Rights Reserved.</p>
	</div>	
	</body>
</html>