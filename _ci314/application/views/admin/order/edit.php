﻿<?php if($this->input->get('status')=="saved"):?>
<div style="position:relative">
<div style="position:absolute; width:100%; padding:0" class="alert alert-success" role="alert">
  <div style="padding:10px">
  <button style="right:5px" type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    Contents Saved
  </div>  
</div>
</div>
<?php endif;?>

    <div class="headtitle" style="margin-bottom:1px">
        <?php if(isset($lan)&&count($lan)>1):?>
        <div class="btn-group">
             <button class="btn dropdown-toggle" data-toggle="dropdown"> <i class="iconfa-flag"></i> Language : <?php echo strtoupper($this->session->userdata($pfile.'_slan'))?><span class="caret"></span></button>
             <ul class="dropdown-menu">
                <?php foreach($lan as $val):?>
                <li <?php if($this->session->userdata($pfile.'_slan')==$val):?>class="active"<?php endif;?>><a href="<?php echo BASE_LINK_BACK.$pfile.'/set_language/'.$val?>"><?php echo strtoupper($val)?></a></li>
                <?php endforeach?>
            </ul>
        </div>
        <?php endif;?> 
    </div>
    <?php
        include_once getcwd().BASE_INCLUDE_BACK.'/js/fckeditor/fckeditor.php';
        $i=0;
        foreach ($orders as $order) :
     ?>
    <div class="widgets">
    <h4 class="widgettitle">Detail</h4>
    <div class="widgetcontent">
    <form action="<?php echo BASE_LINK_BACK.$pfile?>/post" method="post" id="form" class="stdform" enctype="multipart/form-data" data-toggle="validator">
    
        <?php 
            switch ($order['status']) {
                case 1:
                    $show_status = '<span style="color:green">ส่งผู้สารถึงที่หมาย';
                    break;
                case 2:
                    $show_status = '<span style="color:#f2ad55">ค้นหาคนขับ';
                    break;
                case 3:
                    $show_status = '<span style="color:#726d00">กำลังดำเนินการส่งผู้โดยสาร';
                    break;
                default:
                    $show_status = '<span style="color:red">ยกเลิก';
                    break;
            }
        ?>
        <p class="form-group">
            <span >
                สถานะ : <?= $show_status?></span>
            </span><br><br>
            ปล. จะมีสถานะทั้งหมดดังนี้
            <ul style="margin-left:20px;">
                <li><span style="color:red">ยกเลิก</span></li>
                <li><span style="color:#f2ad55">ค้นหาคนขับ</span></li>
                <li><span style="color:#726d00">กำลังดำเนินการส่งผู้โดยสาร</span></li>
                <li><span style="color:green">ส่งผู้สารถึงที่หมาย</span></li>
            </ul>
        </p>
        <hr>

        <p class="form-group">
            <span >
                หมายเลข Order : <?=$order['number']?>
            </span>
        </p>
        <hr>

        <p class="form-group">
            <span >
                ผู้โดยสาร : <a href="<?php echo '/bnadmin/user/edit/'.$order['user_id']?>" target="_blank">คลิกดูรายละเอียด</a>
            </span>
        </p>
        <hr>

        <?php if($order['status'] == 3 || $order['status'] == 1):?>
        <p class="form-group">
            <span >
                คนขับรถ : <a href="<?php echo '/bnadmin/driver/edit/'.$order['driver_id']?>" target="_blank">คลิกดูรายละเอียด</a>
            </span>
        </p>
        <hr>
        <?php endif;?>
     
        <p class="form-group">
            <span >
                เร่ิมจาก : <?= $order['start_location']?> (<?= $order['start_location_lat']?>,<?= $order['start_location_long']?>)
            </span>
        </p>
        <hr>

        <p class="form-group">
            <span >
                ปลายทาง : <?= $order['destination']?> (<?= $order['destination_lat']?>,<?= $order['destination_long']?>)
            </span>
        </p>
        <hr>

        <p class="form-group">
            <span >
                ประเภทรถ : <img style="height:30px;" src="<?= $order['car_type_show']['icon']?>"> <?= $order['car_type_show']['name']?>
            </span>
        </p>
        <hr>

        <p class="form-group">
            <span >
                ข้อความจาก User : <?= $order['note']?>
            </span>
        </p>
        <hr>

        <p class="form-group">
            <span >
                ค่าบริการ : <?= $order['fare']?>
            </span>
        </p>
        <hr>

        <p class="form-group">
            <span >
                จ่ายโดย : <?= $order['payment_by']['card_number']?>
            </span>
        </p>
        <hr>

        <p class="form-group">
            <span >
                เวลาที่เริ่ม : <?= $order['create_on']?>
            </span>
        </p>
        <hr>

        <?php if($order['status'] == 2 || $order['status'] == 1):?>
        <p class="form-group">
            <span >
                เวลาที่รับลูกค้า : <?=$order['pickup_on'];?>
            </span>
        </p>
        <hr>
        <?php endif;?>

        <?php if($order['status'] == 0):?>
        <p class="form-group">
            <span >
                เวลาที่ลูกค้ากดยกเลิก หรือยกเลิกโดยระบบ : <?= $order['cancel_on']?>
            </span>
        </p>
        <hr>
        <?php endif;?>

        <?php if($order['status'] == 1):?>
        <p class="form-group">
            <span >
                เวลาที่ส่งลูกค้า : <?= $order['success_on']?>
            </span>
        </p>
        <hr>
        <?php endif;?>

        <?php if($vote != false):?>
        <p class="form-group">
            <span >
                Comment : <?= $vote[0]['comment']?>
            </span>
        </p>
        <p class="form-group">
            <span >
                Rate ที่ได้ : <?= $vote[0]['rate']?>
            </span>
            <br>ปล. จะมี Rate ทั้งหมด 0-5, 5 คือดีสุด
        </p>
        <hr>
        <?php endif;?>

    <?php if($can_edit):?>
    <div class="stdformbutton">
        <button type="submit" name="submit" class="btn btn-primary" >Update</button>
        <?php if((isset($cancel_btn)&&$cancel_btn)||!isset($cancel_btn)):?>
        <a href="<?php echo BASE_LINK_BACK.$redirect?>" class="btn" >Cancel</a>
        <?php endif;?>
        <input type="hidden" name="action" id="action" value="update"/>
        <input type="hidden" name="id" id="id" value="<?php echo $record->$primary_id?>"/>
        <input type="hidden" name="redirect" id="MM_action" value="<?php echo $redirect?>"/>
    </div>
    <?php endif;?>
 </form>
</div>
</div>




<?php endforeach;?>
<style>
.help-block.with-errors{ color: #a94442}
.help-block.with-errors ul{list-style: none;}
.form-group.has-error input[type="text"],
.form-group.has-error input[type="email"],
.form-group.has-error textarea{
  border-color: #a94442;
}

.widgets{margin-bottom: 20px; cursor: pointer;}
</style>
<link rel="stylesheet" href="<?php echo BASE_INCLUDE_BACK?>css/font-awesome.min.css" type="text/css" />
<link rel="stylesheet" href="<?php echo BASE_INCLUDE_BACK?>css/bootstrap-datetimepicker.min.css" type="text/css" />
<script type="text/javascript" src="<?php echo BASE_INCLUDE_BACK?>js/moment-with-locales.js"></script>
<script type="text/javascript" src="<?php echo BASE_INCLUDE_BACK?>js/bootstrap-datetimepicker.min.js"></script>
<script src="<?php echo BASE_INCLUDE_BACK?>js/fckeditor/fckeditor.js" type="text/javascript" ></script>

<script type="text/javascript" src="<?php echo BASE_INCLUDE_BACK?>js/validator.min.js"></script>
<script type="text/javascript">

function show(id){
  jQuery('#' + id).toggle();
}

jQuery(document).ready(function(){
    // FCKeditor
    jQuery("textarea.FCKeditor").each(function(){
        var oFCKeditor = new FCKeditor(this.name);
        oFCKeditor.BasePath	= '<?php echo BASE_INCLUDE_BACK?>/js/fckeditor/' ;
        oFCKeditor.Width = '700';
        oFCKeditor.Height = '500';
        oFCKeditor.ReplaceTextarea() ;
    });
        
    jQuery('.multi-check input[type="checkbox"]').click(function(){
        var p = $(this).parents('.multi-check');
        var val = [];
        p.find('input[type="checkbox"]').each(function(){
            if(jQuery(this).is(':checked')){
                val.push(jQuery(this).val());
            }
        });
        p.find('input[type="hidden"]').val(JSON.stringify(val));
    });
    setTimeout(function(){
      jQuery('.alert').fadeOut(300);
    },3000);
    
    jQuery('.multi-check').each(function(){
        var val = jQuery(this).find('input[type="hidden"]').val();
        if(val !=''){
            var arr = JSON.parse(val);
            $jQuery(this).find('input[type="checkbox"]').each(function(){
                var input = jQuery(this);
                if(arr.indexOf(input.val()) !== -1) {
                    input.attr('checked','checked');
                }
            });
        }
    });
     var db = jQuery('#dualselect').find('.ds_arrow button');  //get arrows of dual select
    var sel1 = jQuery('#dualselect select:first-child');    //get first select element
    var sel2 = jQuery('#dualselect select:last-child');     //get second select element
    
   // sel1.empty(); //empty it first from dom.
    
    db.click(function(){
      var t = (jQuery(this).hasClass('ds_prev'))? 0 : 1;  // 0 if arrow prev otherwise arrow next
      if(t) {
        sel1.find('option').each(function(){
          if(jQuery(this).is(':selected')) {
            jQuery(this).attr('selected',false);
            var op = sel2.find('option:first-child');
            sel2.append(jQuery(this));
          }
        }); 
      } else {
        sel2.find('option').each(function(){
          if(jQuery(this).is(':selected')) {
            jQuery(this).attr('selected',false);
            sel1.append(jQuery(this));
          }
        });   
      }

      var arr = [];
      sel1.find('option').each(function(){
        arr.push(jQuery(this).val());
      })
      jQuery('input[name="con_json_data1"]').val(JSON.stringify(arr));
      
      return false;
    });
    
});
</script>

<script type="text/javascript">
    jQuery.noConflict();
    (function($) {
      $(function() {
        $('.date').datetimepicker({
          format: 'YYYY-MM-DD HH:mm',
          icons:{
            time: 'fa fa-clock-o',
            date:'fa fa-calendar',
            up:'fa fa-chevron-up',
            down:'fa fa-chevron-down',
            previous:'fa fa-chevron-left',
            next:'fa fa-chevron-right',
            today:'fa fa-calendar-o'
        }});
      });
    })(jQuery);
</script>