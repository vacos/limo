<?php if($this->input->get('status')=="saved"):?>
<div style="position:relative">
<div style="position:absolute; width:100%; padding:0" class="alert alert-success" role="alert">
  <div style="padding:10px">
  <button style="right:5px" type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    Contents Saved
  </div>  
</div>
</div>
<?php endif;?>

    <div class="headtitle" style="margin-bottom:1px">
        <?php if(isset($lan)&&count($lan)>1):?>
        <div class="btn-group">
             <button class="btn dropdown-toggle" data-toggle="dropdown"> <i class="iconfa-flag"></i> Language : <?php echo strtoupper($this->session->userdata($pfile.'_slan'))?><span class="caret"></span></button>
             <ul class="dropdown-menu">
                <?php foreach($lan as $val):?>
                <li <?php if($this->session->userdata($pfile.'_slan')==$val):?>class="active"<?php endif;?>><a href="<?php echo BASE_LINK_BACK.$pfile.'/set_language/'.$val?>"><?php echo strtoupper($val)?></a></li>
                <?php endforeach?>
            </ul>
        </div>
        <?php endif;?> 
        <h4 class="widgettitle">
          <?php if((isset($btn_back)&&$btn_back)||!isset($btn_back)):?>
          <a href="<?php echo BASE_LINK_BACK.$redirect.'/'?>"  style="color:#fff"><i class=" iconfa-chevron-left"></i> BACK</a>
          <?php else:?>&nbsp;<?php endif?>
        </h4>
    </div>
    <div class="widgets">
    <h4 class="widgettitle">EDIT</h4>
    <div class="widgetcontent">
    <form action="<?php echo BASE_LINK_BACK.$pfile?>/post" method="post" id="form" class="stdform" enctype="multipart/form-data" data-toggle="validator">
    <?php if(isset($lan)&&count($lan)>0):?>
     <input type="hidden" value="<?php echo $this->session->userdata($pfile.'_slan')?>" name="<?php echo $prefix.'lan'?>" />
    <?php endif;?>
     <?php
        include_once getcwd().BASE_INCLUDE_BACK.'/js/fckeditor/fckeditor.php';
        $i=0;
        foreach($form as $key=>$val){
           echo ($label[$i]!='')?"<p class='form-group'><label>".$label[$i]." :</label>":'';
           echo ($label[$i]!='')?'<span class="field">':'';
           $attr_validete = isset($validate[$key])?$validate[$key]:'';
           if(is_array($val)){
               switch($val[0]):
                   case 'listbox':
                       //unset($val[0]);
                       echo "<select name='".$key."' id='".$key."'>";
                       foreach($val[1] as $key2=>$val2){
                           $chk = ($record->$key==$key2)?'selected':'';
                           echo "<option value='".$key2."' ".$chk.">".$val2."</option>";
                       }
                       echo "</select>";
                   break;
                   case 'listbox-group':
                       echo "<select name='".$key."' id='".$key."'>";
                       $ci = '';$n=1;
                       foreach($val[1] as $key2=>$val2){
                           $chk = ($record->$key==$key2)?'selected':'';
                           $close = false;
                           if($ci != $val2[1]){
                               echo ($ci!=$val2[1])?"<optgroup label=\"".$val2[0]."\">":'';
                               $ci = $val2[1];
                           }
                           echo "<option value='".$key2."' ".$chk.">".$val2[0]."</option>";
                       }
                       echo "</select>";
                   break;
                   case 'radio':
                       //unset($val[0]);
                       foreach($val[1] as $key2=>$val2){
                           $chk = ($record->$key==$val2)?'checked':'';
                           echo "<label><input name='".$key."' id='".$key."' value='".$val2."' type='radio' class='radio' ".$chk."/>".$key2."</label>";
                       }
                   break;
                   case 'text':
                       echo '&nbsp;&nbsp;'.$record->$val[1].' '.$record->$val[2];
                   break;
                   case 'file':
                       if($record->$key!=''){
                           $path = (isset($val[2])&&$val[2]!='')?base_url().$val[2]:base_url().'uploaded/content/';
                           if($val[1]=="image"){
                                   echo "<img src='".$path.$record->$key."'  style='margin:5px 0; max-width:400px'/><br/>";
                           }else{
                               echo " [<a href='".$path.$record->$key."' target='_blank'>".$record->$key."</a>]<br/>";
                           }
                           echo '<a href="'.BASE_LINK_BACK.$pfile.'/deletefile/'.$key.'/'.$record->$primary_id.'" onclick="return confirm(\'Delete this file!\')"><i class=" iconfa-trash"></i> Delete</a></br>';
                       }
                       if((isset($val[3])&&$val[3]!="V")||!isset($val[3])){
                        echo "<input name='".$key."' id='".$key."' type='file'/>";
                       }
                   break;
                   case 'hidden':
                       echo "<input name='".$key."' id='".$key."' value='".$val[1]."' type='hidden'/>";
                   break;
                   case 'multiselect':
                       echo "<span class='multi-check'>";
                       foreach($val[1] as $key2=>$val2){
                           echo "<span style='padding:2px 5px; display:inline-block; margin:2px 2px;'>";
                           echo "<input type='checkbox' value='".$key2."' name='_".$val[0]."' />".$val2;
                           echo "</span>";
                       }
                           echo "<input name='".$key."' value='".$record->$key."' type='hidden'/>";
                       echo "</span>";
                   break;
                   case 'dual-select':
                       $arr = json_decode($record->$key);
                       echo '
                            <span id="dualselect" class="dualselect" style="margin-left:0">
                              <select class="uniformselect" name="select3" multiple="multiple" size="10">
                              ';
                                foreach($val[1] as $key2=>$val2){
                                    if(in_array($key2,$arr)){
                                      echo '<option value="'.$key2.'">'.$val2.'</option>';
                                    }
                                }
                        echo '</select>
                                <span class="ds_arrow">
                                    <button class="btn ds_prev" type="button"><i class="iconfa-chevron-left"></i></button><br />
                                    <button class="btn ds_next" type="button"><i class="iconfa-chevron-right"></i></button>
                                </span>
                                <select name="select4" multiple="multiple" size="10">';
                                foreach($val[1] as $key2=>$val2){
                                    if(!in_array($key2,$arr)){
                                      echo '<option value="'.$key2.'">'.$val2.'</option>';
                                    }
                                }
                       echo '</select></span>';
                       echo "<input name='".$key."' value='".$record->$key."' type='hidden' autocomplete='off' />";
                   break;
               endswitch;
           }else{
               switch($val):
                    case 'select':
                       echo "<select name='".$key."' id='".$key."'>";
                       foreach($options[$key] as $key2 => $val2){
                          $selected = ($record->$key == $key2) ? 'selected' : '';
                          echo "<option ".$selected." value='".$key2."'>".$val2."</option>";
                       }
                       echo "</select>";
                    break;
                   case 'text':
                       echo "<input name='".$key."' id='".$key."' value='".$record->$key."' type='text' style='width:500px;' autocomplete='off'  ".$attr_validete."/>";
                   break;
                   case 'password':
                       echo "<input name='".$key."' id='".$key."' type='password' style='width:500px;' autocomplete='off'  ".$attr_validete."/>";
                   break;
                   case 'date':
                       echo "<input name='".$key."' id='".$key."' value='".$record->$key."' type='text' class='date'  ".$attr_validete."/>";
                   break;
                   case 'editor':
                       echo "<textarea name='".$key."' class='FCKeditor'>". str_replace($this->config->item('uploaded_url').'images/','/uploaded/images/',htmlspecialchars_decode($record->$key,ENT_QUOTES))."</textarea>";
                   break;
                   case 'textarea':
                       echo "<textarea name='".$key."' id='".$key."' style='width:500px; min-height:200px'  ".$attr_validete.">".$record->$key."</textarea>";
                   break;
                   case 'hidden':
                       echo "<input name='".$key."' id='".$key."' value='".$record->$key."' type='hidden'/>";
                   break;
                   case 'image':
                       if($record->$key!=''){

                           $path = ($record->type=="news") ? PATH_UPLOAD_NEWS : PATH_UPLOAD_SCOOP;

                           if($val[1]=="image"){
                                   echo "<img src='".$path.$record->$key."'  style='margin:5px 0; max-width:400px'/><br/>";
                           }else{
                               echo " [<a href='".$path.$record->$key."' target='_blank'>".$record->$key."</a>]<br/>";
                           }
                           //echo '<a href="'.BASE_LINK_BACK.$pfile.'/deletefile/'.$key.'/'.$record->$primary_id.'" onclick="return confirm(\'Delete this file!\')"><i class=" iconfa-trash"></i> Delete</a></br>';
                       }
                       if((isset($val[3])&&$val[3]!="V")||!isset($val[3])){
                        echo "<input name='".$key."' id='".$key."' type='file'/>";
                       }
                   break;
               endswitch;
           }
           if($label[$i]!=''){
              echo ($attr_validete!='')?'<span class="help-block with-errors"></span> ':'';
              echo "</span></p>";
           }
           $i++;
        }
     ?>
    <div class="stdformbutton">
        <button type="submit" name="submit" class="btn btn-primary" >Update</button>
        <?php if((isset($cancel_btn)&&$cancel_btn)||!isset($cancel_btn)):?>
        <a href="<?php echo BASE_LINK_BACK.$redirect?>" class="btn" >Cancel</a>
        <?php endif;?>
        <input type="hidden" name="action" id="action" value="update"/>
        <input type="hidden" name="id" id="id" value="<?php echo $record->slug?>"/>
        <input type="hidden" name="redirect" id="MM_action" value="<?php echo $redirect?>"/>
    </div>
 </form>
</div>
</div>
<style>
.help-block.with-errors{ color: #a94442}
.help-block.with-errors ul{list-style: none;}
.form-group.has-error input[type="text"],
.form-group.has-error input[type="email"],
.form-group.has-error textarea{
  border-color: #a94442;
}
</style>
<link rel="stylesheet" href="<?php echo BASE_INCLUDE_BACK?>css/font-awesome.min.css" type="text/css" />
<link rel="stylesheet" href="<?php echo BASE_INCLUDE_BACK?>css/bootstrap-datetimepicker.min.css" type="text/css" />
<script type="text/javascript" src="<?php echo BASE_INCLUDE_BACK?>js/moment-with-locales.js"></script>
<script type="text/javascript" src="<?php echo BASE_INCLUDE_BACK?>js/bootstrap-datetimepicker.min.js"></script>
<script src="<?php echo BASE_INCLUDE_BACK?>js/fckeditor/fckeditor.js" type="text/javascript" ></script>

<script type="text/javascript" src="<?php echo BASE_INCLUDE_BACK?>js/validator.min.js"></script>
<script type="text/javascript">
jQuery(document).ready(function(){
    // FCKeditor
    jQuery("textarea.FCKeditor").each(function(){
        var oFCKeditor = new FCKeditor(this.name);
        oFCKeditor.BasePath	= '<?php echo BASE_INCLUDE_BACK?>js/fckeditor/' ;
        oFCKeditor.Width = '700';
        oFCKeditor.Height = '500';
        oFCKeditor.ReplaceTextarea() ;
    });
        
    jQuery('.multi-check input[type="checkbox"]').click(function(){
        var p = $(this).parents('.multi-check');
        var val = [];
        p.find('input[type="checkbox"]').each(function(){
            if(jQuery(this).is(':checked')){
                val.push(jQuery(this).val());
            }
        });
        p.find('input[type="hidden"]').val(JSON.stringify(val));
    });
    setTimeout(function(){
      jQuery('.alert').fadeOut(300);
    },3000);
    
    jQuery('.multi-check').each(function(){
        var val = jQuery(this).find('input[type="hidden"]').val();
        if(val !=''){
            var arr = JSON.parse(val);
            $jQuery(this).find('input[type="checkbox"]').each(function(){
                var input = jQuery(this);
                if(arr.indexOf(input.val()) !== -1) {
                    input.attr('checked','checked');
                }
            });
        }
    });
     var db = jQuery('#dualselect').find('.ds_arrow button');  //get arrows of dual select
    var sel1 = jQuery('#dualselect select:first-child');    //get first select element
    var sel2 = jQuery('#dualselect select:last-child');     //get second select element
    
   // sel1.empty(); //empty it first from dom.
    
    db.click(function(){
      var t = (jQuery(this).hasClass('ds_prev'))? 0 : 1;  // 0 if arrow prev otherwise arrow next
      if(t) {
        sel1.find('option').each(function(){
          if(jQuery(this).is(':selected')) {
            jQuery(this).attr('selected',false);
            var op = sel2.find('option:first-child');
            sel2.append(jQuery(this));
          }
        }); 
      } else {
        sel2.find('option').each(function(){
          if(jQuery(this).is(':selected')) {
            jQuery(this).attr('selected',false);
            sel1.append(jQuery(this));
          }
        });   
      }

      var arr = [];
      sel1.find('option').each(function(){
        arr.push(jQuery(this).val());
      })
      jQuery('input[name="con_json_data1"]').val(JSON.stringify(arr));
      
      return false;
    });
    
});
</script>
<?php echo isset($script)?$script:''?>
<script type="text/javascript">
    jQuery.noConflict();
    (function($) {
      $(function() {
        $('.date').datetimepicker({
          format: 'YYYY-MM-DD HH:mm',
          icons:{
            time: 'fa fa-clock-o',
            date:'fa fa-calendar',
            up:'fa fa-chevron-up',
            down:'fa fa-chevron-down',
            previous:'fa fa-chevron-left',
            next:'fa fa-chevron-right',
            today:'fa fa-calendar-o'
        }});
      });
    })(jQuery);
</script>