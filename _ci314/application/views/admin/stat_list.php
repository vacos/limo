    
    <?php if(isset($lan)&&count($lan)>1):?>
    <div class="headtitle" style="margin-bottom:1px; position:relative">
        <?php if($pfile=="home_info"):?>
        <div class="btn" style="position:absolute;line-height:30px; padding-left:35px" data-toggle="modal" data-target="#mapPosition">
            Map Position   
            <img src="<?php echo BASE_INCLUDE_BACK?>images/home-position-map.jpg" width="25" style="position:absolute; left:1px; top:1px">
        </div> 
        <div class="modal fade" id="mapPosition" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Map Position </h4>
              </div>
              <div class="modal-body">
                <img src="<?php echo BASE_INCLUDE_BACK?>images/home-position-map.jpg" class="img-responsive">
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
        <?php endif;?>
        <h4 class="widgettitle">&nbsp;</h4>
    </div>
    <?php endif;?>   
    <?php if (isset($form_search) && count($form_search) > 0): ?>
    <div  class="dataTables_wrapper">
        <div class="dataTables_length">
        <form action="<?php echo BASE_LINK_BACK ?><?php echo $pfile ?>/index/search" method="post" id="form_search"  name="form_search" class="form_search">
                <span style="padding:10px; color:#888; font-size:15px ">Filter : </span>
                <?php foreach ($form_search as $key => $val): ?>
                        <?php
                        $value = isset($key_search[$key]) ? $key_search[$key] : '';
                        if ($val[0] == "text") {
                            echo '<input name="' . $key . '" value="' . $value . '" type="text"  placeholder="'.$key.'"  style="margin-bottom:0px"/>';
                        } elseif ($val[0] == "inlist") {
                            echo "<select name='" . $key . "' style='margin-bottom:0px'>";
                                echo "<option value=''>".$key."</option>";
                            foreach ($val[2] as $key2 => $val2) {
                                $chk = ($value == $key2) ? 'selected' : '';
                                echo "<option value='" . $key2 . "' " . $chk . ">" . $val2 . "</option>";
                            }
                            echo "</select>";
                        }elseif($val[0] == "sdate"||$val[0] == "edate"){
                            echo "<input name='".$key."' id='".$key."' value='". $value . "' type='text' class='datepicker' />";
                        }
                ?>
                <?php endforeach; ?>
                <button class="btn btn-primary" style="margin-bottom:0px"><i class="iconfa-search"></i> Submit</button>
        </form>
        </div>
        <div class="dataTables_filter"><lable >Items : <?php echo number_format($total_rows,0)?></lable></div>
    </div> 
    <?php endif; ?>      
    <form action="<?php echo BASE_LINK_BACK ?><?php echo $pfile ?>/del_all" method="post" id="form1"  name="form1">
    <div>   
        <table  class="table table-bordered">
            <thead>
                <tr>
                    <th>Page</th>
                    <th>Members</th>
                    <th>Views</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($result->result() as $key => $val):?>
                <td><?php echo "Member/".ucfirst($val->page)?></td>
                <td><span class="badge" style="background:#3B6C8E"><?php echo $val->num_mem?></span></td>
                <td><span class="badge" style="background:#3B6C8E"><?php echo $val->num?></span></td>
                <?php endforeach?>
            </tbody>
        </table>
        <?php if (isset($del_link) && $del_link || !isset($del_link)): ?>
            <button class="btn" type="submit" onclick="return confirm('Delete items.')" ><i class="iconfa-remove"></i> Delete Items</button>
        <?php endif; ?>
        <?php if (isset($add_link) && $add_link): ?>
            <a href="<?php echo BASE_LINK_BACK ?><?php echo $add_link; ?>" class="btn"><i class="iconfa-plus"></i> Add</a>
        <?php endif; ?>
        <?php if (isset($export_link) && $export_link): ?>
            <a href="<?php echo BASE_LINK_BACK ?><?php echo $pfile ?>/export" class="btn"></a>
        <?php endif; ?>
        </form>
    </div>
