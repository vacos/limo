<script type="text/javascript">
jQuery(document).ready(function(){
	   jQuery('#form').submit(function(){
	  		if(jQuery('#old_pass').val()==""){
				alert('Please fill current password');
				 jQuery('#old_pass').focus();
				return false;
			}
	  		if(jQuery('#news_pass').val()==""){
				alert('Please fill news password');
				$('#news_pass').focus();
				return false;
			}
	  		if(jQuery('#news_pass').val()!=jQuery('#con_pass').val()){
				alert('Two password  not match');
				jQuery('#con_pass').focus();
				return false;
			}
	  });
  });
</script>
<h2>Reset Password</h2>
<?php if($err!=""):?>
     <div class="error_box">
        Current password is not true.
     </div>
<?php endif;?>	 
 <form action="<?php echo BASE_LINK_BACK ?>main/password_post" method="post" id="form"   class="niceform">
    <fieldset>
    <p>
        <label for='lf'>Current Password :</label>
        <input type="password" name="old_pass" id="old_pass" value="" class='lf'/>
    </p>
    <p>
        <label for='lf'>New Password :</label>
        <input type="password" name="news_pass" id="news_pass" value="" class='lf'/>
    </p>
    <p>
        <label for='lf'>Confirm Password :</label>
        <input type="password" name="con_pass" id="con_pass" value="" class='lf'/>
    </p>
    <div class="line1"></div>
    <div class="submit">
        <input type="submit" name="submit" class="button"  value="Save"/>
    </div>
    </fieldset>
 </form>