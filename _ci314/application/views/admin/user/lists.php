    
    <?php if(isset($lan)&&count($lan)>1):?>
    <div class="headtitle" style="margin-bottom:1px; position:relative">
        <?php if($pfile=="home_info"):?>
        <div class="btn" style="position:absolute;line-height:30px; padding-left:35px" data-toggle="modal" data-target="#mapPosition">
            Map Position   
            <img src="<?php echo BASE_INCLUDE_BACK?>images/home-position-map.jpg" width="25" style="position:absolute; left:1px; top:1px">
        </div> 
        <div class="modal fade" id="mapPosition" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Map Position </h4>
              </div>
              <div class="modal-body">
                <img src="<?php echo BASE_INCLUDE_BACK?>images/home-position-map.jpg" class="img-responsive">
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
        <?php endif;?>
        <div class="btn-group">
             <button class="btn dropdown-toggle" data-toggle="dropdown"> <i class="iconfa-flag"></i> Language : <?php echo strtoupper($this->session->userdata($pfile.'_slan'))?><span class="caret"></span></button>
             <ul class="dropdown-menu">
                <?php foreach($lan as $val):?>
                <li <?php if($this->session->userdata($pfile.'_slan')==$val):?>class="active"<?php endif;?>><a href="<?php echo BASE_LINK_BACK.$pfile.'/set_language/'.$val?>"><?php echo strtoupper($val)?></a></li>
                <?php endforeach?>
            </ul>
        </div>
        <h4 class="widgettitle">&nbsp;</h4>
    </div>
    <?php endif;?>   
    <?php if (isset($form_search) && count($form_search) > 0): ?>
    <div  class="dataTables_wrapper">
        <div class="dataTables_length">
        <form action="<?php echo BASE_LINK_BACK ?><?php echo $pfile ?>/index/search" method="post" id="form_search"  name="form_search" class="form_search">
                <span style="padding:10px; color:#888; font-size:15px ">Filter : </span>
                <?php foreach ($form_search as $key => $val): ?>
                        <?php
                        $value = isset($key_search[$key]) ? $key_search[$key] : '';
                        if ($val[0] == "text") {
                            echo '<input name="' . $key . '" value="' . $value . '" type="text"  placeholder="'.$key.'"  style="margin-bottom:0px"/>';
                        } elseif ($val[0] == "inlist") {
                            echo "<select name='" . $key . "' style='margin-bottom:0px'>";
                                echo "<option value=''>".$key."</option>";
                            foreach ($val[2] as $key2 => $val2) {
                                $chk = ($value == $key2) ? 'selected' : '';
                                echo "<option value='" . $key2 . "' " . $chk . ">" . $val2 . "</option>";
                            }
                            echo "</select>";
                        }elseif($val[0] == "sdate"||$val[0] == "edate"){
                            echo "<input name='".$key."' id='".$key."' value='". $value . "' type='text' class='datepicker' />";
                        }
                ?>
                <?php endforeach; ?>
                <button class="btn btn-primary" style="margin-bottom:0px"><i class="iconfa-search"></i> Submit</button>
        </form>
        </div>
        <div class="dataTables_filter"><lable >Items : <?php echo number_format($total_rows,0)?></lable></div>
    </div> 
    <?php endif; ?>      
    <form action="<?php echo BASE_LINK_BACK ?><?php echo $pfile ?>/del_all" method="post" id="form1"  name="form1">
    <div>   

        <?php if (isset($del_link) && $del_link || !isset($del_link)): ?>
            <button class="btn" type="submit" onclick="return confirm('Delete items.')" ><i class="iconfa-remove"></i> Delete Items</button>
        <?php endif; ?>
        <?php if (isset($add_link) && $add_link): ?>
            <a href="<?php echo BASE_LINK_BACK ?><?php echo $add_link; ?>" class="btn"><i class="iconfa-plus"></i> Add</a>
        <?php endif; ?>

        <table  class="table table-bordered">
            <thead>
                <tr>
                    <?php $hide_col = 0; ?>
                    <?php if (isset($del_link) && $del_link || !isset($del_link)): ?>
                    <th><input type="checkbox" name="checkAll" class="chkall"   /></th>
                    <?php else:$hide_col++;endif;?>
                    <?php foreach ($table as $key => $val): ?>
                        <th><?php echo $key ?></th>
                    <?php endforeach; ?>
                    <?php if ((isset($status_link) && $status_link) || !isset($status_link)): ?>
                        <th>Status</th>
                    <?php else:$hide_col++;endif;?>
                    <?php if ($gallery_link || $edit_link || $del_link): ?>
                        <th>Manage</th>
                    <?php endif; ?>
                </tr>
            </thead>
            <tbody>
                <?php $i = 1; if ($total_rows > 0): ?>
                <?php foreach ($result as $val): ?>
                        <tr <?php echo ($i % 2 == 0) ? '' : 'class="odd""' ?>>
                            <?php if (isset($del_link) && $del_link || !isset($del_link)): ?>
                            <td>
                                <input type="checkbox" name="del[]" class="chkbox" value="<?php echo $val[$prefix . 'id'] ?>"  />
                            </td>
                            <?php endif;?>
                                <?php foreach ($table as $val2): ?>
                                <td>
                                    <?php
                                    if (is_array($val2[1])) {
                                        if ($val2[1][0] == 'inlist') {
                                            $tl = $val2[1][1];
                                            echo isset($tl[$val[$val2[0]]]) ? $tl[$val[$val2[0]]] : ' - ';
                                        } elseif ($val2[1][0] == 'link') {
                                            echo "<a href=\"" . BASE_LINK_BACK . $val2[1][1] . '/' . $val[$prefix . 'id'] . "\"><img src=\"" . base_url() . "includes/images/" . $val2[1][2] . "\"></a>";
                                        } elseif ($val2[1][0] == 'link-txt') {
                                            echo "<a href=\"" . BASE_LINK_BACK . $val2[1][1] . '/' . $val[$prefix . 'id'] . "\" >" . $val2[1][2] . "</a>";
                                        } else {
                                            echo "err";
                                        }
                                    } else {
                                        switch ($val2[1]):
                                            case'image':
                                                echo "<div style='text-align:center'>";
                                                if ($val[$val2[0]] != '') {
                                                    $path = isset($val2[2])?$val2[2]:'uploaded/content/';
                                                    echo "<img src='" . base_url() . $path . $val[$val2[0]] . "' style='max-height:120px; max-width:120px' />";
                                                } else {
                                                    echo "<span style='font-size:50px'><i class='iconfa-picture'></i></span>";
                                                }
                                                echo "</div>";
                                                break;
                                            case'text':
                                                if($val2[0] == 'used'){
                                                    echo $val[$val2[0]].' / '.$val['total'];
                                                }
                                                else{
                                                    echo $val[$val2[0]];
                                                }
                                                break;
                                            case'badge':
                                                echo "<span class='badge' style='background:red;'>".$val[$val2[0]]."</span>";
                                                break;
                                            case'order':
                                                echo '<div class="btn-group">';
                                                $order = ($val[$val2[0]] != '') ? $val[$val2[0]] : $i;
                                                if ($i != 1) {
                                                    echo "<a href=\"" . BASE_LINK_BACK . $pfile . "/seq/" . $val[$val2[0]] . "/up\" class='btn'><i class='iconfa-arrow-up'></i></a>";
                                                }
                                                echo '<a class="btn">' . $order . '</a>';
                                                if ($total_rows != $i) {
                                                    echo "<a href=\"" . BASE_LINK_BACK . $pfile . "/seq/" . $order . "/down\" class='btn'><i class='iconfa-arrow-down'></i></a>";
                                                }
                                                echo '</div>';
                                                break;
                                             case'link-id':
                                                 $url = BASE_LINK."new_content/index/".$val[$prefix . 'id'] ;
                                                 echo "<a herf='".$url."'>".$url."</a>";
                                                 break;
                                        endswitch;
                                    }
                                    ?>
                                </td>
                        <?php endforeach; ?>
                        <?php if ((isset($status_link) && $status_link) || !isset($status_link)): ?>
                        <td>
                            <div class="btn-group">
                                <button class="btn dropdown-toggle <?php echo ($val[$prefix . 'status'] == "1")?"btn-primary":""?>" data-toggle="dropdown"><?php echo ($val[$prefix . 'status'] == "1") ? 'Show' : 'Hide&nbsp;&nbsp;' ?> <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="<?php echo BASE_LINK_BACK ?><?php echo $pfile ?>/status/<?php echo $val[$prefix . 'id'] ?>/1">Show</a></li>
                                    <li><a href="<?php echo BASE_LINK_BACK ?><?php echo $pfile ?>/status/<?php echo $val[$prefix . 'id'] ?>/0">Hide </a></li>
                                </ul>
                            </div>
                        </td>
                        <?php endif; ?>
                        <?php if ($gallery_link || $edit_link || $del_link): ?>
                        <td>
                            <?php if ((isset($gallery_link) && $gallery_link)): ?>
                                <a href="<?php echo BASE_LINK_BACK ?><?php echo $gallery_link . '/' . $val[$prefix . 'id']; ?>" class="btn"><span class="iconfa-picture"></span></a>
                            <?php endif; ?>
                            <?php if ((isset($edit_link) && $edit_link) || !isset($edit_link)): ?>
                                <a href="<?php echo BASE_LINK_BACK ?><?php echo $pfile ?>/edit/<?php echo $val[$prefix . 'id']; ?>" class="btn"><i class=" iconfa-pencil"></i></a>
                            <?php endif; ?>
                            <?php if (isset($del_link) && $del_link || !isset($del_link)): ?>
                                <a href="<?php echo BASE_LINK_BACK ?><?php echo $pfile ?>/delete/<?php echo $val[$prefix . 'id'] ?>" class="btn" onclick="return confirm('Delete item.')"><i class="iconfa-trash"></i></a>
                            <?php endif; ?>
                            <?php if (isset($export_link) && $export_link): ?>
                                <a href="<?php echo BASE_LINK_BACK ?><?php echo $pfile ?>/export/<?php echo $val[$prefix . 'id'] ?>" target="_blank" class="btn"><i class="icon-download-alt"></i></a>
                            <?php endif; ?>
                        </td>
                        <?php else: $hide_col++;endif; ?>
                    </tr>
            <?php $i++; endforeach; ?>
            <?php else: ?>
            <tr>
                <td colspan="<?php echo count($table) + 3 - $hide_col ?>" align="center" class="rounded"  >
                    <span class="label label-info"> <i class="iconfa-reorder"></i> no record </span>
                </td>
            </tr>
            <?php endif; ?>
            </tbody>
        </table>
        </form>
    </div>
