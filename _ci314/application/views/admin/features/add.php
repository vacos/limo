    
    <div class="headtitle" style="margin-bottom:1px">
        <?php if(isset($lan)&&count($lan)>1):?>
        <div class="btn-group">
             <button class="btn" > <i class="iconfa-flag"></i> First - Language : <?php echo strtoupper($this->session->userdata($pfile.'_slan'))?></button>
        </div>
        <?php endif;?> 
        <h4 class="widgettitle">
          <a href="<?php echo BASE_LINK_BACK.$redirect?>" style="color:#fff"><i class="iconfa-chevron-left"></i> BACK</a>
        </h4>
    </div>
    <div class="widgets">
    <h4 class="widgettitle">ADD</h4>
    <div class="widgetcontent">

    <form action="<?php echo BASE_LINK_BACK.$pfile?>/post" method="post" id="form" class="stdform"  enctype="multipart/form-data" data-toggle="validator">
    <?php if(isset($lan)&&count($lan)>0):?>
     <input type="hidden" value="<?php echo $this->session->userdata($pfile.'_slan')?>" name="<?php echo $prefix.'lan'?>" />
    <?php endif;?>
        <?php
        include_once getcwd().BASE_INCLUDE_BACK.'/js/fckeditor/fckeditor.php';
        $i=0;
        foreach($form as $key=>$val){
           echo ($label[$i]!='')?"<p class='form-group'><label>".$label[$i]." :</label>":'';
           echo ($label[$i]!='')?'<span class="field">':'';
           $attr_validete = isset($validate[$key])?$validate[$key]:'';
           if(is_array($val)){
               switch($val[0]):
                   case 'listbox':
                       //unset($val[0]);
                       echo "<select name='".$key."' id='".$key."'>";
                       foreach($val[1] as $key2=>$val2){
                           echo "<option value='".$key2."'>".$val2."</option>";
                       }
                       echo "</select>";
                   break;
                   case 'listbox-group':
                       echo "<select name='".$key."' id='".$key."'>";
                       $ci = '';$n=1;
                       foreach($val[1] as $key2=>$val2){
                           $close = false;
                           if($ci != $val2[1]){
                               echo ($ci!=$val2[1])?"<optgroup label=\"".$val2[0]."\">":'';
                               $ci = $val2[1];
                           }
                           echo "<option value='".$key2."'>".$val2[0]."</option>";
                       }
                       echo "</select>";
                   break;
                   case 'radio':
                       //unset($val[0]);
                       foreach($val[1] as $key2=>$val2){
                           echo "<label><input name='".$key."' id='".$key."' value='".$val2."' type='radio' class='radio'/>".$key2."</label>";
                       }
                   break;
                   case 'file':
                       echo "<input name='".$key."' id='".$key."' type='file' ".$attr_validete."/>";
                   break;
                   case 'hidden':
                       echo "<input name='".$key."' id='".$key."' value='".$val[1]."' type='hidden'/>";
                   break;
                   case 'multiselect':
                       echo "<span class='multi-check'>";
                       foreach($val[1] as $key2=>$val2){
                           echo "<span style='padding:2px 5px; display:inline-block; margin:2px 2px;'>";
                           echo "<input type='checkbox' value='".$key2."' name='_".$val[0]."' />".$val2;
                           echo "</span>";
                       }
                           echo "<input name='".$key."' value='' type='hidden'/>";
                       echo "</span>";
                   break;
                   case 'dual-select':
                      
                       echo '
                            <span id="dualselect" class="dualselect" style="margin-left:0">
                              <select class="uniformselect" name="select3" multiple="multiple" size="10"></select>
                                <span class="ds_arrow">
                                    <button class="btn ds_prev" type="button"><i class="iconfa-chevron-left"></i></button><br />
                                    <button class="btn ds_next" type="button"><i class="iconfa-chevron-right"></i></button>
                                </span>
                                <select name="select4" multiple="multiple" size="10">';
                                foreach($val[1] as $key2=>$val2){
                                    echo '<option value="'.$key2.'">'.$val2.'</option>';
                                }
                       echo '</select></span>';
                       echo "<input name='".$key."' value='' type='hidden' autocomplete='off' />";
                   break;

               endswitch;
           }else{
               switch($val):
                   case 'text':
                       echo "<input name='".$key."' id='".$key."' value='' type='text' style='width:500px;' autocomplete='off' ".$attr_validete." class='form-control'/>";
                   break;
                   case 'password':
                       echo "<input name='".$key."' id='".$key."' type='password' style='width:500px;' autocomplete='off' ".$attr_validete."/>";
                   break;
                   case 'date':
                       echo "<input name='".$key."' id='".$key."' value='' type='text' class='date'  ".$attr_validete."/>";
                   break;
                   case 'editor':
                       /*
                       $oFCKeditor = new FCKeditor($key);
                       $oFCKeditor->BasePath = base_url()."includes/js/fckeditor/";
                       $oFCKeditor->Value =  " ";
                       $oFCKeditor->Width = ($pfile=="business3")?600:850;
                       $oFCKeditor->Height = ($pfile=="business3")?350:500;
                       $oFCKeditor->Create();*/
                       echo "<textarea name='".$key."' class='FCKeditor'></textarea>";
                   break;
                   case 'textarea':
                       echo "<textarea name='".$key."' id='".$key."' style='width:500px; min-height:200px' ".$attr_validete."/></textarea>";
                   break;
                   case 'hidden':
                       echo "<input name='".$key."' id='".$key."' value='' type='hidden'/>";
                   break;
               endswitch;
           }
           if($label[$i]!=''){
              echo ($attr_validete!='')?'<span class="help-block with-errors"></span> ':'';
              echo "</span></p>";
           }

           //
           $i++;
        }
     ?>
    <div class="stdformbutton">
        <button type="submit" name="submit" class="btn btn-primary" >Save</button>
        <a class="btn"   href="<?php echo BASE_LINK_BACK.$redirect?>">Cancel</a>
        <input type="hidden" name="action" id="action" value="create"/>
        <input type="hidden" name="redirect" id="MM_action" value="<?php echo $redirect?>"/>
    </div>
 </form>
</div>
</div>
<style>
.help-block.with-errors{ color: #a94442}
.help-block.with-errors ul{list-style: none;}
.form-group.has-error input[type="text"],
.form-group.has-error input[type="email"],
.form-group.has-error textarea{
  border-color: #a94442;
}
</style>
<link rel="stylesheet" href="<?php echo BASE_INCLUDE_BACK?>css/font-awesome.min.css" type="text/css" />
<link rel="stylesheet" href="<?php echo BASE_INCLUDE_BACK?>css/bootstrap-datetimepicker.min.css" type="text/css" />
<script type="text/javascript" src="<?php echo BASE_INCLUDE_BACK?>js/moment-with-locales.js"></script>
<script type="text/javascript" src="<?php echo BASE_INCLUDE_BACK?>js/bootstrap-datetimepicker.min.js"></script>
<script src="<?php echo BASE_INCLUDE_BACK?>js/fckeditor/fckeditor.js" type="text/javascript" ></script>

<script type="text/javascript" src="<?php echo BASE_INCLUDE_BACK?>js/validator.min.js"></script>
<script type="text/javascript">
jQuery(document).ready(function(){
    // FCKeditor
    jQuery("textarea.FCKeditor").each(function(){
        var oFCKeditor = new FCKeditor(this.name);
        oFCKeditor.BasePath	= '<?php echo BASE_INCLUDE_BACK?>js/fckeditor/' ;
        oFCKeditor.Width = '700';
        oFCKeditor.Height = '500';
        oFCKeditor.ReplaceTextarea() ;
    });
    
    jQuery('.multi-check input[type="checkbox"]').click(function(){
        var p = jQuery(this).parents('.multi-check');
        var val = [];
        p.find('input[type="checkbox"]').each(function(){
            if(jQuery(this).is(':checked')){
                val.push(jQuery(this).val());
            }
        });
        p.find('input[type="hidden"]').val(JSON.stringify(val));
    });

    var db = jQuery('#dualselect').find('.ds_arrow button');  //get arrows of dual select
    var sel1 = jQuery('#dualselect select:first-child');    //get first select element
    var sel2 = jQuery('#dualselect select:last-child');     //get second select element
    
    sel1.empty(); //empty it first from dom.
    
    db.click(function(){
      var t = (jQuery(this).hasClass('ds_prev'))? 0 : 1;  // 0 if arrow prev otherwise arrow next
      if(t) {
        sel1.find('option').each(function(){
          if(jQuery(this).is(':selected')) {
            jQuery(this).attr('selected',false);
            var op = sel2.find('option:first-child');
            sel2.append(jQuery(this));
          }
        }); 
      } else {
        sel2.find('option').each(function(){
          if(jQuery(this).is(':selected')) {
            jQuery(this).attr('selected',false);
            sel1.append(jQuery(this));
          }
        });   
      }

      var arr = [];
      sel1.find('option').each(function(){
        arr.push(jQuery(this).val());
      })
      jQuery('input[name="con_json_data1"]').val(JSON.stringify(arr));
      
      return false;
    });
});
</script>
<?php echo isset($script)?$script:''?>
<script type="text/javascript">
    jQuery.noConflict();
    (function($) {
      $(function() {
        $('.date').datetimepicker({
          format: 'YYYY-MM-DD HH:mm',
          icons:{
            time: 'fa fa-clock-o',
            date:'fa fa-calendar',
            up:'fa fa-chevron-up',
            down:'fa fa-chevron-down',
            previous:'fa fa-chevron-left',
            next:'fa fa-chevron-right',
            today:'fa fa-calendar-o'
        }});
      });
    })(jQuery);
</script>