<?php 
$menus = array(
    'main/home'=>'Home',
    'user'=>'User',
    'driver'=>'Driver',
    'page'=>'Page',
    'page_driver'=>'Page Driver',
    //'ads'=>'Ads',
    );
$submenus = array(
    'page'=> array(
                    'page/edit/legal'=>'Legal',
                    'page/edit/term'=>'Term & Condition',
                    'page/edit/refund'=>'Refund',
                    'page/edit/privacy'=>'Privacy & Policy',
                ),
    'page_driver'=> array(
                    'page/edit/term_driver'=>'Term & Condition',
                    'page/edit/privacy_driver'=>'Privacy & Policy',
                    'page/edit/alert_driver'=>'Alert Driver',
                ),
);

if($this->session->userdata('admin_level')==2){
    $menus = array('order'=>'Orders','new_order'=>'New Orders');
}
//$submenus['home'] = array('home_banners'=>'Banners','home_info'=>'Shortcut Boxes');
//$submenus['ecard'] = array('ecard');
$pfile = isset($pfile) ? $pfile : '';
// --------- CONFIG MENU -----------
?>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Portnick | <?php echo DOMAIN?></title>
<link rel="shortcut icon" href="/assets/images/favicon.ico" />
<link rel="stylesheet" href="<?php echo BASE_INCLUDE_BACK?>css/style.default.css" type="text/css" />
<link rel="stylesheet" href="<?php echo BASE_INCLUDE_BACK?>css/style.navyblue.css" type="text/css" />
<script type="text/javascript" src="<?php echo BASE_INCLUDE_BACK?>js/jquery-2.0.3.min.js"></script>
<script type="text/javascript" src="<?php echo BASE_INCLUDE_BACK?>js/jquery-migrate-1.1.1.min.js"></script>
<script type="text/javascript" src="<?php echo BASE_INCLUDE_BACK?>js/modernizr.min.js"></script>
<script type="text/javascript" src="<?php echo BASE_INCLUDE_BACK?>js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo BASE_INCLUDE_BACK?>js/custom.js"></script>
<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="<?php echo BASE_INCLUDE_BACK?>js/excanvas.min.js"></script><![endif]-->


<link href="/assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<link href="/assets/css/datetime/bootstrap-datetimepicker.min.css" rel="stylesheet">
<script type="text/javascript" src="<?php echo BASE_INCLUDE_BACK?>js/moment-with-locales.js"></script>
<script type="text/javascript" src="<?php echo BASE_INCLUDE_BACK?>js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript">

    function confirmAction() {
        var response = confirm("Are you sure?");
        if ( response == true ){}
        else{return false;}
    }

    <?php if($this->session->flashdata('alert') != ''):?>
    alert('<?php echo $this->session->flashdata('alert')?>');
    <?php endif;?>
</script>
</head>
<body>
<div class="mainwrapper">
    <div class="header">
        <div class="logo" style=" padding:12px 0; ">
            <a href="<?php echo BASE_LINK_BACK?>">
                <h1 style="color:#fff"><?php echo DOMAIN?></h1>
            </a>
        </div>
        <div class="headerinner">
            <ul class="headmenu">
                <li class="right">
                    <div class="userloggedinfo">
                        <img src="<?php echo BASE_INCLUDE_BACK?>images/photos/thumb1.png" alt="" />
                        <div class="userinfo">
                            <h5><?php echo $this->session->userdata('admin_user')?> <small>- <?php echo DOMAIN?></small></h5>
                            <ul>
                                <li><a href="<?php echo BASE_LINK_BACK?>main/password">Edit Profile</a></li>
                                <!--<li><a href="">Account Settings</a></li>-->
                                <li><a href="<?php echo BASE_LINK_BACK?>main/logout">Sign Out</a></li>
                            </ul>
                        </div>
                    </div>
                </li>
            </ul><!--headmenu-->
        </div>
    </div>
    <div class="leftpanel">
        <div class="leftmenu">        
            <ul class="nav nav-tabs nav-stacked">
                <li class="nav-header">Navigation</li>
                <?php foreach ($menus as $link=>$menu):?>
                <?php if(isset($submenus[$link])):?>
                <li class="dropdown"><a href=""><span class="iconfa-list"></span> <?php echo $menu?></a>
                    <ul <?php if(in_array($pfile, $submenus[$link])):?>style="display:block"<?php endif;?>>
                        <?php foreach ($submenus[$link] as $link=>$submenu):?>
                        <li <?php if($pfile == $link):?>class="active"<?php endif;?>>
                            <a href="<?php echo BASE_LINK_BACK.$link?>"><?php echo ucfirst($submenu)?></a>
                        </li>
                        <?php endforeach;?>
                    </ul>
                </li>
                <?php else:?>
                 <li <?php if($pfile == $link):?>class="active"<?php endif;?>><a href="<?php echo BASE_LINK_BACK.$link?>"><span class=" iconfa-list"></span> <?php echo $menu?></a></li>
                <?php endif;?>
                <?php endforeach;?>
            </ul>
        </div><!--leftmenu-->
    </div><!-- leftpanel -->
    <div class="rightpanel">
        <!--pageheader-->
        <ul class="breadcrumbs">
            <li>
                <a href="<?php echo BASE_LINK_BACK?>"> <i class="iconfa-home"></i></a>
                <span class="separator"></span> 
            </li>
            <?php if(!isset($breadcrumbs)||(isset($breadcrumbs)&&count($breadcrumbs)==1)):?>
            <li>
                <a href="<?php echo BASE_LINK_BACK?><?php echo isset($pfile)?$pfile:""?>"> <?php echo isset($title)?$title:''?></a>
                <?php if(isset($breadcrumbs)):?><span class="separator"></span><?php endif;?>
            </li>
            <?php endif;?>
            <?php if(isset($breadcrumbs)):?>
            <?php $i=1;foreach($breadcrumbs as $tt=>$path):?>
            <li>
                <a <?php echo ($path!='')?'href="'.BASE_LINK_BACK.$path.'"':''?>><?php echo $tt?></a>
                <?php if(count($breadcrumbs)>$i):?><span class="separator"></span><?php endif;?>
            </li>
            <?php $i++;endforeach?>
            <?php endif;?>
        </ul>
        <div class="pageheader">
            <div class="pageicon">
                <?php $icon = array("table"=>"iconfa-table","form"=>"iconfa-edit","box"=>"iconfa-picture")?>
                <span class="<?php echo (isset($display)&&isset($icon[$display]))?$icon[$display]:'iconfa-laptop';?>"></span>
            </div>
            <div class="pagetitle">
                <h5>&nbsp;</h5>
                <h1><?php echo isset($title)?$title:''?></h1>
            </div>
        </div>
        <div class="maincontent">
            <div class="maincontentinner">
                <?php echo isset($body)?$body:''?>
                <div class="footer">
                    <div class="footer-left">
                        <span>&copy; Portnick.com CMS. All Rights Reserved.</span>
                    </div>
                    <div class="footer-right">
                        <span> <a href="#"> </a></span>
                    </div>
                </div><!--footer-->
            </div><!--maincontentinner-->
        </div><!--maincontent-->
    </div><!--rightpanel-->
</div><!--mainwrapper-->
<script>
jQuery(function(){
    jQuery('.leftmenu .nav-tabs>.dropdown').each(function(){
        var dis = jQuery(this);
        if(dis.find('.active').length>0){
            dis.addClass('active');
            dis.find('ul').show();
        }
        console.log('ds');
    });
});
</script>
</body>
</html>
