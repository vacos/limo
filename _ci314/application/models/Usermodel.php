<?php
class Usermodel extends CI_Model {

    function __construct()
    {
        parent::__construct();
        $this->tb_user = 'user';
        $this->tb_login_log = 'login_log';
    }

    function check($phone = '',$id = ''){
        $where['phone'] = $phone;
        if($where['phone'] == ''){
            $where['id'] = $id;
            unset($where['phone']);
        }
        $this->db->where($where);
        $q_member = $this->db->get($this->tb_user);
        if($q_member->num_rows() > 0){
            return true;
        }
        else{
            return false;
        }
    }

	function login($phone = '',$password = ''){
        $where['phone'] = $phone;
        $where['password'] = $password;
        $where['verify'] = 1;
        $this->db->where($where);
        $q_member = $this->db->get($this->tb_user);
        if($q_member->num_rows() > 0){
            return true;
        }
        else{
            return false;
        }
    }

    function update($id,$data = array()){
        $where['id'] = $id;
        $this->db->where($where);
        $this->db->update($this->tb_user, $data); 
        return true;
    }

    function insert($data = array()){
        $this->db->insert($this->tb_user, $data); 
        return true;
    }

    function get($phone = '',$id = ''){
        $where['phone'] = $phone;
        if($where['phone'] == ''){
            $where['id'] = $id;
            unset($where['phone']);
        }
        $this->db->where($where);
        $q_member = $this->db->get($this->tb_user);
        if($q_member->num_rows() > 0){

            $members = $q_member->result_array();

            if($members[0]['image'] != ''){
                $members[0]['image'] = $this->config->item('usr_url').$members[0]['image'];
            }

            return $this->redataUser($members[0]);
        }
        else{
            return false;
        }
    }

    function log_login($method = 'login',$user_id = 0,$time,$lat = '',$long = ''){
        if($method == 'login'){
            $data['user_id'] = $user_id;
            $data['last_login'] = $time;
            $data['lat'] = $lat;
            $data['long'] = $long;
            $this->db->insert($this->tb_login_log, $data);
            return true;
        }
        elseif($method == 'logout'){

            $last_log_id = $this->getLastLogin($user_id);

            if($last_log_id != false){
                $data['last_logout'] = $time;
                $this->db->where('id',$last_log_id);
                $this->db->update($this->tb_login_log, $data); 
                return true;
            }
        }

        return false;
        
    }

    private function getLastLogin($user_id){
        $where['user_id'] = $user_id;
        $this->db->where($where);
        $this->db->limit(1);
        $this->db->order_by('last_login','desc');
        $last_log = $this->db->get($this->tb_login_log)->result_array(); 
        if($last_log > 0){
            return $last_log[0]['id'];
        }else{
            return false;
        }
    }

    private function redataUser($users){
        $unset_all = array('password');
        foreach ($unset_all as $unset) {
            unset($users[$unset]);
        }

        if($users['type'] == 0){
            unset($users['income']);
            unset($users['pay_income_on']);
            unset($users['rate']);
            unset($users['car']);
        }else{

            $users += $this->getLatLong($users['id']);
        }

        return $users;
    }

    function getLatLong($user_id){

        $this->db->order_by('last_login','desc');
        $this->db->where('user_id',$user_id);
        $logs = $this->db->get($this->tb_login_log)->row_array(); 
        $r['lat'] = @$logs['lat'];
        $r['long'] = @$logs['long'];
        return $r;
    }

    function search($id='',$keyword = '',$type = '',$offset = 0,$limit = 30){

        $sql = "SELECT * FROM `user` WHERE";

        if($id !== ''){
            $id = (int) $id;
            $sql .= '`id` = '.$id.' AND ';
        }
        
        if($type !== ''){
            $type = (int) $type;
            $sql .= '`type` ='.$type.' AND ';
        }

        $sql .= "(`name` LIKE '%$keyword%' ESCAPE '!' OR `phone` LIKE '%$keyword%' ESCAPE '!')";

        $sql .= "ORDER BY `name` ASC";

        $sql .= " LIMIT 1000";

        $q = $this->db->query($sql);
        //echo $this->db->last_query();
        if($q->num_rows() > 0){
            $return = array();

            foreach ($q->result_array() as $member) {
                    
                $return[] = $this->redataUser($member);
            }

            return $return;
        }
        else{
            return false;
        }
    }

}
?>