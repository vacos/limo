<?php
class Drivermodel extends CI_Model {

    function __construct()
    {
        parent::__construct();
        $this->tb_order = 'order';
        $this->tb_user = 'user';
        $this->tb_vote = 'vote';
        $this->date_now = date('Y-m-d H:i:s');
    }

    function getVote($order_id){
        $this->db->where('order_id',$order_id);
        $q = $this->db->get($this->tb_vote);
        if($q->num_rows() > 0){
            return $q->result_array();
        }
        else{
            return false;
        }
    }

    function vote($order_id,$rate,$comment){
        $data['order_id'] = $order_id;
        $data['rate'] = $rate;
        $data['comment'] = $comment;
        $data['create_on'] = $this->date_now;
        $this->db->insert($this->tb_vote, $data); 
        return $this->db->insert_id();
    }

    function check($driver_id){
        $this->db->where('id',$driver_id);
        $this->db->where('verify',1);
        $this->db->where('type',2);
        $q = $this->db->get($this->tb_user);
        if($q->num_rows() > 0){
            return true;
        }
        else{
            return false;
        }
    }

    function confirm($order_id,$driver_id){
        $this->db->where('id',$order_id);
        $this->db->where('status',2);//Find Driver
        $data['status'] = 3;
        $data['update_status_on'] = $this->date_now;
        $data['driving_on'] = $this->date_now;
        $data['driver_id'] = $driver_id;
        $this->db->update($this->tb_order,$data);
    }

    function pickup($order_id,$driver_id){
        $this->db->where('id',$order_id);
        $this->db->where('status',3);//Driving
        $this->db->where('driver_id',$driver_id);
        $data['update_status_on'] = $this->date_now;
        $data['pickup_on'] = $this->date_now;
        $this->db->update($this->tb_order,$data);
    }

    function success($order_id,$driver_id){
        $this->db->where('id',$order_id);
        $this->db->where('status',3);//Driving
        $this->db->where('driver_id',$driver_id);
        $data['status'] = 1;
        $data['update_status_on'] = $this->date_now;
        $data['success_on'] = $this->date_now;
        $this->db->update($this->tb_order,$data);
    }

    function find(){
        $sql = 'SELECT id FROM `user` WHERE `type` = 2 and `active` = 1 AND `id` NOT IN(SELECT driver_id FROM `order` WHERE status = 3)';
        $drivers = $this->db->query($sql)->result_array();
        if(count($drivers) > 0){
            $i = 0;
            
            foreach ($drivers as $driver) {

                $sql = 'SELECT * FROM `login_log` WHERE `user_id` = '.$driver['id'].' ORDER BY `login_log`.`last_login` DESC LIMIT 1';
                $log_drivers = $this->db->query($sql)->result_array();
                if(count($log_drivers) > 0){
                    $r[$i] = array(
                        'driver_id' => $log_drivers[0]['user_id'],
                        'lat' => $log_drivers[0]['lat'],
                        'long' => $log_drivers[0]['long']
                    );
                    $i++;
                }
                
            }
            //print_r($r);die;
            return $r;
        }else{
            return false;
        }
    }

}
?>