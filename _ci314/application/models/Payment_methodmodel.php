<?php
class Payment_methodmodel extends CI_Model {

    function __construct()
    {
        parent::__construct();
        $this->tb_payment_method = 'payment_method';
    }

    function check_cash($user_id = ''){

        $where['card_number	'] = 'Cash';
        $where['user_id'] = $user_id;

        $this->db->where($where);
        $q = $this->db->get($this->tb_payment_method);
        if($q->num_rows() > 0){
            return true;
        }
        else{
            return false;
        }
    }

    function check($id = '',$token = '',$user_id = ''){

        $where['id'] = $id;

        if($where['id'] == ''){
            $where['token'] = $token;
            unset($where['id']);
        }

        $where['user_id'] = $user_id;

        $this->db->where($where);
        $q = $this->db->get($this->tb_payment_method);
        if($q->num_rows() > 0){
            return true;
        }
        else{
            return false;
        }
    }

    function update($id = '',$data = array()){
        $where['id'] = $id;
        $this->db->where($where);
        $this->db->update($this->tb_payment_method, $data); 
        return true;
    }

    function delete($id = ''){
        $where['id'] = $id;
        $this->db->where($where);
        $this->db->delete($this->tb_payment_method); 
        return true;
    }

    function insert($data = array()){
        $this->db->insert($this->tb_payment_method, $data); 
        return $this->db->insert_id();
    }

    function get($id = '',$is_show_card = false){
        $where['id'] = $id;
        $this->db->where($where);
        $q = $this->db->get($this->tb_payment_method);
        if($q->num_rows() > 0){

            $methods = $q->result_array();

            return $this->redata($methods[0],$is_show_card);
        }
        else{
            return false;
        }
    }

    function listing($user_id = ''){

        $re_methods = array();

        $where['user_id'] = $user_id;
        
        $this->db->where($where);
        $this->db->order_by('update_on','desc');
        $q = $this->db->get($this->tb_payment_method);
        if($q->num_rows() > 0){

            $methods = $q->result_array();
            
            foreach ($methods as $key => $method) {
                $re_methods[$key] = $this->redata($method);
            }
            
        }

        return $re_methods;
    }

    private function redata($method,$is_show_card = false){

        $unset_all = array('cvv','create_on','update_on','token','country');

        if($method['card_number'] == 'Cash'){

            $unset_all_more = array('expire','country');

            $unset_all = array_merge($unset_all,$unset_all_more);
        }

        foreach ($unset_all as $unset) {
            unset($method[$unset]);
        }
        $card_number = ($is_show_card) ? $method['card_number'] : $this->replaceCardNumber($method['card_number']);
        $method['card_number'] = ($method['card_number'] != 'Cash') ? $card_number : 'Cash';

        return $method;
    }

    private function replaceCardNumber($card_number = ''){
        return "**** **** **** $card_number";
        // $last_card_number = substr($card_number,12,4);
        // return "**** **** **** $last_card_number";
    }

}
?>