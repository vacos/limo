<?php
class Mainmodel extends CI_Model {

	function get($table=null,$where=null,$order_by=null,$limit=null,$offset=null,$like=null,$or_where=null,$or_like=null){

		if($table != ''){
			$this->db->from($table);
		}
			
		if($where != ''){
			$this->db->where($where);
		}
		
		if($or_where != ''){
			$this->db->or_where($or_where);
		}
			
		if($limit != '' || $offset != ''){
			if($offset != ''){
				$this->db->limit($limit, $offset);
			}
			else{
				$this->db->limit($limit);
			}
		}
			
		if($order_by != ''){
			$this->db->order_by($order_by);
		}
			
		if($like != ''){
			$this->db->like($like);
		}
		
		if($or_like != ''){
			$this->db->or_like($or_like);
		}
			
		$query = $this->db->get();
		return $query;
	}
	// GET

	function edit($table=null,$where=null,$data=null,$set=null){
		$return = false;

		if($table != ''){
			$this->db->from($table);
		}
			
		if($where != ''){
			$this->db->where($where);
		}
			
		if($set != ''){
			foreach ($set as $key => $val){
				$this->db->set($key, $val , FALSE);
			}
			$this->db->update($table);
		}
			
		if($data != ''){
			$return = ($this->db->update($table,$data) ? true : false);
		}
			
		return $return;
	}

	function count($table=null,$where=null,$or_where=null){
		if($table != ''){
			$this->db->from($table);
		}
			
		if($where != ''){
			$this->db->where($where);
		}
		
		if($or_where != ''){
			$this->db->or_where($or_where);
		}

		return $this->db->count_all_results();
	}

	function add($table=null,$data=null){
		$this->db->insert($table,$data);
		return $this->db->insert_id() ;
	}
	
	function del($table=null,$where=null){
		$this->db->where($where);
		return $this->db->delete($table);
	}
	
	function join($table=null,$where=null,$order_by=null,$limit=null,$offset=null,$like=null,$or_like=null,$join=null,$or_where=null){
		
		if($table != ''){
			$this->db->from($table);
		}
			
		if($where != ''){
			$this->db->where($where);
		}
		
		if($or_where != ''){
			$this->db->or_where($or_where);
		}
			
		if($limit != '' || $offset != ''){
			if($offset != ''){
				$this->db->limit($limit, $offset);
			}
			else{
				$this->db->limit($limit);
			}
		}
			
		if($order_by != ''){
			$this->db->order_by($order_by);
		}
			
		if($like != ''){
			$this->db->like($like);
		}
		
		if($or_like != ''){
			$this->db->or_like($or_like);
		}
		
		if($join != ''){
			foreach ($join as $key => $val){
				$this->db->join($key, $val ,'left');
			}
		}
			
		$query = $this->db->get();
		return $query;
	}
	
	function count_join($table=null,$where=null,$like=null,$or_like=null,$join=null,$or_where=null){
		if($table != ''){
			$this->db->from($table);
		}
		
		if($where != ''){
			$this->db->where($where);
		}
		
		if($or_where != ''){
			$this->db->or_where($or_where);
		}
		
		if($like != ''){
			$this->db->like($like);
		}
		
		if($or_like != ''){
			$this->db->or_like($or_like);
		}
		
		if($join != ''){
			foreach ($join as $key => $val){
				$this->db->join($key, $val ,'left');
			}
		}
	
		return $this->db->count_all_results();
	}
	
	function get_sql($sql){
		$query = $this->db->query($sql);
		//echo $this->db->last_query();
		return $query;
	}

	public function date_show($time)
    {
        $arr_time = explode(' ', $time);

        $arr_show = explode('-', $arr_time[0]);
        $show['date'] = $arr_show[2].'.'.$arr_show[1].'.'.$arr_show[0];
        $show['time'] = $arr_time[1];

        return $show;
    }

    public function this_day(){
    	$day = date('d');
    	$month = date('m')*1;
		if($this->langauge == 'th'){
	        $month_arr=array( 
	            "0"=>"", 
	            "1"=>"มกราคม", 
	            "2"=>"กุมภาพันธ์", 
	            "3"=>"มีนาคม", 
	            "4"=>"เมษายน", 
	            "5"=>"พฤษภาคม", 
	            "6"=>"มิถุนายน",  
	            "7"=>"กรกฎาคม", 
	            "8"=>"สิงหาคม", 
	            "9"=>"กันยายน", 
	            "10"=>"ตุลาคม", 
	            "11"=>"พฤศจิกายน", 
	            "12"=>"ธันวาคม"                   
	        );
	        $year = date('Y')+543;
	    }
	   
	    if($this->langauge == 'en'){
	        $month_arr=array( 
	            "0"=>"", 
	            "1"=>"January", 
	            "2"=>"February", 
	            "3"=>"March", 
	            "4"=>"April", 
	            "5"=>"May", 
	            "6"=>"June",  
	            "7"=>"July", 
	            "8"=>"August", 
	            "9"=>"September", 
	            "10"=>"October", 
	            "11"=>"November", 
	            "12"=>"December"                   
	        );
	        $year = date('Y');
	    }
	    

	    return $day.' '.$month_arr[$month].' '.$year;
    }


}
?>