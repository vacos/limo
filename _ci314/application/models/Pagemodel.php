<?php
class Pagemodel extends CI_Model {

    function __construct()
    {
        parent::__construct();
        $this->tb_page = 'page';
    }

    function get($slug = ''){
        $this->db->where('slug',$slug);
        $q = $this->db->get($this->tb_page);
        //echo $this->db->last_query();
        if($q->num_rows() > 0){
            $page = $q->result_array();
            return $page[0];
        }
        else{
            return false;
        }
    }

}
?>