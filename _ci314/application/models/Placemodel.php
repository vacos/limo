<?php
class Placemodel extends CI_Model {

    function __construct()
    {
        parent::__construct();
        $this->tb_place = 'place';
    }

    function check_type($user_id,$type = 0){
        $this->db->where('type',$type);
        $this->db->where('user_id',$user_id);
        $q = $this->db->get($this->tb_place);
        //echo $this->db->last_query();die;
        if($q->num_rows() > 0){
            return true;
        }
        else{
            return false;
        }
    }

    function get($user_id,$place_id = 0){
        $this->db->where('id',$place_id);
        $this->db->where('user_id',$user_id);
        $q = $this->db->get($this->tb_place);
        //echo $this->db->last_query();
        if($q->num_rows() > 0){
            $place = $q->result_array();
            $r[0] = $this->reData($place[0]);
            return $r;
        }
        else{
            return false;
        }
    }

    function listing($user_id = 0){
        $this->db->order_by("type", "asc");
        $this->db->order_by("create_on", "desc");
        $this->db->where('user_id',$user_id);
        $q = $this->db->get($this->tb_place);

        $places = $q->result_array();

        $r = array();

        if(count($places) > 0){

            $i = 0;

            foreach ($places as $place) {

                $r[$i] = $this->reData($place);

                $i++;
            }

        }

        return $r;
    }

    function insert($data){
        $this->db->insert($this->tb_place, $data); 
        return $this->db->insert_id();
    }

    function delete($user_id,$place_id = 0){
        $this->db->where('user_id', $user_id);
        $this->db->where('id', $place_id);
        $this->db->delete($this->tb_place);
        //echo $this->db->last_query();die;
    }

    private function reData($place){
        switch ($place['type']) {
            case 1:
                $name = 'home';
                break;
            case 2:
                $name = 'work';
                break;
            default:
                $name = 'other';
                break;
        }

        $r['place_id'] = $place['id'];
        $r['name'] = $place['name'];
        $r['type'] = $name;
        $r['lat'] = $place['lat'];
        $r['long'] = $place['long'];
        return $r;
    }

}
?>