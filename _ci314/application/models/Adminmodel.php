<?php
class adminModel extends CI_Model {
    private $table = '';
    function __construct()
    {
        //Call the Model constructor
        parent::__construct();
    }
    function set($table){
        $this->table = $table;
    }
    function getlist($limit="",$offset="",$filter=array()){
        $this->_where($filter);
        if($limit!=""||$offset!=""){
            $this->db->limit($limit,$offset);
        }
        return $this->db->get($this->table);
    }
    function getrecord($filter=array()){
        $this->_where($filter);
        return $this->db->get($this->table);
    }
    function countrow($filter=array()){
        $this->_where($filter);
        return $this->db->get($this->table)->num_rows();
    }
    function _where($filter=array()){
            foreach($filter as $key=>$val){
                if($val!=""){
                    switch($key){
                        case'order':
                            foreach($val as $key2=>$val2){
                                $this->db->order_by($key2,$val2);
                            }
                            break;
                        case'limit':
                            $this->db->limit($val);
                            break;
                        case'join':
                            foreach($val as $key2=>$val2){
                                $this->db->join($key2,$val2);
                            }
                            break;
                        case'left-join':
                            foreach($val as $key2=>$val2){
                                $this->db->join($key2,$val2,'left');
                            }
                            break;
                        case'words':
                            $this->db->where($val);
                            break;
                        case'md5':
                            $this->db->where("md5({$val[0]})='{$val[1]}'");
                            break;
                        case'group':
                            $this->db->group_by($val);
                            break;
                        default:
                            $val = ($val=='null')?'':$val;
                            $this->db->where($key,$val);
                        break;
                    }
                }
            }
    }
    function add($data){
        $this->db->insert($this->table,$data);
        return $this->db->insert_id() ;
    }
    
    function edit($filter=array(),$data=array())
    {
        $chk_fil = true;
        foreach($filter as $key=>$val){
            if($val==''){
                $chk_fil = false;
            }
        }
        
        $rt = false;
        if($chk_fil){
            $this->_where($filter);
            $rt =  $this->db->update($this->table,$data);
        }
        return $rt;

    }
    
    function del($filter=array()){
        $this->_where($filter);
        $this->db->delete($this->table);
    }
    
    function sql($sql){
        return $this->db->query($sql);
    }
	
}
?>