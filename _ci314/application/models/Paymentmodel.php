<?php
class Paymentmodel extends CI_Model {

    function __construct()
    {
        parent::__construct();
        $this->tb_payment = 'payment';
        $this->tb_order = 'order';
        $this->tb_payment_method = 'payment_method';
        $this->tb_user = 'user';
        $this->date_now = date('Y-m-d H:i:s');
    }

    function success($id = '',$token = '',$user = 'cron',$log = ''){
        $where['id'] = $id;
        $data['update_status_on'] = $this->date_now;
        $data['success_on'] = $this->date_now;
        $data['status'] = 1;
        $data['user'] = $user;
        $data['log'] = $log;
        // if($token != ''){
        //     $data['charge_token'] = $token;
        // }
        
        //print_r($data);die;
        $this->db->where($where);
        $this->db->update($this->tb_payment, $data); 
        return true;
    }

    function pending($id = '',$charge_token,$authorize_uri = ''){
        $where['id'] = $id;
        $data['update_status_on'] = $this->date_now;
        $data['status'] = 3;//pending
        $data['charge_token'] = $charge_token;
        $data['authorize_uri'] = $authorize_uri;
        //print_r($data);die;
        $this->db->where($where);
        $this->db->update($this->tb_payment, $data); 
        return true;
    }

    function fail($id = '',$log = ''){
        $where['id'] = $id;
        $data['update_status_on'] = $this->date_now;
        $data['status'] = 2;//fail
        $data['log'] = $log;
        //print_r($data);die;
        $this->db->where($where);
        $this->db->update($this->tb_payment, $data); 
        return true;
    }

    function insert($order_id){
        $data['order_id'] = $order_id;
        $data['update_status_on'] = $this->date_now;
        $data['create_on'] = $this->date_now;
        $this->db->insert($this->tb_payment, $data); 
        return $this->db->insert_id();
    }

    function cron(){

        $where[$this->tb_payment.'.status'] = 0;//Waiting

        $selects = array(
            $this->tb_payment.'.id as payment_id',
            $this->tb_payment_method.'.token as card_token',
            $this->tb_payment_method.'.card_number',
            $this->tb_user.'.id as user_id',
            $this->tb_user.'.token as user_token',
            $this->tb_order.'.fare',
            $this->tb_order.'.driver_id',
        );
        $select_txt = implode(',',$selects);
        $this->db->select($select_txt);
        $this->db->where($where);
        $this->db->limit(10);
        $this->db->order_by($this->tb_payment.'.create_on','asc');
        $this->db->join($this->tb_order, $this->tb_payment.'.order_id = '.$this->tb_order.'.id', 'left');
        $this->db->join($this->tb_payment_method, $this->tb_payment_method.'.id = '.$this->tb_order.'.id_payment', 'left');
        $this->db->join($this->tb_user, $this->tb_user.'.id = '.$this->tb_order.'.user_id', 'left');
        return $this->db->get($this->tb_payment)->result_array();

    }

    function get_charge($payment_id = '',$order_id = ''){

        if($payment_id != ''){
            $where[$this->tb_payment.'.id'] = $payment_id;
        }
        
        if($order_id != ''){
            $where[$this->tb_payment.'.order_id'] = $order_id;
        }

        $selects = array(
            $this->tb_payment.'.id as payment_id',
            $this->tb_payment.'.authorize_uri as authorize_uri',
            $this->tb_payment.'.status as status_payment',
            $this->tb_payment.'.charge_token as charge_token',
            $this->tb_payment_method.'.token as card_token',
            $this->tb_payment_method.'.card_number',
            $this->tb_user.'.id as user_id',
            $this->tb_user.'.token as user_token',
            $this->tb_order.'.fare',
            $this->tb_order.'.driver_id',
        );
        $select_txt = implode(',',$selects);
        $this->db->select($select_txt);
        $this->db->where($where);
        $this->db->limit(10);
        $this->db->order_by($this->tb_payment.'.create_on','asc');
        $this->db->join($this->tb_order, $this->tb_payment.'.order_id = '.$this->tb_order.'.id', 'left');
        $this->db->join($this->tb_payment_method, $this->tb_payment_method.'.id = '.$this->tb_order.'.id_payment', 'left');
        $this->db->join($this->tb_user, $this->tb_user.'.id = '.$this->tb_order.'.user_id', 'left');
        return $this->db->get($this->tb_payment)->result_array();

    }

    
}
?>