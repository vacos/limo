<?php
class Carmodel extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    function listing(){
        
        $car[] = array(
            'id' => 1,
            'name' => 'Camry',
            //'icon' => $this->config->item('static_url').'di/car/sedan.png',
            'icon' => $this->config->item('static_url').'di/car/camry.png',
        );

        // $car[] = array(
        //     'id' => 2,
        //     'name' => 'Taxi',
        //     'icon' => $this->config->item('static_url').'di/car/taxi.png',
        // );

        // $car[] = array(
        //     'id' => 3,
        //     'name' => 'Van',
        //     'icon' => $this->config->item('static_url').'di/car/van.png',
        // );

        return $car;
    }

}
?>