<?php
class Omisemodel extends CI_Model {

    function __construct()
    {
        parent::__construct();
        $this->tb_omise_log = 'omise_log';
        $this->date_now = date('Y-m-d H:i:s');
    }

    function insert($user_id = 0,$log = ''){
        $data['user_id'] = $user_id;
        $data['log'] = $log;
        $data['create_on'] = $this->date_now;
        $this->db->insert($this->tb_omise_log, $data); 
        return $this->db->insert_id();
    }
}
?>