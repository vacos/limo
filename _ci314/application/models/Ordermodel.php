<?php
class Ordermodel extends CI_Model {

    function __construct()
    {
        parent::__construct();
        $this->tb_order = 'order';
        $this->tb_order_log = 'order_log';
        $this->tb_user = 'user';
        $this->tb_payment = 'payment';

        $selects_orders = array('id','user_id','number','start_location_lat','start_location_long','start_location','destination_lat','destination_long','destination','schedule_ride','car_type','note','id_payment','fare','status','create_on','update_status_on','cancel_on','driver_id','driving_on','pickup_on','success_on','expressway');

        $select_text = '';
        foreach ($selects_orders as $order_select) {
            $select_text .= 'order.'.$order_select.', ';
        }

        $selects_user = array('image','type','name','phone');
        foreach ($selects_user as $user_select) {
            $select_text .= 'user.'.$user_select.', ';
        }
        $this->select_text = $select_text;
    }

    function find_driver(){
        $this->db->select($this->select_text);
        
        $this->db->order_by($this->tb_order.'.create_on','asc');
        $this->db->where('status',2);
        $this->db->join($this->tb_user, $this->tb_user.'.id = '.$this->tb_order.'.user_id', 'left');
        $q = $this->db->get($this->tb_order);
        //echo $this->db->last_query();
        if($q->num_rows() > 0){
            $orders = $q->result_array();
            $r = array();
            foreach ($orders as $key => $value) {
                $r[$key] = $value;

                if($value['image'] != ''){
                    $r[$key]['image'] = $this->config->item('usr_url').$value['image'];
                }

                $r[$key] = $this->redataUser($value);
            }

            return $r;
        }
        else{
            return false;
        }
    }

    function calculate($car_id,$km,$time){
        $start_km = 2;
        $bath_per_km = 15;
        $bath_per_min = 6;
        $fare = 100;
        $time = ($time < 1) ? 1 : $time;

        if($car_id == 1){

            $km_run = $km - $start_km;
            $fare_km = 0;

            if($km_run > 0){
                $fare_km = $km_run*$bath_per_km;
            }

            $fare_time = $time*$bath_per_min;

            $fare = $fare+$fare_km+$fare_time;
        }
        $fare = $fare + $this->pluslimo($fare);
        $fare = $fare + $this->plusomise($fare);
        return 20;
        return ceil($fare);
    }

    private function pluslimo($fare){
        $service_limo = 10;
        $service_fare = ($fare*$service_limo)/100;
        return $service_fare;
    }

    private function plusomise($fare){
        $service_omise = 3.65;
        $service_fare = ($fare*$service_omise)/100;
        $service_fare = $this->plusvat($service_fare);
        return $service_fare;
    }

    private function plusvat($fare){
        $vat = 7;
        $service_fare = ($fare*$vat)/100;
        return $service_fare;
    }

    // function calculate($car_id,$km,$time){

    //     $cal = 99;

    //     return $cal;
    // }

    function calculate_income_driver($fare){

        //$cal = 99;

        return $fare;
    }

    function check($order_id = 0){
        $this->db->where('id',$order_id);
        $q = $this->db->get($this->tb_order);
        //echo $this->db->last_query();
        if($q->num_rows() > 0){
            return true;
        }
        else{
            return false;
        }
    }

    function check_status($order_id = 0,$status = 0){
        $this->db->where('id',$order_id);
        $this->db->where('status',$status);
        $q = $this->db->get($this->tb_order);
        //echo $this->db->last_query();
        if($q->num_rows() > 0){
            return true;
        }
        else{
            return false;
        }
    }

    function get($user_id,$order_id = 0,$is_show = false){
        $this->db->select($this->select_text);
        $this->db->where($this->tb_order.'.id',$order_id);
        if(!$is_show){
            $this->db->where($this->tb_order.'.user_id',$user_id);
        }
        $this->db->join($this->tb_user, $this->tb_user.'.id = '.$this->tb_order.'.user_id', 'left');
        $q = $this->db->get($this->tb_order);
        //echo $this->db->last_query();die;
        if($q->num_rows() > 0){
            $history = $q->result_array();
            
            if($history[0]['image'] != ''){
               $history[0]['image'] = $this->config->item('usr_url').$history[0]['image'];
            }

            $history[0] = $this->reData($history[0]);
            $history[0] = $this->redataUser($history[0]);
            return $history;
        }
        else{
            return false;
        }
    }

    function search_number($order_number = ''){
        $this->db->where('number',$order_number);
        $q = $this->db->get($this->tb_order);
        //echo $this->db->last_query();
        if($q->num_rows() > 0){
            $history = $q->result_array();
            $r[0] = $this->reData($history[0]);
            return $r;
        }
        else{
            return false;
        }
    }

    function history($user_id = 0,$offset = 0,$limit = 10){
        $this->db->limit($limit,$offset);
        $this->db->order_by("number", "desc");
        $this->db->where('user_id',$user_id);
        $this->db->where('status',1);
        $q = $this->db->get($this->tb_order);

        $histories = $q->result_array();

        $r = array();

        if(count($histories) > 0){

            $i = 0;

            foreach ($histories as $history) {

                $r[$i] = $this->reData($history);

                $i++;
            }

        }

        return $r;
    }

    function history_driver($user_id = 0,$offset = 0,$limit = 10){
        $this->db->limit($limit,$offset);
        $this->db->order_by("number", "desc");
        $this->db->where($this->tb_order.'.driver_id',$user_id);
        $this->db->where($this->tb_order.'.status',1);
        $this->db->or_where($this->tb_order.'.status',2);
        $this->db->or_where($this->tb_order.'.status',3);
        $this->db->or_where($this->tb_order.'.status',4);
        $q = $this->db->get($this->tb_order);

        $histories = $q->result_array();

        $r = array();

        if(count($histories) > 0){

            $i = 0;

            foreach ($histories as $history) {

                $r[$i] = $this->reData($history);

                $i++;
            }

        }

        return $r;
    }

    function insert($data){
        $this->db->insert($this->tb_order, $data); 
        return $this->db->insert_id();
    }

    function getLastOrderNumber(){
        $this->db->limit(1);
        $this->db->order_by("number", "desc");
        $q = $this->db->get($this->tb_order);
        if($q->num_rows() > 0){
            $order = $q->result_array();
            return $order[0]['number'] + 1;
        }
        else{
            return 1100001;
        }
    }

    function cancel($order_id,$user_id){
        $this->db->where('id',$order_id);
        $this->db->where('user_id',$user_id);
        $data['status'] = 0;
        $data['update_status_on'] = $this->date_now;
        $data['cancel_on'] = $this->date_now;
        $this->db->update($this->tb_order,$data);
    }

    private function reData($history){
        $r = $history;
        $r['date_show'] = $this->convertTimeShow($history['create_on']);
        $r['showcard'] = $this->getCard($history['id_payment']);
        $r['showstatus'] = 'Success';// Fix Success
        return $r;
    }

    private function getCard($card_id){
        $sql = "SELECT card_number FROM `payment_method` WHERE id = ".$card_id." LIMIT 1";
        $card = $this->db->query($sql)->result_array();
        if(count($card) > 0){
            return $card[0]['card_number'];
        }else{
            return '';
        }
    }

    private function convertTimeShow($date = ''){
        $timestamp = strtotime($date);
        return date('d M Y H:i',$timestamp);
    }

    private function redataUser($users){
        $unset_all = array('password');
        foreach ($unset_all as $unset) {
            unset($users[$unset]);
        }

        if($users['type'] == 0){
            unset($users['income']);
            unset($users['pay_income_on']);
        }

        return $users;
    }

    function update_order_old($date_time_del_10_min){

        $where['create_on <'] = $date_time_del_10_min;
        $where['status'] = 2;//Find Driver
        $this->db->where($where);
        
        $data_update['status'] = 0;
        $date = date('Y-m-d H:i:s');
        $data_update['update_status_on'] = $date;
        $data_update['cancel_on'] = $date;
        $this->db->update($this->tb_order, $data_update); 

    }


}
?>