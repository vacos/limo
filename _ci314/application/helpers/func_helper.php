<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Translate given text according to specified language
 *
 * @access	public
 * @param	string
 * @return	string
 */

if(!function_exists('translate')){
    function translate($key = ''){
        $CI =& get_instance();
        return (isset($CI->session->userdata['translate'][$key])) ? $CI->session->userdata['translate'][$key] : $key;
    }
}

if(!function_exists('getMeta')){
    function getMeta($page,$lan){
        $CI =& get_instance();
        $filter = array('con_module'=>MOD_METADATA,'con_title like'=>$page,'con_status'=>1,'con_lan'=>$lan);
        $CI->db->where($filter);
        $CI->db->order_by('con_order','ASC');
        $record  =  $CI->db->get('tc_contents');

        if($record->num_rows()>0){
            $row = $record->row();
            return $text = '
                <!-- Hook Meta-->
                <meta name="Title" content="'.trim($row->con_metatitle).'"/>
                <meta name="Description" content="'.trim($row->con_metadesc).'"/>
                <meta name="Keywords" content="'.trim($row->con_metakeys).'"/>
                <meta property="og:title"         content="'.trim($row->con_metatitle).'" />
                <meta property="og:description"   content="'.trim($row->con_metadesc).'" />
                <!-- End Hook Meta-->
            ';
        }else{
            return false;
        }
    }
}
if (!function_exists('chkBrowser'))
{
    function chkBrowser(){
        $tag = '<span class="cn tl"></span><span class="cn tr"></span><span class="cn br"></span><span class="cn bl"></span>';
        return (preg_match("/MSIE/",$_SERVER['HTTP_USER_AGENT'])==1)?$tag:'';
        //return preg_match("/".$nameBroser."/",$_SERVER['HTTP_USER_AGENT']);(chkBrowser("MSIE")==1)
    }
}

if (!function_exists('checkLower9'))
{
    function checkLower9(){
        return (preg_match('/(?i)msie [1-9]/',$_SERVER['HTTP_USER_AGENT']))?true:false;
    }
}
if (!function_exists('chkIE'))
{
    function chkIE(){
        return (preg_match("/MSIE/",$_SERVER['HTTP_USER_AGENT'])==1)?true:false;
    }
}
if (!function_exists('dateString'))
{
    function dateString($time){
        return TimeAgo(convert_datetime($time));
        //return preg_match("/".$nameBroser."/",$_SERVER['HTTP_USER_AGENT']);(chkBrowser("MSIE")==1)
    }
}

if (!function_exists('dimension'))
{
    function dimension($data,$size){
        $dim = explode('X',$data);
        return ($dim[0]<=$dim[1])?'height="'.$size.'"':'width="'.$size.'"';
    }
}


if (!function_exists('cutStr'))
{
    function cutStr($txt,$len){
        return (strlen(utf8_decode($txt))>=$len)?mb_substr($txt,0,$len,'utf-8').' ...':$txt;
    }
}


if (!function_exists('cURL'))
{
    function cURL(){
        $protocol = strpos(strtolower($_SERVER['SERVER_PROTOCOL']),'https')=== FALSE ? 'http' : 'https';
        $host     = $_SERVER['HTTP_HOST'];
        $script   = $_SERVER['SCRIPT_NAME'];
        $params   = $_SERVER['QUERY_STRING'];
        return $protocol . '://' . $host . $script . '?' . $params;
    }
}

if (!function_exists('thMonth'))
{
    function thMonth($date){
        $i = date('m',strtotime($date));
        $month =  array('','มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม');
        echo $month[$i*1];

    }
}



   function convert_datetime($str) {
        list($date, $time) = explode(' ', $str);
    	list($year, $month, $day) = explode('-', $date);
    	list($hour, $minute, $second) = explode(':', $time);
    	$timestamp = mktime($hour, $minute, $second, $month, $day, $year);
    	return $timestamp;
    }

 function TimeAgo($datefrom,$dateto=-1){
// Defaults and assume if 0 is passed in that
// its an error rather than the epoch

    if($datefrom<=0) { return "A long time ago"; }
    if($dateto==-1) { $dateto = time(); }

// Calculate the difference in seconds betweeen
// the two timestamps

    $difference = $dateto - $datefrom;

// If difference is less than 60 seconds,
// seconds is a good interval of choice

    if($difference < 60){
        $interval = "s";
    }

// If difference is between 60 seconds and
// 60 minutes, minutes is a good interval
    elseif($difference >= 60 && $difference<60*60){
     $interval = "n";
    }

// If difference is between 1 hour and 24 hours
// hours is a good interval
    elseif($difference >= 60*60 && $difference<60*60*24){
    $interval = "h";
    }

// If difference is between 1 day and 7 days
// days is a good interval
    elseif($difference >= 60*60*24 && $difference<60*60*24*7){
        $interval = "d";
    }

// If difference is between 1 week and 30 days
// weeks is a good interval
    elseif($difference >= 60*60*24*7 && $difference < 60*60*24*30){
    $interval = "ww";
    }

// If difference is between 30 days and 365 days
// months is a good interval, again, the same thing
// applies, if the 29th February happens to exist
// between your 2 dates, the function will return
// the 'incorrect' value for a day
    elseif($difference >= 60*60*24*30 && $difference < 60*60*24*365){
        $interval = "m";
    }

// If difference is greater than or equal to 365
// days, return year. This will be incorrect if
// for example, you call the function on the 28th April
// 2008 passing in 29th April 2007. It will return
// 1 year ago when in actual fact (yawn!) not quite
// a year has gone by
    elseif($difference >= 60*60*24*365){
        $interval = "y";
    }

// Based on the interval, determine the
// number of units between the two dates
// From this point on, you would be hard
// pushed telling the difference between
// this function and DateDiff. If the $datediff
// returned is 1, be sure to return the singular
// of the unit, e.g. 'day' rather 'days'

    switch($interval){
        case "m":
        $months_difference = floor($difference / 60 / 60 / 24 /
        29);
        while (mktime(date("H", $datefrom), date("i", $datefrom),
        date("s", $datefrom), date("n", $datefrom)+($months_difference),
        date("j", $dateto), date("Y", $datefrom)) < $dateto)
        {
        $months_difference++;
        }
        $datediff = $months_difference;

        // We need this in here because it is possible
        // to have an 'm' interval and a months
        // difference of 12 because we are using 29 days
        // in a month

        if($datediff==12)
        {
        $datediff--;
        }

        $res = ($datediff==1) ? "$datediff month ago" : "$datediff
        months ago";
        break;

        case "y":
        $datediff = floor($difference / 60 / 60 / 24 / 365);
        $res = ($datediff==1) ? "$datediff year ago" : "$datediff
        years ago";
        break;

        case "d":
        $datediff = floor($difference / 60 / 60 / 24);
        $res = ($datediff==1) ? "$datediff day ago" : "$datediff
        days ago";
        break;

        case "ww":
        $datediff = floor($difference / 60 / 60 / 24 / 7);
        $res = ($datediff==1) ? "$datediff week ago" : "$datediff
        weeks ago";
        break;

        case "h":
        $datediff = floor($difference / 60 / 60);
        $res = ($datediff==1) ? "$datediff hour ago" : "$datediff
        hours ago";
        break;

        case "n":
        $datediff = floor($difference / 60);
        $res = ($datediff==1) ? "$datediff minute ago" :
        "$datediff minutes ago";
        break;

        case "s":
        $datediff = $difference;
        $res = ($datediff==1) ? "$datediff second ago" :
        "$datediff seconds ago";
        break;
    }
    return $res;
}

if (!function_exists('friendlyURL'))
{
    function friendlyURL($string){
        $string = preg_replace("`\[.*\]`U","",$string);
        $string = preg_replace('`&(amp;)?#?[a-z0-9]+;`i','-',$string);
        $string = str_replace('%', '-percent', $string);
        $string = htmlentities($string, ENT_COMPAT, 'utf-8');
        $string = preg_replace( "`&([a-z])(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig|quot|rsquo);`i","\\1", $string );
        $string = preg_replace( array("`[^a-z0-9ก-๙เ-า]`i","`[-]+`") , "-", $string);
        return strtolower(trim($string, '-'));
    }
}

if (!function_exists('getPost'))
{
    function getPost( $key ){
    if(empty( $_POST[$key] ) )
        return '';
    return $_POST[$key];
    }
}

if (!function_exists('encrypt'))
{
    function encrypt($text){
        $arr = explode("/",$text);
        $entext = "";
        ///uploaded/scan/40/000000001/400000000012/Cardio_EKG
        foreach ($arr as $key=>$val) {
            if($key<2){
                $val = "";
            }elseif($key==2){
                $val = $val;
            }elseif($key==3){
                $val = "-34245v".$val;
            }elseif($key==4){
                $val = "b04320c".$val;
            }else{
                $val = "o34gf".$val;
            }
            $entext .= $val;
        }
        return $entext;
    }

}
if (!function_exists('decrypt'))
{
   function decrypt($text){
    ///uploaded/scan/40/000000001/400000000012/Cardio_EKG
      $detext=""; 
      $text = str_replace(array("-34245v","b04320c","o34gf"), "/", $text);
      $arr = explode("/",$text);
      $detext = "";
        ///uploaded/scan/40/000000001/400000000012/Cardio_EKG
        foreach ($arr as $key=>$val) {
            if($key==0){
                $val = $val;//*0.5;
            }elseif($key==1){
                $val = sprintf('%09d',$val);
            }
            $detext .= "/".$val;
        }
      return "uploaded/scan".$detext;
    }
}


